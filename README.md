# The Empower Stack

This repository contains the libraries and shared services used by the Empower Stack. 

This is where the development of the core of the stack is happening. The documentation and website is also maintained here.

Here is what each folder contains:
- The lib-go folder contains all golang libraries available in the stack.
- the login folder contains the login service, used by the stack to manage authentification
- The website and docs folders contain the source of the website and the documentation.

Each time a commit is done, the new image of the login service will be built and the website will be published on Gitlab Pages.

You can find more information about the Empower Stack on the following website: https://empower.sh