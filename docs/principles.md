---
id: principles
title: Principles
---

The principles are the values driving the decisions we make as part of designing the Empower Stack.

## Stay simple

The core goal of the Empower Stack is to bring even the most complex concepts and technologies to as many developers as possible. For this, the stack must be accessible and straightforward.

It must be simple enough to be used by non-professionals, passionate people, and associations. The code of the stack itself must be simple to read so that any user can become a contributor.


## Libraries over framework

Libraries are functions you can call from the programing language, anywhere. Frameworks are specific technologies you can use to easily get the desired result. You call a library from your code, while a framework calls your code and thus enclose it.

Most technologies out there are frameworks. They are great and many projects would not be delivered without them but they have a fundamental flaw: When you use them you learn the framework, you do not learn the language. And then many developers do not improve as much as they could because they lose time learning something which may completely disappear some years later.

Helping the developers improve their skills is also one of the core goals of the stack, this is even where the name comes from, to empower them. To be productive, preexisting functions prior to your project are important, and we believe they can be efficiently delivered by libraries instead of a framework that would jail them inside.


## Always use what the experts use

For every important part of the stack, whether it is the back-office language, the frontend, the container orchestrator, etc... All of these are specifics domains, for each of them we find who the best experts are, who manage the most complex cases, and find out what they are using.

This is not as simple as just looking at which technology seems most popular, has more Github stars. People love following trends, sometimes bad trends. Following the real experts looks way more reliable to us.


## Do not reinvent the wheel, except if the existing solutions do not fit our values

In the Empower Stack, we focus our effort on integration, not creating new technology. If something seems to have proven successful, has a large and active community, then we become part of this community and focus on integrating it with the other technologies we chose.

Still, we do not want to reuse at all costs. If we realize some part are not mature enough, do not share the same vision, or doesn't exist, then it shall be implemented as part of the stack.


## Stay compatible at each level

The stack is a set of independents components, you should not be forced to use the whole stack. 

For example, you can use the back-office libraries to create an API, but then use completely different technology to create the clients using it, or use the container orchestrator to include applications in languages which are not part of the stack.

For this, choosing and using standard is extremely important. People are not forced to commit to the Empower Stack completely and can implement it bit by bit, staying compatible with their legacy applications and the many services you can find on the Internet.