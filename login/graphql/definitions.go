package graphql

import (
	"gitlab.com/empowerlab/stack/lib-go/libgraphql"
	// "gitlab.com/empowerlab/stack/lib-go/libgrpc"
	// "gitlab.com/empowerlab/stack/lib-go/liborm"

	"gitlab.com/empowerlab/stack/login/grpc"
)

var Definitions *libgraphql.Definitions

func init() {

	Definitions = &libgraphql.Definitions{
		Name:       "login",
		Repository: "gitlab.com/empowerlab/stack/login",
	}

	Definitions.Register(&libgraphql.Definition{
		Grpc: grpc.Definitions.GetByID("account"),
	})

	Definitions.Register(&libgraphql.Definition{
		Grpc: grpc.Definitions.GetByID("role"),
	})

	// dataGRPC := grpc.Definitions.GetByID("data")
	// Definitions.Register(&libgraphql.Definition{
	// 	Grpc: grpc.Definitions.GetByID("data"),
	// 	CustomFuncs: []*liborm.CustomFunc{
	// 		dataGRPC.GetCustomFuncByName("loadData"),
	// 	},
	// })

}
