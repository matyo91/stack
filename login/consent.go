package main

import (
	// "context"
	"fmt"
	"html/template"
	"net/http"

	hydra "github.com/ory/hydra/sdk/go/hydra/client/admin"
	"github.com/ory/hydra/sdk/go/hydra/models"
	"github.com/pkg/errors"
	// "golang.org/x/oauth2"
)

// handles request at /home - a small page that let's you know what you can do in this app. Usually the first.
// page a user sees.
func handleHome(w http.ResponseWriter, _ *http.Request) {
	// var config = client.GetOAuth2Config()
	// config.RedirectURL = "http://localhost:4445/callback"
	// config.Scopes = []string{"offline", "openid"}

	// var authURL = client.GetOAuth2Config().AuthCodeURL(state) + "&nonce=" + state
	// renderTemplate(w, "home.html", authURL)
}

// After pressing "click here", the Authorize Code flow is performed and the user is redirected to Hydra. Next, Hydra
// validates the consent request (it's not valid yet) and redirects us to the consent endpoint which we set with `CONSENT_URL=http://localhost:4445/consent`.
func handleConsent(w http.ResponseWriter, r *http.Request) {
	// Get the consent requerst id from the query.
	consentChallenge := r.URL.Query().Get("consent_challenge")
	if consentChallenge == "" {
		http.Error(w, errors.New("Consent endpoint was called without a consent challenge").Error(), http.StatusBadRequest)
		return
	}

	// Fetch consent information
	consentRequest, err := client.GetConsentRequest(hydra.NewGetConsentRequestParams().WithConsentChallenge(consentChallenge))
	if err != nil {
		http.Error(w, errors.Wrap(err, "The consent request endpoint does not respond").Error(), http.StatusBadRequest)
		return
	}
	//  else if consentRequestresponse.StatusCode != http.StatusOK {
	// 	http.Error(w, errors.New(fmt.Sprintf("Consent request endpoint gave status code %d but expected %d", response.StatusCode, http.StatusOK)).Error(), http.StatusBadRequest)
	// 	return
	// }

	// This helper checks if the user is already authenticated. If not, we
	// redirect them to the login endpoint.
	// user := authenticated(r)
	// if user == "" {
	// 	http.Redirect(w, r, "/login?consent="+consentRequestID, http.StatusFound)
	// 	return
	// }

	skip := true
	for _, scope := range consentRequest.Payload.RequestedScope {
		if scope != "openid" && scope != "offline" {
			skip = false
		}
	}

	// Apparently, the user is logged in. Now we check if we received POST
	// request, or a GET request.
	if r.Method == "POST" || skip {
		// Ok, apparently the user gave their consent!

		// Let's check which scopes the user granted.
		var grantedScopes = []string{}
		if skip {
			grantedScopes = []string{"openid", "offline"}
		} else {
			// Parse the HTTP form - required by Go.
			if err := r.ParseForm(); err != nil {
				http.Error(w, errors.Wrap(err, "Could not parse form").Error(), http.StatusBadRequest)
				return
			}

			for key := range r.PostForm {
				// And add each scope to the list of granted scopes.
				grantedScopes = append(grantedScopes, key)
			}
		}

		fmt.Printf("challenge consent %s\n", consentChallenge)
		// Ok, now we accept the consent request.
		request, err := client.AcceptConsentRequest(hydra.NewAcceptConsentRequestParams().WithBody(
			&models.HandledConsentRequest{
				GrantedScope: grantedScopes,
				// Remember: true,
				// RememberFor: 3600,
			}).WithConsentChallenge(consentChallenge))
		if err != nil {
			http.Error(w, errors.Wrap(err, "The accept consent request endpoint encountered a network error").Error(), http.StatusInternalServerError)
			return
		}
		//  else if response.StatusCode != http.StatusOK {
		// 	http.Error(w, errors.New(fmt.Sprintf("Accept consent request endpoint gave status code %d but expected %d", response.StatusCode, http.StatusNoContent)).Error(), http.StatusInternalServerError)
		// 	return
		// }

		// Redirect the user back to hydra, and append the consent response! If the user denies request you can
		// either handle the error in the authentication endpoint, or redirect the user back to the original application
		// with:
		// response, err := client.RejectOAuth2ConsentRequest(consentRequestId, payload)
		fmt.Println(request.Payload.RedirectTo)
		http.Redirect(w, r, request.Payload.RedirectTo, http.StatusFound)
		return
	}

	// We received a get request, so let's show the html site where the user may give consent.
	renderTemplate(w, "consent.html", struct {
		*hydra.GetConsentRequestOK
		ConsentChallenge string
	}{GetConsentRequestOK: consentRequest, ConsentChallenge: consentChallenge})
}

// The user hits this endpoint if not authenticated. In this example, they can sign in with the credentials
// buzz:lightyear
func handleLogin(w http.ResponseWriter, r *http.Request) {
	challenge := r.URL.Query().Get("login_challenge")

	user, err := ab.LoadCurrentUser(&r)
	if user != nil && err == nil {

		if challenge == "" {
			if cookie, err := r.Cookie("login-challenge"); err == nil {
				if err = cookieStore.Decode("login-challenge", cookie.Value, &challenge); err == nil {
					fmt.Printf("The value of challenge is %q\n", challenge)
				}
			}
		}

		if challenge == "" {
			http.Redirect(w, r, "/", http.StatusFound)
			return
		}

		fmt.Printf("challenge login %s\n", challenge)
		request, err := client.AcceptLoginRequest(hydra.NewAcceptLoginRequestParams().WithBody(
			&models.HandledLoginRequest{
				// The subject is a string, usually the user id.
				Subject: &user.(*User).UUID,
				Context: map[string]interface{}{
					"roles": []string{"admin"},
				},
				// Remember: true,
				// RememberFor: 3600,
			}).WithLoginChallenge(challenge))
		// fmt.Printf("response %+v\n", request.Payload)
		// fmt.Printf("%s %s\n", response.StatusCode, http.StatusOK)
		if err != nil {
			http.Error(w, errors.Wrap(err, "The accept login request endpoint encountered a network error").Error(), http.StatusInternalServerError)
			return
		}
		// else if response.StatusCode != http.StatusOK {
		// 	http.Error(w, errors.New(fmt.Sprintf("Accept login request endpoint gave status code %d but expected %d", response.StatusCode, http.StatusNoContent)).Error(), http.StatusInternalServerError)
		// 	return
		// }

		fmt.Println("redirectTo", request.Payload.RedirectTo)
		http.Redirect(w, r, request.Payload.RedirectTo, http.StatusFound)
		return
	}

	if challenge != "" {
		if encoded, err := cookieStore.Encode("login-challenge", challenge); err == nil {
			cookie := &http.Cookie{
				Name:  "login-challenge",
				Value: encoded,
				Path:  "/login",
			}
			http.SetCookie(w, cookie)
		}
	}

	http.Redirect(w, r, "/auth/login", http.StatusFound)
	return

	// Fetch consent information
	// loginRequest, response, err := client.GetLoginRequest(challenge)
	// if err != nil {
	// 	http.Error(w, errors.Wrap(err, "The login request endpoint does not respond").Error(), http.StatusBadRequest)
	// 	return
	// } else if response.StatusCode != http.StatusOK {
	// 	http.Error(w, errors.Wrapf(err, "Login request endpoint gave status code %d but expected %d", response.StatusCode, http.StatusOK).Error(), http.StatusBadRequest)
	// 	return
	// }

	// // Is it a POST request?
	// if r.Method == "POST" {
	// 	// Parse the form
	// 	if err := r.ParseForm(); err != nil {
	// 		http.Error(w, errors.Wrap(err, "Could not parse form").Error(), http.StatusBadRequest)
	// 		return
	// 	}

	// 	// Check the user's credentials
	// 	if r.Form.Get("username") != "buzz" || r.Form.Get("password") != "lightyear" {
	// 		http.Error(w, "Provided credentials are wrong, try buzz:lightyear", http.StatusBadRequest)
	// 		return
	// 	}

	// 	// Let's create a session where we store the user id. We can ignore errors from the session store
	// 	// as it will always return a session!
	// 	session, _ := store.Get(r, sessionName)
	// 	session.Values["user"] = "buzz-lightyear"

	// 	// Store the session in the cookie
	// 	if err := store.Save(r, w, session); err != nil {
	// 		http.Error(w, errors.Wrap(err, "Could not persist cookie").Error(), http.StatusBadRequest)
	// 		return
	// 	}

	// 	// Redirect the user back to the consent endpoint. In a normal app, you would probably
	// 	// add some logic here that is triggered when the user actually performs authentication and is not
	// 	// part of the consent flow.
	// 	// http.Redirect(w, r, "/consent?consent="+consentRequestID, http.StatusFound)

	// 	// Ok, now we accept the consent request.
	// 	request, response, err := client.AcceptLoginRequest(challenge, swagger.AcceptLoginRequest{
	// 		// The subject is a string, usually the user id.
	// 		Acr: "test@test.test",
	// 		Remember: true,
	// 		RememberFor: 3600,
	// 	})
	// 	if err != nil {
	// 		http.Error(w, errors.Wrap(err, "The accept login request endpoint encountered a network error").Error(), http.StatusInternalServerError)
	// 		return
	// 	} else if response.StatusCode != http.StatusNoContent {
	// 		http.Error(w, errors.Wrapf(err, "Accept login request endpoint gave status code %d but expected %d", response.StatusCode, http.StatusNoContent).Error(), http.StatusInternalServerError)
	// 		return
	// 	}

	// 	// Redirect the user back to hydra, and append the consent response! If the user denies request you can
	// 	// either handle the error in the authentication endpoint, or redirect the user back to the original application
	// 	// with:
	// 	//
	// 	//   response, err := client.RejectOAuth2ConsentRequest(consentRequestId, payload)
	// 	fmt.Println(request.RedirectTo)
	// 	http.Redirect(w, r, request.RedirectTo, http.StatusFound)
	// 	return
	// }

	// // It's a get request, so let's render the template
	// renderTemplate(w, "login.html", challenge)
}

// Once the user has given their consent, we will hit this endpoint. Again,
// this is not something that would be included in a traditional consent app,
// but we added it so you can see the data once the consent flow is done.
func handleCallback(w http.ResponseWriter, r *http.Request) {
	// in the real world you should check the state query parameter, but this is omitted for brevity reasons.

	// Exchange the access code for an access (and optionally) a refresh token
	// token, err := client.GetOAuth2Config().Exchange(context.Background(), r.URL.Query().Get("code"))
	// if err != nil {
	// 	http.Error(w, errors.Wrap(err, "Could not exhange token").Error(), http.StatusBadRequest)
	// 	return
	// }

	// Render the output
	// renderTemplate(w, "callback.html", struct {
	// 	*oauth2.Token
	// 	IDToken interface{}
	// }{
	// 	Token:   token,
	// 	IDToken: token.Extra("id_token"),
	// })
}

// authenticated checks if our cookie store has a user stored and returns the
// user's name, or an empty string if the user is not yet authenticated.
func authenticated(r *http.Request) string {
	session, _ := store.Get(r, sessionName)
	if u, ok := session.Values["user"]; !ok {
		return ""
	} else if user, ok := u.(string); !ok {
		return ""
	} else {
		return user
	}
}

// renderTemplate is a convenience helper for rendering templates.
func renderTemplate(w http.ResponseWriter, id string, d interface{}) bool {
	if t, err := template.New(id).ParseFiles("./templates/" + id); err != nil {
		http.Error(w, errors.Wrap(err, "Could not render template").Error(), http.StatusInternalServerError)
		return false
	} else if err := t.Execute(w, d); err != nil {
		http.Error(w, errors.Wrap(err, "Could not render template").Error(), http.StatusInternalServerError)
		return false
	}
	return true
}
