package main

import (
	"context"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
	// "io/ioutil"
	"encoding/base64"
	// "path/filepath"

	"github.com/volatiletech/authboss"
	_ "github.com/volatiletech/authboss/auth"
	// "github.com/volatiletech/authboss/confirm"
	"github.com/volatiletech/authboss/defaults"
	// "github.com/volatiletech/authboss/lock"
	_ "github.com/volatiletech/authboss/logout"
	// aboauth "github.com/volatiletech/authboss/oauth2"
	_ "github.com/volatiletech/authboss/recover"
	_ "github.com/volatiletech/authboss/register"
	"github.com/volatiletech/authboss/remember"

	"github.com/volatiletech/authboss-clientstate"
	"github.com/volatiletech/authboss-renderer"

	// httptransport "github.com/go-openapi/runtime/client"
	// "github.com/go-openapi/strfmt"

	// "github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/joho/godotenv"
	"github.com/pkg/errors"
	// negronilogrus "github.com/meatballhat/negroni-logrus"
	// "github.com/ory/common/env"
	// "github.com/gorilla/securecookie"
	hydraclient "github.com/ory/hydra/sdk/go/hydra/client"
	hydra "github.com/ory/hydra/sdk/go/hydra/client/admin"
	// ketoclient "github.com/ory/keto/sdk/go/keto/client"
	keto "github.com/ory/keto/sdk/go/keto/client/engines"
	// "github.com/urfave/negroni"
	"github.com/aarondl/tpl"
	"github.com/gorilla/schema"
	"github.com/graph-gophers/graphql-go"
	// "github.com/graph-gophers/graphql-go/relay"
	// "github.com/justinas/alice"
	"github.com/go-chi/chi"
	"github.com/justinas/nosurf"
	// "github.com/rs/cors"
	// "--skip-tls-verify", "--endpoint
	opentracing "github.com/opentracing/opentracing-go"
	jaeger "github.com/uber/jaeger-client-go"
	config "github.com/uber/jaeger-client-go/config"
	"gitlab.com/empowerlab/stack/lib-go/lib"
	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libdata/drivers/memory"
	"gitlab.com/empowerlab/stack/lib-go/libdata/drivers/postgres"
	"gitlab.com/empowerlab/stack/lib-go/libgrpc"
	"gitlab.com/empowerlab/stack/lib-go/liborm"

	_ "gitlab.com/empowerlab/stack/login/gen/grpc/server"
	gengrpc "gitlab.com/empowerlab/stack/login/gen/grpc/server"
	genorm "gitlab.com/empowerlab/stack/login/gen/orm"
	_ "gitlab.com/empowerlab/stack/login/orm/custom"
)

// This store will be used to save user authentication
var store = sessions.NewCookieStore([]byte("something-very-secret-keep-it-safe"))

// The session is a unique session identifier
const sessionName = "authentication"

// This is the Hydra SDK
var client *hydra.Client
var clientKeto *keto.Client

// DBCluster todo
var DBCluster *libdata.Cluster

var graphqlSchema *graphql.Schema

// A state for performing the OAuth 2.0 flow. This is usually not part of a consent app, but in order for the demo
// to make sense, it performs the OAuth 2.0 authorize code flow.
var state = "demostatedemostatedemo"

var funcs = template.FuncMap{
	"formatDate": func(date time.Time) string {
		return date.Format("2006/01/02 03:04pm")
	},
	"yield": func() string { return "" },
}

var (
	ab        = authboss.New()
	database  = newStorer()
	templates = tpl.Must(tpl.Load("views", "views/partials", "layout.html.tpl", funcs))
	schemaDec = schema.NewDecoder()

	sessionStore abclientstate.SessionStorer
	cookieStore  abclientstate.CookieStorer
)

const (
	sessionCookieName = "ab_blog"
)

func auth(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		token := r.Header.Get("Authorization")
		// jwt, err := checkToken(token)
		// if err != nil {
		// 	fmt.Println(err)
		// }

		// nolint: golint
		next.ServeHTTP(w, r.WithContext(context.WithValue(
			ctx, "token", strings.Replace(token, "Bearer ", "", 1))))
	})
}

//go:generate go run gen/gen.go
//go:generate protoc -I=. -I=$GOPATH/src -I=$GOPATH/src/github.com/gogo/protobuf/protobuf --proto_path=./gen/orm/pb --gogofaster_out=plugins=grpc:./gen/orm/pb main.proto
//go:generate protoc -I=. -I=$GOPATH/src -I=$GOPATH/src/github.com/gogo/protobuf/protobuf --proto_path=./gen/grpc/pb --gogofaster_out=plugins=grpc:./gen/grpc/pb main.proto

func main() {
	var err error

	godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	cfg := &config.Configuration{
		Sampler: &config.SamplerConfig{
			Type:  "const",
			Param: 1,
		},
		Reporter: &config.ReporterConfig{
			LogSpans:          false,
			CollectorEndpoint: os.Getenv("JAEGER_ENDPOINT"),
		},
	}
	tracer, closer, err := cfg.New("login", config.Logger(jaeger.StdLogger))
	if err != nil {
		panic(fmt.Sprintf("ERROR: cannot init Jaeger: %v\n", err))
	}
	defer closer.Close()
	opentracing.SetGlobalTracer(tracer)

	// postgresDriver := &postgres.Driver{}
	// postgresInfo := libdata.ClusterInfo{
	// 	Driver: postgresDriver,
	// 	URL:    os.Getenv("DATABASE_URL"),
	// }
	// liborm.Cluster, err = libdata.InitDB(false, &postgresInfo, &postgresInfo, nil)
	// if err != nil {
	// 	log.Fatalf("Error initializing db %s", err.Error())
	// }
	liborm.Cluster.CacheDriver = &memory.Driver{}

	liborm.Cluster.Register(genorm.RoleDefinition.Model)
	liborm.Cluster.Register(genorm.AccountDefinition.Model)
	liborm.Cluster.Register(genorm.EventDefinition.Model)
	// liborm.Cluster.Register(genorm.DataDefinition.Model)
	// libdata.EventDriver = postgresDriver

	// graphqlSchema = graphql.MustParseSchema(SchemaContent, &Resolver{})

	// Initialize the hydra SDK. The defaults work if you started hydra as described in the README.md
	// client = hydra.New(
	// 	httptransport.New(os.Getenv("HYDRA_CLUSTER_URL"), "/", nil),
	// 	strfmt.Default)
	adminURL, err := url.Parse(os.Getenv("HYDRA_CLUSTER_URL"))
	if err != nil {
		log.Fatal(err)
	}
	oryhydra := hydraclient.NewHTTPClientWithConfig(nil, &hydraclient.TransportConfig{Schemes: []string{adminURL.Scheme}, Host: adminURL.Host, BasePath: adminURL.Path})
	client = oryhydra.Admin

	// 	&hydra. .Configuration{
	// 	ClientID:     os.Getenv("HYDRA_CLIENT_ID"),
	// 	ClientSecret: os.Getenv("HYDRA_CLIENT_SECRET"),
	// 	AdminURL:     os.Getenv("HYDRA_CLUSTER_URL"),
	// 	PublicURL:    os.Getenv("HYDRA_CLUSTER_URL"),
	// 	Scopes:       []string{"hydra.consent"},
	// })
	// if err != nil {
	// 	log.Fatalf("Unable to connect to the Hydra SDK because %s", err)
	// }

	// ketoURL, err := url.Parse(os.Getenv("KETO_CLUSTER_URL"))
	// if err != nil {
	// 	log.Fatal(err)
	// }
	// oryketo := ketoclient.NewHTTPClientWithConfig(nil, &ketoclient.TransportConfig{Schemes: []string{ketoURL.Scheme}, Host: ketoURL.Host, BasePath: ketoURL.Path})
	// clientKeto = oryketo.Engines
	// clientKeto = keto.New(
	// 	httptransport.New(os.Getenv("KETO_CLUSTER_URL"), "/", nil),
	// 	strfmt.Default)

	// NewCodeGenSDK(&keto.Configuration{
	// 	EndpointURL: os.Getenv("KETO_CLUSTER_URL"),
	// })
	// if err != nil {
	// 	log.Fatalf("Unable to connect to the Hydra SDK because %s", err)
	// }

	cookieStoreKey, _ := base64.StdEncoding.DecodeString(`NpEPi8pEjKVjLGJ6kYCS+VTCzi6BUuDzU0wrwXyf5uDPArtlofn2AG6aTMiPmN3C909rsEWMNqJqhIVPGP3Exg==`)
	sessionStoreKey, _ := base64.StdEncoding.DecodeString(`AbfYwmmt8UCwUuhd9qvfNA9UCuN1cVcKJN1ofbiky6xCyyBj20whe40rJa3Su0WOWLWcPpO1taqJdsEI/65+JA==`)
	cookieStore = abclientstate.NewCookieStorer(cookieStoreKey, nil)
	sessionStore = abclientstate.NewSessionStorer(sessionCookieName, sessionStoreKey, nil)

	ab.Config.Paths.RootURL = os.Getenv("LOGIN_CLUSTER_URL")
	ab.Config.Paths.AuthLoginOK = "/login"
	ab.Config.Paths.RegisterOK = "/login"
	ab.Config.Storage.Server = database
	ab.Config.Storage.SessionState = sessionStore
	ab.Config.Storage.CookieState = cookieStore
	// ab.OAuth2Storer = database
	// ab.MountPath = "/auth"
	ab.Config.Core.ViewRenderer = abrenderer.NewHTML("/auth", "ab_views")
	ab.Config.Core.MailRenderer = abrenderer.NewEmail("/auth", "ab_views")
	ab.Config.Core.Mailer = defaults.LogMailer{}

	ab.Config.Modules.LogoutMethod = "GET"
	ab.Config.Modules.RegisterPreserveFields = []string{"email", "name"}
	defaults.SetCore(&ab.Config, false, false)

	emailRule := defaults.Rules{
		FieldName: "email", Required: true,
		MatchError: "Must be a valid e-mail address",
		MustMatch:  regexp.MustCompile(`.*@.*\.[a-z]{1,}`),
	}
	passwordRule := defaults.Rules{
		FieldName: "password", Required: true,
		MinLength: 4,
	}
	nameRule := defaults.Rules{
		FieldName: "name", Required: true,
		MinLength: 2,
	}

	ab.Config.Core.BodyReader = defaults.HTTPBodyReader{
		ReadJSON: false,
		Rulesets: map[string][]defaults.Rules{
			"register":    {emailRule, passwordRule, nameRule},
			"recover_end": {passwordRule},
		},
		Confirms: map[string][]string{
			"register":    {"password", authboss.ConfirmPrefix + "password"},
			"recover_end": {"password", authboss.ConfirmPrefix + "password"},
		},
		Whitelist: map[string][]string{
			"register": []string{"email", "name", "password"},
		},
	}

	// oauthcreds := struct {
	// 	ClientID     string `toml:"client_id"`
	// 	ClientSecret string `toml:"client_secret"`
	// }{}

	// // Set up Google OAuth2 if we have credentials in the
	// // file oauth2.toml for it.
	// _, err := toml.DecodeFile("oauth2.toml", &oauthcreds)
	// if err == nil && len(oauthcreds.ClientID) != 0 && len(oauthcreds.ClientSecret) != 0 {
	// 	fmt.Println("oauth2.toml exists, configuring google oauth2")
	// 	ab.Config.Modules.OAuth2Providers = map[string]authboss.OAuth2Provider{
	// 		"google": authboss.OAuth2Provider{
	// 			OAuth2Config: &oauth2.Config{
	// 				ClientID:     oauthcreds.ClientID,
	// 				ClientSecret: oauthcreds.ClientSecret,
	// 				Scopes:       []string{`profile`, `email`},
	// 				Endpoint:     google.Endpoint,
	// 			},
	// 			FindUserDetails: aboauth.GoogleUserDetails,
	// 		},
	// 	}
	// } else if os.IsNotExist(err) {
	// 	fmt.Println("oauth2.toml doesn't exist, not registering oauth2 handling")
	// } else {
	// 	fmt.Println("error loading oauth2.toml:", err)
	// }

	// ab.LayoutDataMaker = layoutData
	// ab.LogWriter = os.Stdout

	// b, err := ioutil.ReadFile(filepath.Join("views", "layout.html.tpl"))
	// if err != nil {
	// 	panic(err)
	// }
	// ab.Layout = template.Must(template.New("layout").Funcs(funcs).Parse(string(b)))

	// ab.XSRFName = "csrf_token"
	// ab.XSRFMaker = func(_ http.ResponseWriter, r *http.Request) string {
	// 	return nosurf.Token(r)
	// }

	// ab.CookieStoreMaker = NewCookieStorer
	// ab.SessionStoreMaker = NewSessionStorer

	if err := ab.Init(); err != nil {
		// Handle error, don't let program continue to run
		log.Fatalln(err)
	}

	// Set up our router
	schemaDec.IgnoreUnknownKeys(true)

	// Set up a router and some routes
	mux := chi.NewRouter()
	// mux.Use(logger, nosurfing, ab.LoadClientStateMiddleware, remember.Middleware(ab), dataInjector)
	mux.Use(logger, ab.LoadClientStateMiddleware, remember.Middleware(ab))
	mux.HandleFunc("/consent", handleConsent)
	mux.HandleFunc("/login", handleLogin)
	mux.HandleFunc("/callback", handleCallback)
	mux.Handle("/graphiql", lib.GraphiQL{})
	// mux.Handle("/graphql", cors.Default().Handler(auth(&relay.Handler{Schema: graphqlSchema})))

	mux.Group(func(mux chi.Router) {
		mux.Use(authboss.ModuleListMiddleware(ab), nosurfing, dataInjector)
		mux.Get("/", index)
		mux.Mount("/auth", http.StripPrefix("/auth", ab.Config.Core.Router))
	})

	// Set up a request logger, useful for debugging
	// n := negroni.New()
	// n.Use(negronilogrus.NewMiddleware())
	// n.UseHandler(r)

	// Set up our middleware chain
	// stack := alice.New(logger, nosurfing, ab.ExpireMiddleware).Then(r)
	// stack := alice.New(logger, ab.ExpireMiddleware).Then(r)

	gengrpc.NewServer(tracer)

	if len(os.Args) > 1 {
		if os.Args[1] == "migrate" {
			err = liborm.Cluster.InitSingleTenant(context.Background())
			if err != nil {
				fmt.Println(errors.Wrap(err, "Couldn't initialize single tenant"))
			}
			return
		}
	}

	go func() {
		log.Println("Listening on :" + os.Getenv("PORT_GRPC"))
		port, _ := strconv.Atoi(os.Getenv("PORT_GRPC"))

		server := libgrpc.Server{}
		server.Listen(port)
	}()

	// Start http server
	log.Println("Listening on :" + os.Getenv("LOGIN_AUTO_DEPLOY_SERVICE_PORT"))
	http.ListenAndServe(":"+os.Getenv("LOGIN_AUTO_DEPLOY_SERVICE_PORT"), mux)

}

func dataInjector(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		data := layoutData(w, &r)
		r = r.WithContext(context.WithValue(r.Context(), authboss.CTXKeyData, data))
		handler.ServeHTTP(w, r)
	})
}

// layoutData is passing pointers to pointers be able to edit the current pointer
// to the request. This is still safe as it still creates a new request and doesn't
// modify the old one, it just modifies what we're pointing to in our methods so
// we're able to skip returning an *http.Request everywhere
func layoutData(w http.ResponseWriter, r **http.Request) authboss.HTMLData {
	currentUserName := ""
	userInter, err := ab.LoadCurrentUser(r)
	if userInter != nil && err == nil {
		currentUserName = userInter.(*User).Name
	}

	return authboss.HTMLData{
		"loggedin":          userInter != nil,
		"current_user_name": currentUserName,
		"csrf_token":        nosurf.Token(*r),
		"flash_success":     authboss.FlashSuccess(w, *r),
		"flash_error":       authboss.FlashError(w, *r),
	}
}

func index(w http.ResponseWriter, r *http.Request) {
	mustRender(w, r, "index", authboss.HTMLData{})
}

func mustRender(w http.ResponseWriter, r *http.Request, name string, data authboss.HTMLData) {
	// We've sort of hijacked the authboss mechanism for providing layout data
	// for our own purposes. There's nothing really wrong with this but it looks magical
	// so here's a comment.
	var current authboss.HTMLData
	dataIntf := r.Context().Value(authboss.CTXKeyData)
	if dataIntf == nil {
		current = authboss.HTMLData{}
	} else {
		current = dataIntf.(authboss.HTMLData)
	}

	current.MergeKV("csrf_token", nosurf.Token(r))
	current.Merge(data)

	err := templates.Render(w, name, current)
	if err == nil {
		return
	}

	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(http.StatusInternalServerError)
	fmt.Fprintln(w, "Error occurred rendering template:", err)
}

func redirect(w http.ResponseWriter, r *http.Request, path string) {
	http.Redirect(w, r, path, http.StatusFound)
}
