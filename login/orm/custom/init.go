package custom

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/liborm"

	"gitlab.com/empowerlab/stack/login/gen/orm"
	pborm "gitlab.com/empowerlab/stack/login/gen/orm/pb"
)

func init() {
	libdata.PostInitTenant = func(ctx context.Context) error {

		tx, err := liborm.Cluster.BeginTransaction(ctx, &libdata.Tenant{}, true)
		if err != nil {
			return errors.Wrap(err, "Couldn't begin transaction")
		}
		defer func() {
			err = tx.CommitTransaction(ctx, err)
		}()

		accountPool := orm.AccountDefinition.NewPool(&liborm.Env{
			Context:     ctx,
			Transaction: tx,
		})
		accounts, err := accountPool.Select([]*libdata.Filter{{
			Field: "name", Operator: "=", Operande: "admin",
		}}, nil, nil, nil)
		if err != nil {
			return errors.Wrap(err, "Couldn't create storage")
		}

		if len(accounts.Slice()) < 1 {
			_, err := accountPool.Create([]*pborm.CreateAccountRequest{{
				Account: &pborm.Account{
					Name:     "admin",
					Email:    "admin",
					Password: "admin",
				},
			}})
			if err != nil {
				return errors.Wrap(err, "Couldn't create admin")
			}
		}

		return nil
	}
}
