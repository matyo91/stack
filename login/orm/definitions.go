package orm

import (
	// 	"context"
	"fmt"
	"log"
	"os"
	// 	"time"

	// 	"github.com/pkg/errors"
	"github.com/joho/godotenv"
	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libdata/drivers/postgres"
	"gitlab.com/empowerlab/stack/lib-go/libdata/fields"
	"gitlab.com/empowerlab/stack/lib-go/libdata/specials"
	"gitlab.com/empowerlab/stack/lib-go/liborm"
	// 	"gitlab.com/empowerlab/stack/lib-go/libutils"
)

var Definitions *liborm.Definitions

func init() {

	err := godotenv.Load()
	if err != nil {
		fmt.Println("Error loading .env file")
	}

	fmt.Println(os.Getenv("DATABASE_URL"))
	postgresDriver := &postgres.Driver{}
	postgresInfo := &libdata.ClusterInfo{
		Driver: postgresDriver,
		URL:    os.Getenv("DATABASE_URL"),
	}
	liborm.Cluster, err = libdata.InitDB(false, postgresInfo, postgresInfo, nil)
	if err != nil {
		log.Fatalf("Error initializing db %s", err.Error())
	}

	Definitions = &liborm.Definitions{
		Repository: "gitlab.com/empowerlab/stack/login",
	}

	Definitions.Register(&liborm.Definition{
		Model: &libdata.ModelDefinition{
			Cluster: liborm.Cluster,
			Name:    "account",
			Fields: []libdata.Field{
				&fields.Text{Name: "name", Required: true},
				&fields.Text{Name: "email", Required: true},
				&fields.Text{Name: "password", Required: true},
				&fields.One2many{Name: "roles", Reference: "role", InverseField: "accountUUID", String: "Roles"},
			},
			Datetime:    true,
			CanAssignID: false,
		},
	})

	Definitions.Register(&liborm.Definition{
		Model: &libdata.ModelDefinition{
			Cluster: liborm.Cluster,
			Name:    "role",
			Fields: []libdata.Field{
				&fields.Many2one{Name: "accountUUID", Reference: "account", Required: true},
				&fields.Text{Name: "name", Required: true},
			},
			Datetime:    true,
			CanAssignID: false,
		},
	})

	specials.EventModel.Cluster = liborm.Cluster
	libdata.EventDriver = postgresDriver
	Definitions.Register(&liborm.Definition{
		Model: specials.EventModel,
	})
}
