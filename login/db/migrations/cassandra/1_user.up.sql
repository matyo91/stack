CREATE TABLE IF NOT EXISTS user (
 	id 				text,
 	name 			text,
 	email 			text,
 	password		text,
	created_at timestamp,
	updated_at timestamp,
    PRIMARY KEY(id)
);