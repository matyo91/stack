CREATE TABLE IF NOT EXISTS "user" (
	id 				SERIAL PRIMARY KEY,
 	name 			TEXT NOT NULL,
 	email 			TEXT NOT NULL,
 	password		TEXT NOT NULL,
	created_at          TIMESTAMP NOT NULL DEFAULT NOW(),
	updated_at          TIMESTAMP NOT NULL DEFAULT NOW()
);