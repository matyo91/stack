package main

import (
	"gitlab.com/empowerlab/stack/login/graphql"
	"gitlab.com/empowerlab/stack/login/grpc"
	"gitlab.com/empowerlab/stack/login/orm"
)

func main() {
	orm.Definitions.GenProtos()
	orm.Definitions.Gen()
	grpc.Definitions.GenProtos()
	grpc.Definitions.GenServer()
	grpc.Definitions.GenClient()
	graphql.Definitions.Gen()
}
