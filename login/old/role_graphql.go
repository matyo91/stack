package old

// import (
// 	graphql "github.com/graph-gophers/graphql-go"
// 	"github.com/pkg/errors"
// 	"golang.org/x/net/context"

// 	"gitlab.com/empowerlab/stack/lib-go/libgraphql"
// )

// type roleGraphqlDefinition struct {
// 	Graphql *libgraphql.Definition
// }

// // ServiceChildDefinition todo
// var RoleGraphqlDefinition = roleGraphqlDefinition{
// 	Graphql: &libgraphql.Definition{
// 		Model: RoleDefinition.Model,
// 	},
// }

// // RoleResolver todo
// type RoleResolver struct {
// 	p *Role
// }

// // ID todo
// func (pr *RoleResolver) ID() graphql.ID {
// 	return graphql.ID(pr.p.ID)
// }

// type roleSearchInput struct {
// 	ID *[]graphql.ID
// }

// // Role todo
// // nolint: dupl, unparam
// func (r *Resolver) Role(ctx context.Context, args *struct {
// 	ID graphql.ID
// }) (*RoleResolver, error) {

// 	role, err := RoleGraphqlDefinition.Graphql.Get(ctx, DBCluster, "", args.ID, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't get role")
// 	}

// 	return &RoleResolver{role.(*Role)}, nil
// }

// // AllRolesMeta todo
// // nolint: golint, megacheck
// func (r *Resolver) AllRolesMeta(ctx context.Context, args *struct {
// 	Filter  *roleSearchInput
// 	First   *int32
// 	After   *graphql.ID
// 	OrderBy *string
// }) (*_QueryMetaResolver, error) {
// 	roles, err := r.AllRoles(ctx, args)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't get roles")
// 	}
// 	return &_QueryMetaResolver{count: int32(len(roles))}, nil
// }

// // AllRoles todo
// func (r *Resolver) AllRoles(ctx context.Context, args *struct {
// 	Filter  *roleSearchInput
// 	First   *int32
// 	After   *graphql.ID
// 	OrderBy *string
// }) ([]*RoleResolver, error) {

// 	roles, err := RoleGraphqlDefinition.Graphql.Select(
// 		ctx, DBCluster, "", args.Filter, []string{}, args.First, args.After, args.OrderBy)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't get role")
// 	}

// 	var resolvers []*RoleResolver
// 	for _, r := range roles.([]interface{}) {
// 		resolvers = append(resolvers, &RoleResolver{r.(*Role)})
// 	}

// 	return resolvers, nil
// }

// // CreateRole todo
// // nolint: megacheck
// func (r *Resolver) CreateRole(ctx context.Context, args *struct {
// 	ID graphql.ID
// }) (rr *RoleResolver, err error) {

// 	record := Role{}
// 	role, err := RoleGraphqlDefinition.Graphql.Create(ctx, DBCluster, "", args, &record)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't create role")
// 	}
// 	// roles, _, err := roleDefinition.ModelDefinition.GraphqlCreate(ctx, args, record)
// 	// if err != nil {
// 	// 	return nil, errors.Wrap(err, "Couldn't insert role")
// 	// }

// 	return &RoleResolver{role.(*Role)}, nil
// }

// // UpdateRole todo
// // nolint: megacheck
// func (r *Resolver) UpdateRole(ctx context.Context, args *struct {
// 	ID graphql.ID
// }) (*RoleResolver, error) {

// 	record := Role{}
// 	role, err := RoleGraphqlDefinition.Graphql.Update(ctx, DBCluster, "", args, &record, []string{}, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't update role")
// 	}

// 	return &RoleResolver{role.(*Role)}, nil
// }

// // DeleteRole todo
// // nolint: dupl
// func (r *Resolver) DeleteRole(ctx context.Context, args *struct {
// 	ID graphql.ID
// }) (*RoleResolver, error) {

// 	role, err := RoleGraphqlDefinition.Graphql.Delete(ctx, DBCluster, "", args.ID, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't update role")
// 	}

// 	return &RoleResolver{role.(*Role)}, nil
// }
