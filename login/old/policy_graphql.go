package old

// import (
// 	"strings"

// 	graphql "github.com/graph-gophers/graphql-go"
// 	"github.com/pkg/errors"
// 	"golang.org/x/net/context"

// 	"gitlab.com/empowerlab/stack/lib-go/libgraphql"
// )

// type policyGraphqlDefinition struct {
// 	Graphql *libgraphql.Definition
// }

// // ServiceChildDefinition todo
// var PolicyGraphqlDefinition = policyGraphqlDefinition{
// 	Graphql: &libgraphql.Definition{
// 		Model: PolicyDefinition.Model,
// 	},
// }

// // PolicyResolver todo
// type PolicyResolver struct {
// 	p *Policy
// }

// // ID todo
// func (pr *PolicyResolver) ID() graphql.ID {
// 	return graphql.ID(pr.p.ID)
// }

// // ID todo
// func (pr *PolicyResolver) Subjects() string {
// 	return strings.Join(pr.p.Subjects, ",")
// }

// // ID todo
// func (pr *PolicyResolver) Actions() string {
// 	return strings.Join(pr.p.Actions, ",")
// }

// // ID todo
// func (pr *PolicyResolver) Resources() string {
// 	return strings.Join(pr.p.Resources, ",")
// }

// type policySearchInput struct {
// 	ID *[]graphql.ID
// }

// // Policy todo
// // nolint: dupl, unparam
// func (r *Resolver) Policy(ctx context.Context, args *struct {
// 	ID graphql.ID
// }) (*PolicyResolver, error) {

// 	policy, err := PolicyGraphqlDefinition.Graphql.Get(ctx, DBCluster, "", args.ID, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't get policy")
// 	}

// 	return &PolicyResolver{policy.(*Policy)}, nil
// }

// // AllPolicysMeta todo
// // nolint: golint, megacheck
// func (r *Resolver) AllPoliciesMeta(ctx context.Context, args *struct {
// 	Filter  *policySearchInput
// 	First   *int32
// 	After   *graphql.ID
// 	OrderBy *string
// }) (*_QueryMetaResolver, error) {
// 	policys, err := r.AllPolicies(ctx, args)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't get policys")
// 	}
// 	return &_QueryMetaResolver{count: int32(len(policys))}, nil
// }

// // AllPolicys todo
// func (r *Resolver) AllPolicies(ctx context.Context, args *struct {
// 	Filter  *policySearchInput
// 	First   *int32
// 	After   *graphql.ID
// 	OrderBy *string
// }) ([]*PolicyResolver, error) {

// 	policys, err := PolicyGraphqlDefinition.Graphql.Select(
// 		ctx, DBCluster, "", args.Filter, []string{}, args.First, args.After, args.OrderBy)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't get policy")
// 	}

// 	var resolvers []*PolicyResolver
// 	for _, r := range policys.([]interface{}) {
// 		resolvers = append(resolvers, &PolicyResolver{r.(*Policy)})
// 	}

// 	return resolvers, nil
// }

// // CreatePolicy todo
// // nolint: megacheck
// func (r *Resolver) CreatePolicy(ctx context.Context, args *struct {
// 	Subjects  string
// 	Actions   string
// 	Resources string
// }) (rr *PolicyResolver, err error) {

// 	fields := struct {
// 		Subjects  []string
// 		Actions   []string
// 		Resources []string
// 	}{
// 		Subjects:  strings.Split(args.Subjects, ","),
// 		Actions:   strings.Split(args.Actions, ","),
// 		Resources: strings.Split(args.Resources, ","),
// 	}

// 	record := Policy{}
// 	policy, err := PolicyGraphqlDefinition.Graphql.Create(ctx, DBCluster, "", &fields, &record)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't create policy")
// 	}
// 	// policys, _, err := policyDefinition.ModelDefinition.GraphqlCreate(ctx, args, record)
// 	// if err != nil {
// 	// 	return nil, errors.Wrap(err, "Couldn't insert policy")
// 	// }

// 	return &PolicyResolver{policy.(*Policy)}, nil
// }

// // UpdatePolicy todo
// // nolint: megacheck
// func (r *Resolver) UpdatePolicy(ctx context.Context, args *struct {
// 	ID        graphql.ID
// 	Subjects  *string
// 	Actions   *string
// 	Resources *string
// }) (*PolicyResolver, error) {

// 	fields := struct {
// 		Subjects  *[]string
// 		Actions   *[]string
// 		Resources *[]string
// 	}{}

// 	if args.Subjects != nil {
// 		split := strings.Split(*args.Subjects, ",")
// 		fields.Actions = &split
// 	}
// 	if args.Actions != nil {
// 		split := strings.Split(*args.Actions, ",")
// 		fields.Actions = &split
// 	}
// 	if args.Resources != nil {
// 		split := strings.Split(*args.Resources, ",")
// 		fields.Actions = &split
// 	}

// 	record := Policy{}
// 	policy, err := PolicyGraphqlDefinition.Graphql.Update(ctx, DBCluster, "", args, &record, []string{}, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't update policy")
// 	}

// 	return &PolicyResolver{policy.(*Policy)}, nil
// }

// // DeletePolicy todo
// // nolint: dupl
// func (r *Resolver) DeletePolicy(ctx context.Context, args *struct {
// 	ID graphql.ID
// }) (*PolicyResolver, error) {

// 	policy, err := PolicyGraphqlDefinition.Graphql.Delete(ctx, DBCluster, "", args.ID, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't update policy")
// 	}

// 	return &PolicyResolver{policy.(*Policy)}, nil
// }
