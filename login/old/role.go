package old

// import (
// 	// "fmt"
// 	"context"
// 	// "os"
// 	"time"

// 	"github.com/ory/keto/sdk/go/keto/swagger"

// 	// "github.com/volatiletech/authboss"
// 	// "github.com/davecgh/go-spew/spew"
// 	"github.com/pkg/errors"
// 	"gitlab.com/empowerlab/stack/lib-go/libdata"
// 	"gitlab.com/empowerlab/stack/lib-go/libmodel"
// 	"gitlab.com/empowerlab/stack/lib-go/libutils"
// )

// // Role todo
// type Role struct {
// 	ID        string    `json:"id" db:"id"`
// 	CreatedAt time.Time `json:"createdAt" db:"created_at"`
// 	UpdatedAt time.Time `json:"updatedAt" db:"updated_at"`
// }

// // GetID todo
// func (t *Role) GetID() string {
// 	return t.ID
// }

// // GetCreatedAt todo
// func (t *Role) GetCreatedAt() time.Time {
// 	return t.CreatedAt
// }

// // GetUpdatedAt todo
// func (t *Role) GetUpdatedAt() time.Time {
// 	return t.UpdatedAt
// }

// // RoleDefinition todo
// type roleDefinition struct {
// 	Model *libmodel.Definition
// }

// var RoleDefinition = roleDefinition{
// 	Model: &libmodel.Definition{
// 		DBInfo: &libdata.Model{
// 			Model:       "role",
// 			Datetime:    false,
// 			CanAssignID: true,
// 		},
// 	},
// }

// func init() {
// 	RoleDefinition.Model.Init()
// 	RoleDefinition.Model.Newer = RoleDefinition.newRecord
// 	RoleDefinition.Model.Getter = RoleDefinition.Getter
// 	RoleDefinition.Model.Selecter = RoleDefinition.Selecter
// 	RoleDefinition.Model.Creater = RoleDefinition.Creater
// 	RoleDefinition.Model.Updater = RoleDefinition.Updater
// 	RoleDefinition.Model.Deleter = RoleDefinition.Deleter
// }
// func (d *roleDefinition) newRecord() interface{} {
// 	return &Role{}
// }

// // Get todo
// func (d *roleDefinition) Getter(
// 	ctx context.Context, tx *libdata.Transaction, roleID string, with []string) (interface{}, error) {

// 	role, _, err := clientKeto.GetRole(roleID)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't get role")
// 	}

// 	return &Role{
// 		ID: role.Id,
// 	}, nil

// }

// // Get todo
// func (d *roleDefinition) Get(
// 	ctx context.Context, tx *libdata.Transaction, roleID string) (*Role, error) {

// 	record, err := d.Getter(ctx, tx, roleID, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't get role")
// 	}

// 	return record.(*Role), nil
// }

// // Select todo
// func (d *roleDefinition) Selecter(
// 	ctx context.Context, tx *libdata.Transaction, filters []*libdata.Filter,
// 	with []string, limit *uint, after *string, orderByArg *string,
// ) ([]interface{}, map[string]interface{}, error) {

// 	roles, _, err := clientKeto.ListRoles("", 0, 0)
// 	if err != nil {
// 		return nil, nil, errors.Wrap(err, "Couldn't select roles")
// 	}

// 	var records []interface{}
// 	recordByIds := map[string]interface{}{}
// 	for _, role := range roles {
// 		record := &Role{
// 			ID: role.Id,
// 		}
// 		records = append(records, record)
// 		recordByIds[record.ID] = record
// 	}
// 	return records, recordByIds, nil
// }

// // Select todo
// func (d *roleDefinition) Select(
// 	ctx context.Context, tx *libdata.Transaction, filters []*libdata.Filter,
// 	limit *uint, after *string, orderByArg *string,
// ) ([]*Role, map[string]*Role, error) {
// 	records, recordByIds, err := d.Selecter(
// 		ctx, tx, filters, []string{}, limit, after, orderByArg)
// 	if err != nil {
// 		return nil, nil, errors.Wrap(err, "Couldn't get roles")
// 	}
// 	var roles []*Role
// 	for _, record := range records {
// 		roles = append(roles, record.(*Role))
// 	}
// 	roleByIds := map[string]*Role{}
// 	for id, record := range recordByIds {
// 		roleByIds[id] = record.(*Role)
// 	}
// 	return roles, roleByIds, nil
// }

// func (d *roleDefinition) Creater(ctx context.Context, tx *libdata.Transaction, roles []interface{}) (
// 	[]interface{}, map[string]interface{}, error) {

// 	var records []interface{}
// 	recordByIds := map[string]interface{}{}
// 	for _, role := range roles {
// 		role, _, err := clientKeto.CreateRole(swagger.Role{
// 			Id: role.(*Role).ID,
// 		})
// 		if err != nil {
// 			return nil, nil, errors.Wrap(err, "Couldn't create role")
// 		}

// 		record, err := d.Get(ctx, tx, role.Id)
// 		if err != nil {
// 			return nil, nil, errors.Wrap(err, "Couldn't get role")
// 		}

// 		records = append(records, record)
// 		recordByIds[record.ID] = record
// 	}

// 	return records, recordByIds, nil
// }

// // Create todo
// func (d *roleDefinition) Create(ctx context.Context, tx *libdata.Transaction, roles []*Role) (
// 	[]*Role, map[string]*Role, error) {

// 	records, recordByIds, err := d.Creater(ctx, tx, libutils.InterfaceSlice(roles))
// 	if err != nil {
// 		return nil, nil, errors.Wrap(err, "Couldn't create role")
// 	}
// 	roles = []*Role{}
// 	for _, record := range records {
// 		roles = append(roles, record.(*Role))
// 	}
// 	roleByIds := map[string]*Role{}
// 	for id, record := range recordByIds {
// 		roleByIds[id] = record.(*Role)
// 	}

// 	return roles, roleByIds, nil
// }

// // Update todo
// func (d *roleDefinition) Updater(
// 	ctx context.Context, tx *libdata.Transaction, filters []*libdata.Filter, role interface{}, fields []string, with []string,
// ) (map[string]interface{}, error) {
// 	return nil, nil
// }

// // Update todo
// func (d *roleDefinition) Update(
// 	ctx context.Context, tx *libdata.Transaction, filters []*libdata.Filter, role *Role, fields []string,
// ) (map[string]*Role, error) {

// 	recordByIds, err := d.Updater(
// 		ctx, tx, filters, role, fields, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't update")
// 	}
// 	roleByIds := map[string]*Role{}
// 	for id, record := range recordByIds {
// 		roleByIds[id] = record.(*Role)
// 	}

// 	return roleByIds, nil
// }

// // Delete todo
// func (d *roleDefinition) Deleter(
// 	ctx context.Context, tx *libdata.Transaction, filters []*libdata.Filter, with []string) (map[string]interface{}, error) {

// 	recordByIds := map[string]interface{}{}
// 	for _, filter := range filters {
// 		if filter.Field == "id" {
// 			record, err := d.Get(ctx, tx, filter.Operande.(string))
// 			if err != nil {
// 				return nil, errors.Wrap(err, "Couldn't get role")
// 			}
// 			recordByIds[record.ID] = record

// 			_, err = clientKeto.DeleteRole(record.ID)
// 			if err != nil {
// 				return nil, errors.Wrap(err, "Couldn't delete role")
// 			}
// 		}
// 	}
// 	return recordByIds, nil
// }

// // Delete todo
// func (d *roleDefinition) Delete(
// 	ctx context.Context, tx *libdata.Transaction, filters []*libdata.Filter) (map[string]*Role, error) {

// 	recordByIds, err := d.Deleter(
// 		ctx, tx, filters, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't update")
// 	}
// 	roleByIds := map[string]*Role{}
// 	for id, record := range recordByIds {
// 		roleByIds[id] = record.(*Role)
// 	}

// 	return roleByIds, nil
// }

// // type storer struct {
// // 	Roles  map[string]Role
// // 	Tokens map[string][]string
// // }

// // func newStorer() *storer {
// // 	return &storer{
// // 		// Roles: map[string]Role{},
// // 		Tokens: make(map[string][]string),
// // 	}
// // }

// // func (s storer) Create(key string, attr authboss.Attributes) error {
// // 	fmt.Printf("key %+v\n", key)
// // 	fmt.Printf("attrs %+v\n", attr)

// // 	tx, err := db.BeginTransaction(os.Getenv("DATABASE_NAME"))
// // 	if err != nil {
// // 		return errors.Wrap(err, "Couldn't begin transaction")
// // 	}
// // 	defer func() {
// // 		err = db.EndTransaction(tx, err)
// // 	}()

// // 	_, _, err = roleDefinition.Create(context.TODO(), tx, []*Role{{
// // 		Name: attr["email"].(string),
// // 		Email: attr["email"].(string),
// // 		Password: attr["password"].(string),
// // 	}})
// // 	if err != nil {
// // 		return errors.Wrap(err, "Couldn't create role")
// // 	}
// // 	// var role Role
// // 	// if err := attr.Bind(&role, true); err != nil {
// // 	// 	return err
// // 	// }

// // 	// role.ID = nextRoleID
// // 	// nextRoleID++

// // 	// s.Roles[key] = role
// // 	// fmt.Println("Create")
// // 	// spew.Dump(s.Roles)
// // 	return nil
// // }

// // func (s storer) Put(key string, attr authboss.Attributes) error {
// // 	// return s.Create(key, attr)
// // 	return nil
// // }

// // func (s storer) Get(key string) (result interface{}, err error) {
// // 	tx, err := db.BeginTransaction(os.Getenv("DATABASE_NAME"))
// // 	if err != nil {
// // 		return nil, errors.Wrap(err, "Couldn't begin transaction")
// // 	}
// // 	defer func() {
// // 		err = db.EndTransaction(tx, err)
// // 	}()

// // 	roles, _, err := roleDefinition.Select(context.TODO(), tx, []*db.Filter{{
// // 		Field: "email", Operator: "=", Operande: key,
// // 	}}, nil, nil, nil)
// // 	if err != nil {
// // 		return nil, errors.Wrap(err, "Couldn't select roles")
// // 	}

// // 	if len(roles) < 1 {
// // 		return nil, authboss.ErrRoleNotFound
// // 	}
// // 	// role, ok := s.Roles[key]
// // 	// if !ok {
// // 	// 	return nil, authboss.ErrRoleNotFound
// // 	// }

// // 	// return &role, nil
// // 	// return &Role{
// // 	// 	ID: "test",
// // 	// 	Name: "test",
// // 	// 	Email: "test",
// // 	// 	Password: "test",
// // 	// }, nil
// // 	return roles[0], nil
// // }

// // func (s storer) AddToken(key, token string) error {
// // 	s.Tokens[key] = append(s.Tokens[key], token)
// // 	fmt.Println("AddToken")
// // 	spew.Dump(s.Tokens)
// // 	return nil
// // }

// // func (s storer) DelTokens(key string) error {
// // 	delete(s.Tokens, key)
// // 	fmt.Println("DelTokens")
// // 	spew.Dump(s.Tokens)
// // 	return nil
// // }

// // func (s storer) UseToken(givenKey, token string) error {
// // 	toks, ok := s.Tokens[givenKey]
// // 	if !ok {
// // 		return authboss.ErrTokenNotFound
// // 	}

// // 	for i, tok := range toks {
// // 		if tok == token {
// // 			toks[i], toks[len(toks)-1] = toks[len(toks)-1], toks[i]
// // 			s.Tokens[givenKey] = toks[:len(toks)-1]
// // 			return nil
// // 		}
// // 	}

// // 	return authboss.ErrTokenNotFound
// // }

// // func (s storer) ConfirmRole(tok string) (result interface{}, err error) {
// // 	// fmt.Println("==============", tok)

// // 	// for _, u := range s.Roles {
// // 	// 	if u.ConfirmToken == tok {
// // 	// 		return &u, nil
// // 	// 	}
// // 	// }

// // 	return nil, authboss.ErrRoleNotFound
// // }

// // func (s storer) RecoverRole(rec string) (result interface{}, err error) {
// // // 	for _, u := range s.Roles {
// // // 		if u.RecoverToken == rec {
// // // 			return &u, nil
// // // 		}
// // // 	}

// // 	return nil, authboss.ErrRoleNotFound
// // }
