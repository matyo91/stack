package old

// import (
// 	// "fmt"
// 	"context"
// 	// "os"
// 	"time"

// 	"github.com/ory/keto/sdk/go/keto/swagger"

// 	// "github.com/volatiletech/authboss"
// 	// "github.com/davecgh/go-spew/spew"
// 	"github.com/pkg/errors"
// 	"gitlab.com/empowerlab/stack/lib-go/libdata"
// 	"gitlab.com/empowerlab/stack/lib-go/libmodel"
// 	"gitlab.com/empowerlab/stack/lib-go/libutils"
// )

// // Policy todo
// type Policy struct {
// 	ID          string    `json:"id" db:"id"`
// 	Description string    `json:"description"`
// 	Subjects    []string  `json:"subjects"`
// 	Actions     []string  `json:"actions"`
// 	Resources   []string  `json:"resources"`
// 	CreatedAt   time.Time `json:"createdAt" db:"created_at"`
// 	UpdatedAt   time.Time `json:"updatedAt" db:"updated_at"`
// }

// // GetID todo
// func (t *Policy) GetID() string {
// 	return t.ID
// }

// // GetCreatedAt todo
// func (t *Policy) GetCreatedAt() time.Time {
// 	return t.CreatedAt
// }

// // GetUpdatedAt todo
// func (t *Policy) GetUpdatedAt() time.Time {
// 	return t.UpdatedAt
// }

// // PolicyDefinition todo
// type policyDefinition struct {
// 	Model *libmodel.Definition
// }

// var PolicyDefinition = policyDefinition{
// 	Model: &libmodel.Definition{
// 		DBInfo: &libdata.Model{
// 			Model:       "policy",
// 			Datetime:    false,
// 			CanAssignID: false,
// 		},
// 	},
// }

// func init() {
// 	PolicyDefinition.Model.Init()
// 	PolicyDefinition.Model.Newer = PolicyDefinition.newRecord
// 	PolicyDefinition.Model.Getter = PolicyDefinition.Getter
// 	PolicyDefinition.Model.Selecter = PolicyDefinition.Selecter
// 	PolicyDefinition.Model.Creater = PolicyDefinition.Creater
// 	PolicyDefinition.Model.Updater = PolicyDefinition.Updater
// 	PolicyDefinition.Model.Deleter = PolicyDefinition.Deleter
// }
// func (d *policyDefinition) newRecord() interface{} {
// 	return &Policy{}
// }

// // Get todo
// func (d *policyDefinition) Getter(
// 	ctx context.Context, tx *libdata.Transaction, policyID string, with []string) (interface{}, error) {

// 	policy, _, err := clientKeto.GetPolicy(policyID)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't get policy")
// 	}

// 	return &Policy{
// 		ID: policy.Id,
// 	}, nil

// }

// // Get todo
// func (d *policyDefinition) Get(
// 	ctx context.Context, tx *libdata.Transaction, policyID string) (*Policy, error) {

// 	record, err := d.Getter(ctx, tx, policyID, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't get policy")
// 	}

// 	return record.(*Policy), nil
// }

// // Select todo
// func (d *policyDefinition) Selecter(
// 	ctx context.Context, tx *libdata.Transaction, filters []*libdata.Filter,
// 	with []string, limit *uint, after *string, orderByArg *string,
// ) ([]interface{}, map[string]interface{}, error) {

// 	policys, _, err := clientKeto.ListPolicies(0, 0)
// 	if err != nil {
// 		return nil, nil, errors.Wrap(err, "Couldn't select policys")
// 	}

// 	var records []interface{}
// 	recordByIds := map[string]interface{}{}
// 	for _, policy := range policys {
// 		record := &Policy{
// 			ID: policy.Id,
// 		}
// 		records = append(records, record)
// 		recordByIds[record.ID] = record
// 	}
// 	return records, recordByIds, nil
// }

// // Select todo
// func (d *policyDefinition) Select(
// 	ctx context.Context, tx *libdata.Transaction, filters []*libdata.Filter,
// 	limit *uint, after *string, orderByArg *string,
// ) ([]*Policy, map[string]*Policy, error) {
// 	records, recordByIds, err := d.Selecter(
// 		ctx, tx, filters, []string{}, limit, after, orderByArg)
// 	if err != nil {
// 		return nil, nil, errors.Wrap(err, "Couldn't get policys")
// 	}
// 	var policys []*Policy
// 	for _, record := range records {
// 		policys = append(policys, record.(*Policy))
// 	}
// 	policyByIds := map[string]*Policy{}
// 	for id, record := range recordByIds {
// 		policyByIds[id] = record.(*Policy)
// 	}
// 	return policys, policyByIds, nil
// }

// func (d *policyDefinition) Creater(ctx context.Context, tx *libdata.Transaction, policys []interface{}) (
// 	[]interface{}, map[string]interface{}, error) {

// 	var records []interface{}
// 	recordByIds := map[string]interface{}{}
// 	for _, policy := range policys {
// 		policy, _, err := clientKeto.CreatePolicy(swagger.Policy{
// 			Description: policy.(*Policy).Description,
// 			Subjects:    policy.(*Policy).Subjects,
// 			Actions:     policy.(*Policy).Actions,
// 			Resources:   policy.(*Policy).Resources,
// 		})
// 		if err != nil {
// 			return nil, nil, errors.Wrap(err, "Couldn't create policy")
// 		}

// 		record, err := d.Get(ctx, tx, policy.Id)
// 		if err != nil {
// 			return nil, nil, errors.Wrap(err, "Couldn't get policy")
// 		}

// 		records = append(records, record)
// 		recordByIds[record.ID] = record
// 	}

// 	return records, recordByIds, nil
// }

// // Create todo
// func (d *policyDefinition) Create(ctx context.Context, tx *libdata.Transaction, policys []*Policy) (
// 	[]*Policy, map[string]*Policy, error) {

// 	records, recordByIds, err := d.Creater(ctx, tx, libutils.InterfaceSlice(policys))
// 	if err != nil {
// 		return nil, nil, errors.Wrap(err, "Couldn't create policy")
// 	}
// 	policys = []*Policy{}
// 	for _, record := range records {
// 		policys = append(policys, record.(*Policy))
// 	}
// 	policyByIds := map[string]*Policy{}
// 	for id, record := range recordByIds {
// 		policyByIds[id] = record.(*Policy)
// 	}

// 	return policys, policyByIds, nil
// }

// // Update todo
// func (d *policyDefinition) Updater(
// 	ctx context.Context, tx *libdata.Transaction, filters []*libdata.Filter, policy interface{}, fields []string, with []string,
// ) (map[string]interface{}, error) {
// 	return nil, nil
// }

// // Update todo
// func (d *policyDefinition) Update(
// 	ctx context.Context, tx *libdata.Transaction, filters []*libdata.Filter, policy *Policy, fields []string,
// ) (map[string]*Policy, error) {

// 	recordByIds, err := d.Updater(
// 		ctx, tx, filters, policy, fields, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't update")
// 	}
// 	policyByIds := map[string]*Policy{}
// 	for id, record := range recordByIds {
// 		policyByIds[id] = record.(*Policy)
// 	}

// 	return policyByIds, nil
// }

// // Delete todo
// func (d *policyDefinition) Deleter(
// 	ctx context.Context, tx *libdata.Transaction, filters []*libdata.Filter, with []string) (map[string]interface{}, error) {

// 	recordByIds := map[string]interface{}{}
// 	for _, filter := range filters {
// 		if filter.Field == "id" {
// 			record, err := d.Get(ctx, tx, filter.Operande.(string))
// 			if err != nil {
// 				return nil, errors.Wrap(err, "Couldn't get policy")
// 			}
// 			recordByIds[record.ID] = record

// 			_, err = clientKeto.DeletePolicy(record.ID)
// 			if err != nil {
// 				return nil, errors.Wrap(err, "Couldn't delete policy")
// 			}
// 		}
// 	}
// 	return recordByIds, nil
// }

// // Delete todo
// func (d *policyDefinition) Delete(
// 	ctx context.Context, tx *libdata.Transaction, filters []*libdata.Filter) (map[string]*Policy, error) {

// 	recordByIds, err := d.Deleter(
// 		ctx, tx, filters, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't update")
// 	}
// 	policyByIds := map[string]*Policy{}
// 	for id, record := range recordByIds {
// 		policyByIds[id] = record.(*Policy)
// 	}

// 	return policyByIds, nil
// }

// // type storer struct {
// // 	Policys  map[string]Policy
// // 	Tokens map[string][]string
// // }

// // func newStorer() *storer {
// // 	return &storer{
// // 		// Policys: map[string]Policy{},
// // 		Tokens: make(map[string][]string),
// // 	}
// // }

// // func (s storer) Create(key string, attr authboss.Attributes) error {
// // 	fmt.Printf("key %+v\n", key)
// // 	fmt.Printf("attrs %+v\n", attr)

// // 	tx, err := db.BeginTransaction(os.Getenv("DATABASE_NAME"))
// // 	if err != nil {
// // 		return errors.Wrap(err, "Couldn't begin transaction")
// // 	}
// // 	defer func() {
// // 		err = db.EndTransaction(tx, err)
// // 	}()

// // 	_, _, err = policyDefinition.Create(context.TODO(), tx, []*Policy{{
// // 		Name: attr["email"].(string),
// // 		Email: attr["email"].(string),
// // 		Password: attr["password"].(string),
// // 	}})
// // 	if err != nil {
// // 		return errors.Wrap(err, "Couldn't create policy")
// // 	}
// // 	// var policy Policy
// // 	// if err := attr.Bind(&policy, true); err != nil {
// // 	// 	return err
// // 	// }

// // 	// policy.ID = nextPolicyID
// // 	// nextPolicyID++

// // 	// s.Policys[key] = policy
// // 	// fmt.Println("Create")
// // 	// spew.Dump(s.Policys)
// // 	return nil
// // }

// // func (s storer) Put(key string, attr authboss.Attributes) error {
// // 	// return s.Create(key, attr)
// // 	return nil
// // }

// // func (s storer) Get(key string) (result interface{}, err error) {
// // 	tx, err := db.BeginTransaction(os.Getenv("DATABASE_NAME"))
// // 	if err != nil {
// // 		return nil, errors.Wrap(err, "Couldn't begin transaction")
// // 	}
// // 	defer func() {
// // 		err = db.EndTransaction(tx, err)
// // 	}()

// // 	policys, _, err := policyDefinition.Select(context.TODO(), tx, []*db.Filter{{
// // 		Field: "email", Operator: "=", Operande: key,
// // 	}}, nil, nil, nil)
// // 	if err != nil {
// // 		return nil, errors.Wrap(err, "Couldn't select policys")
// // 	}

// // 	if len(policys) < 1 {
// // 		return nil, authboss.ErrPolicyNotFound
// // 	}
// // 	// policy, ok := s.Policys[key]
// // 	// if !ok {
// // 	// 	return nil, authboss.ErrPolicyNotFound
// // 	// }

// // 	// return &policy, nil
// // 	// return &Policy{
// // 	// 	ID: "test",
// // 	// 	Name: "test",
// // 	// 	Email: "test",
// // 	// 	Password: "test",
// // 	// }, nil
// // 	return policys[0], nil
// // }

// // func (s storer) AddToken(key, token string) error {
// // 	s.Tokens[key] = append(s.Tokens[key], token)
// // 	fmt.Println("AddToken")
// // 	spew.Dump(s.Tokens)
// // 	return nil
// // }

// // func (s storer) DelTokens(key string) error {
// // 	delete(s.Tokens, key)
// // 	fmt.Println("DelTokens")
// // 	spew.Dump(s.Tokens)
// // 	return nil
// // }

// // func (s storer) UseToken(givenKey, token string) error {
// // 	toks, ok := s.Tokens[givenKey]
// // 	if !ok {
// // 		return authboss.ErrTokenNotFound
// // 	}

// // 	for i, tok := range toks {
// // 		if tok == token {
// // 			toks[i], toks[len(toks)-1] = toks[len(toks)-1], toks[i]
// // 			s.Tokens[givenKey] = toks[:len(toks)-1]
// // 			return nil
// // 		}
// // 	}

// // 	return authboss.ErrTokenNotFound
// // }

// // func (s storer) ConfirmPolicy(tok string) (result interface{}, err error) {
// // 	// fmt.Println("==============", tok)

// // 	// for _, u := range s.Policys {
// // 	// 	if u.ConfirmToken == tok {
// // 	// 		return &u, nil
// // 	// 	}
// // 	// }

// // 	return nil, authboss.ErrPolicyNotFound
// // }

// // func (s storer) RecoverPolicy(rec string) (result interface{}, err error) {
// // // 	for _, u := range s.Policys {
// // // 		if u.RecoverToken == rec {
// // // 			return &u, nil
// // // 		}
// // // 	}

// // 	return nil, authboss.ErrPolicyNotFound
// // }
