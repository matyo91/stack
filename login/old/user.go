package old

// import (
// 	// "fmt"
// 	"context"
// 	// "os"
// 	"time"

// 	// "github.com/volatiletech/authboss"
// 	// "github.com/davecgh/go-spew/spew"
// 	"github.com/pkg/errors"
// 	"gitlab.com/empowerlab/stack/lib-go/libdata"
// 	"gitlab.com/empowerlab/stack/lib-go/libmodel"
// 	"gitlab.com/empowerlab/stack/lib-go/libutils"
// )

// // User todo
// type User struct {
// 	ID        string    `json:"id" db:"id"`
// 	Name      string    `json:"name" db:"name"`
// 	Email     string    `json:"email" db:"email"`
// 	Password  string    `json:"password" db:"password"`
// 	CreatedAt time.Time `json:"createdAt" db:"created_at"`
// 	UpdatedAt time.Time `json:"updatedAt" db:"updated_at"`
// }

// // GetID todo
// func (t *User) GetID() string {
// 	return t.ID
// }

// // SetID todo
// func (a *User) SetID(id string) {
// 	a.ID = id
// }

// // GetCreatedAt todo
// func (t *User) GetCreatedAt() time.Time {
// 	return t.CreatedAt
// }

// // GetUpdatedAt todo
// func (t *User) GetUpdatedAt() time.Time {
// 	return t.UpdatedAt
// }

// // UserDefinition todo
// type userDefinition struct {
// 	Model *libmodel.Definition
// }

// var UserDefinition = userDefinition{
// 	Model: &libmodel.Definition{
// 		DBInfo: &libdata.Model{
// 			Model:       "user",
// 			Datetime:    true,
// 			CanAssignID: false,
// 		},
// 	},
// }

// func init() {
// 	UserDefinition.Model.Init()
// 	UserDefinition.Model.Newer = UserDefinition.newRecord
// }
// func (d *userDefinition) newRecord() interface{} {
// 	return &User{}
// }

// // Get todo
// func (d *userDefinition) Get(
// 	ctx context.Context, tx *libdata.Transaction, userID string) (*User, error) {

// 	record, err := d.Model.Get(ctx, tx, userID, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't get user")
// 	}

// 	return record.(*User), nil
// }

// // Select todo
// func (d *userDefinition) Select(
// 	ctx context.Context, tx *libdata.Transaction, filters []*libdata.Filter,
// 	limit *uint, after *string, orderByArg *string,
// ) ([]*User, map[string]*User, error) {
// 	records, recordByIds, err := d.Model.Select(
// 		ctx, tx, filters, []string{}, limit, after, orderByArg)
// 	if err != nil {
// 		return nil, nil, errors.Wrap(err, "Couldn't get users")
// 	}
// 	var users []*User
// 	for _, record := range records {
// 		users = append(users, record.(*User))
// 	}
// 	userByIds := map[string]*User{}
// 	for id, record := range recordByIds {
// 		userByIds[id] = record.(*User)
// 	}
// 	return users, userByIds, nil
// }

// // Create todo
// func (d *userDefinition) Create(ctx context.Context, tx *libdata.Transaction, users []*User) (
// 	[]*User, map[string]*User, error) {

// 	records, recordByIds, err := d.Model.Create(ctx, tx, libutils.InterfaceSlice(users))
// 	if err != nil {
// 		return nil, nil, errors.Wrap(err, "Couldn't get users")
// 	}
// 	users = []*User{}
// 	for _, record := range records {
// 		users = append(users, record.(*User))
// 	}
// 	userByIds := map[string]*User{}
// 	for id, record := range recordByIds {
// 		userByIds[id] = record.(*User)
// 	}

// 	return users, userByIds, nil
// }

// // Update todo
// func (d *userDefinition) Update(
// 	ctx context.Context, tx *libdata.Transaction, filters []*libdata.Filter, user *User, fields []string,
// ) (map[string]*User, error) {

// 	recordByIds, err := d.Model.Update(
// 		ctx, tx, filters, user, fields, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't update")
// 	}
// 	userByIds := map[string]*User{}
// 	for id, record := range recordByIds {
// 		userByIds[id] = record.(*User)
// 	}

// 	return userByIds, nil
// }

// // Delete todo
// func (d *userDefinition) Delete(
// 	ctx context.Context, tx *libdata.Transaction, filters []*libdata.Filter) (map[string]*User, error) {

// 	recordByIds, err := d.Model.Delete(
// 		ctx, tx, filters, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't update")
// 	}
// 	userByIds := map[string]*User{}
// 	for id, record := range recordByIds {
// 		userByIds[id] = record.(*User)
// 	}

// 	return userByIds, nil
// }

// // type storer struct {
// // 	Users  map[string]User
// // 	Tokens map[string][]string
// // }

// // func newStorer() *storer {
// // 	return &storer{
// // 		// Users: map[string]User{},
// // 		Tokens: make(map[string][]string),
// // 	}
// // }

// // func (s storer) Create(key string, attr authboss.Attributes) error {
// // 	fmt.Printf("key %+v\n", key)
// // 	fmt.Printf("attrs %+v\n", attr)

// // 	tx, err := db.BeginTransaction(os.Getenv("DATABASE_NAME"))
// // 	if err != nil {
// // 		return errors.Wrap(err, "Couldn't begin transaction")
// // 	}
// // 	defer func() {
// // 		err = db.EndTransaction(tx, err)
// // 	}()

// // 	_, _, err = userDefinition.Create(context.TODO(), tx, []*User{{
// // 		Name: attr["email"].(string),
// // 		Email: attr["email"].(string),
// // 		Password: attr["password"].(string),
// // 	}})
// // 	if err != nil {
// // 		return errors.Wrap(err, "Couldn't create user")
// // 	}
// // 	// var user User
// // 	// if err := attr.Bind(&user, true); err != nil {
// // 	// 	return err
// // 	// }

// // 	// user.ID = nextUserID
// // 	// nextUserID++

// // 	// s.Users[key] = user
// // 	// fmt.Println("Create")
// // 	// spew.Dump(s.Users)
// // 	return nil
// // }

// // func (s storer) Put(key string, attr authboss.Attributes) error {
// // 	// return s.Create(key, attr)
// // 	return nil
// // }

// // func (s storer) Get(key string) (result interface{}, err error) {
// // 	tx, err := db.BeginTransaction(os.Getenv("DATABASE_NAME"))
// // 	if err != nil {
// // 		return nil, errors.Wrap(err, "Couldn't begin transaction")
// // 	}
// // 	defer func() {
// // 		err = db.EndTransaction(tx, err)
// // 	}()

// // 	users, _, err := userDefinition.Select(context.TODO(), tx, []*db.Filter{{
// // 		Field: "email", Operator: "=", Operande: key,
// // 	}}, nil, nil, nil)
// // 	if err != nil {
// // 		return nil, errors.Wrap(err, "Couldn't select users")
// // 	}

// // 	if len(users) < 1 {
// // 		return nil, authboss.ErrUserNotFound
// // 	}
// // 	// user, ok := s.Users[key]
// // 	// if !ok {
// // 	// 	return nil, authboss.ErrUserNotFound
// // 	// }

// // 	// return &user, nil
// // 	// return &User{
// // 	// 	ID: "test",
// // 	// 	Name: "test",
// // 	// 	Email: "test",
// // 	// 	Password: "test",
// // 	// }, nil
// // 	return users[0], nil
// // }

// // func (s storer) AddToken(key, token string) error {
// // 	s.Tokens[key] = append(s.Tokens[key], token)
// // 	fmt.Println("AddToken")
// // 	spew.Dump(s.Tokens)
// // 	return nil
// // }

// // func (s storer) DelTokens(key string) error {
// // 	delete(s.Tokens, key)
// // 	fmt.Println("DelTokens")
// // 	spew.Dump(s.Tokens)
// // 	return nil
// // }

// // func (s storer) UseToken(givenKey, token string) error {
// // 	toks, ok := s.Tokens[givenKey]
// // 	if !ok {
// // 		return authboss.ErrTokenNotFound
// // 	}

// // 	for i, tok := range toks {
// // 		if tok == token {
// // 			toks[i], toks[len(toks)-1] = toks[len(toks)-1], toks[i]
// // 			s.Tokens[givenKey] = toks[:len(toks)-1]
// // 			return nil
// // 		}
// // 	}

// // 	return authboss.ErrTokenNotFound
// // }

// // func (s storer) ConfirmUser(tok string) (result interface{}, err error) {
// // 	// fmt.Println("==============", tok)

// // 	// for _, u := range s.Users {
// // 	// 	if u.ConfirmToken == tok {
// // 	// 		return &u, nil
// // 	// 	}
// // 	// }

// // 	return nil, authboss.ErrUserNotFound
// // }

// // func (s storer) RecoverUser(rec string) (result interface{}, err error) {
// // // 	for _, u := range s.Users {
// // // 		if u.RecoverToken == rec {
// // // 			return &u, nil
// // // 		}
// // // 	}

// // 	return nil, authboss.ErrUserNotFound
// // }
