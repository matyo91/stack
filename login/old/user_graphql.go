package old

// import (
// 	graphql "github.com/graph-gophers/graphql-go"
// 	"github.com/pkg/errors"
// 	"golang.org/x/net/context"

// 	"gitlab.com/empowerlab/stack/lib-go/libgraphql"
// )

// type userGraphqlDefinition struct {
// 	Graphql *libgraphql.Definition
// }

// // ServiceChildDefinition todo
// var UserGraphqlDefinition = userGraphqlDefinition{
// 	Graphql: &libgraphql.Definition{
// 		Model: UserDefinition.Model,
// 	},
// }

// // UserResolver todo
// type UserResolver struct {
// 	p *User
// }

// // ID todo
// func (pr *UserResolver) ID() graphql.ID {
// 	return graphql.ID(pr.p.ID)
// }

// // Host todo
// func (pr *UserResolver) Name() string {
// 	return pr.p.Name
// }

// // Login todo
// func (pr *UserResolver) Email() string {
// 	return pr.p.Email
// }

// // Password todo
// func (pr *UserResolver) Password() string {
// 	return pr.p.Password
// }

// type userSearchInput struct {
// 	ID *[]graphql.ID
// }

// // User todo
// // nolint: dupl, unparam
// func (r *Resolver) User(ctx context.Context, args *struct {
// 	ID graphql.ID
// }) (*UserResolver, error) {

// 	user, err := UserGraphqlDefinition.Graphql.Get(ctx, DBCluster, "", args.ID, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't get user")
// 	}

// 	return &UserResolver{user.(*User)}, nil
// }

// // AllUsersMeta todo
// // nolint: golint, megacheck
// func (r *Resolver) AllUsersMeta(ctx context.Context, args *struct {
// 	Filter  *userSearchInput
// 	First   *int32
// 	After   *graphql.ID
// 	OrderBy *string
// }) (*_QueryMetaResolver, error) {
// 	users, err := r.AllUsers(ctx, args)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't get users")
// 	}
// 	return &_QueryMetaResolver{count: int32(len(users))}, nil
// }

// // AllUsers todo
// func (r *Resolver) AllUsers(ctx context.Context, args *struct {
// 	Filter  *userSearchInput
// 	First   *int32
// 	After   *graphql.ID
// 	OrderBy *string
// }) ([]*UserResolver, error) {

// 	users, err := UserGraphqlDefinition.Graphql.Select(
// 		ctx, DBCluster, "", args.Filter, []string{}, args.First, args.After, args.OrderBy)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't get user")
// 	}

// 	var resolvers []*UserResolver
// 	for _, r := range users.([]interface{}) {
// 		resolvers = append(resolvers, &UserResolver{r.(*User)})
// 	}

// 	return resolvers, nil
// }

// // CreateUser todo
// // nolint: megacheck
// func (r *Resolver) CreateUser(ctx context.Context, args *struct {
// 	ID       graphql.ID
// 	Name     string
// 	Email    string
// 	Password string
// }) (rr *UserResolver, err error) {

// 	record := User{}
// 	user, err := UserGraphqlDefinition.Graphql.Create(ctx, DBCluster, "", args, &record)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't create user")
// 	}
// 	// users, _, err := userDefinition.ModelDefinition.GraphqlCreate(ctx, args, record)
// 	// if err != nil {
// 	// 	return nil, errors.Wrap(err, "Couldn't insert user")
// 	// }

// 	return &UserResolver{user.(*User)}, nil
// }

// // UpdateUser todo
// // nolint: megacheck
// func (r *Resolver) UpdateUser(ctx context.Context, args *struct {
// 	ID       graphql.ID
// 	Name     *string
// 	Email    *string
// 	Password *string
// }) (*UserResolver, error) {

// 	record := User{}
// 	user, err := UserGraphqlDefinition.Graphql.Update(ctx, DBCluster, "", args, &record, []string{}, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't update user")
// 	}

// 	return &UserResolver{user.(*User)}, nil
// }

// // DeleteUser todo
// // nolint: dupl
// func (r *Resolver) DeleteUser(ctx context.Context, args *struct {
// 	ID graphql.ID
// }) (*UserResolver, error) {

// 	user, err := UserGraphqlDefinition.Graphql.Delete(ctx, DBCluster, "", args.ID, []string{})
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't update user")
// 	}

// 	return &UserResolver{user.(*User)}, nil
// }
