package old

// import (
// 	"context"

// 	"github.com/pkg/errors"
// 	// "gitlab.com/YBuron/lib-go/db"
// )

// // // SchemaCreate todo
// // // nolint: dupl, megacheck
// // func (r *Resolver) SchemaCreate(args *struct {
// // 	Schema        string
// // 	AdminPassword string
// // }) (bool, error) {
// // 	err := db.CreateSchema(args.Schema)
// // 	if err != nil {
// // 		return false, errors.Wrap(err, "Couldn't create schema")
// // 	}
// // 	return true, nil
// // }

// // // SchemaDrop todo
// // // nolint: dupl, megacheck
// // func (r *Resolver) SchemaDrop(args *struct {
// // 	Schema        string
// // 	AdminPassword string
// // }) (bool, error) {
// // 	err := db.DropSchema(args.Schema)
// // 	if err != nil {
// // 		return false, errors.Wrap(err, "Couldn't drop schema")
// // 	}
// // 	return true, nil
// // }

// // InitSingleTenant todo
// // nolint: dupl
// func (r *Resolver) InitSingleTenant(ctx context.Context) (bool, error) {
// 	err := DBCluster.InitSingleTenant(ctx)
// 	if err != nil {
// 		return false, errors.Wrap(err, "Couldn't initialize single tenant")
// 	}
// 	return true, nil
// }

// // DropSingleTenant todo
// // nolint: dupl
// func (r *Resolver) DropSingleTenant(ctx context.Context) (bool, error) {
// 	err := DBCluster.DropSingleTenant(ctx)
// 	if err != nil {
// 		return false, errors.Wrap(err, "Couldn't drop single tenant")
// 	}
// 	return true, nil
// }
