package main

import "gopkg.in/go-playground/validator.v9"

type key string

const (
	testMode         = "True"
	noAutoDeploy key = "noAutoDeploy"
	noAutoPurge  key = "noAutoPurge"
)

// Resolver todo
type Resolver struct{}

// Validate todo
var Validate *validator.Validate

// ValidateComputation todo
func ValidateComputation(fl validator.FieldLevel) bool {
	// nolint: lll
	return fl.Field().String() == "average" || fl.Field().String() == "highest" || fl.Field().String() == ""
}

// _QueryMetaResolver todo
type _QueryMetaResolver struct {
	count int32
}

// ID todo
func (qr *_QueryMetaResolver) Count() int32 {
	return qr.count
}

// SchemaContent todo
// nolint: lll
var SchemaContent = `
	schema {
		query: Query
		mutation: Mutation
	}
	# The query type, represents all of the entry points into our object graph
	type Query {
		User(id: ID!): User!
		_allUsersMeta(filter: UserSearchInput, first: Int, after: ID, orderBy: String): _QueryMeta!
		allUsers(filter: UserSearchInput, first: Int, after: ID, orderBy: String): [User]!
		Role(id: ID!): Role!
		_allRolesMeta(filter: RoleSearchInput, first: Int, after: ID, orderBy: String): _QueryMeta!
		allRoles(filter: RoleSearchInput, first: Int, after: ID, orderBy: String): [Role]!
		Policy(id: ID!): Policy!
		_allPoliciesMeta(filter: PolicySearchInput, first: Int, after: ID, orderBy: String): _QueryMeta!
		allPolicies(filter: PolicySearchInput, first: Int, after: ID, orderBy: String): [Policy]!
	}
	# The mutation type, represents all updates we can make to our data
	type Mutation {
		initSingleTenant(): Boolean!
		dropSingleTenant(): Boolean!
		createUser(
			name: String!
			email: String!
			password: String!): User!
		updateUser(
			id: ID!
			name: String
			email: String
			password: String): User!
		deleteUser(id: ID!): User!
		createRole(
			id: ID!): Role!
		updateRole(
			id: ID!): Role!
		deleteRole(id: ID!): Role!
		createPolicy(
			subjects: String!
			actions: String!
			resources: String!): Policy!
		updatePolicy(
			id: ID!
			subjects: String
			actions: String
			resources: String): Policy!
		deletePolicy(id: ID!): Policy!
	}
	# A humanoid creature from the Star Wars universe
	type _QueryMeta {
		# The ID of the human
		count: Int!
	}
	# A humanoid creature from the Star Wars universe
	type User {
		# The ID of the human
		id: ID!
		# The ID of the human
		name: String!
		# The ID of the human
		email: String!
		# The friends of the human exposed as a connection with edges
		password: String!
	}
	# A humanoid creature from the Star Wars universe
	input UserSearchInput {
		# What this human calls themselves
		id: ID
	}
	# A humanoid creature from the Star Wars universe
	type Role {
		# The ID of the human
		id: ID!
	}
	# A humanoid creature from the Star Wars universe
	input RoleSearchInput {
		# What this human calls themselves
		id: ID
	}
	# A humanoid creature from the Star Wars universe
	type Policy {
		# The ID of the human
		id: ID!
		# The ID of the human
		subjects: String!
		# The ID of the human
		actions: String!
		# The ID of the human
		resources: String!
	}
	# A humanoid creature from the Star Wars universe
	input PolicySearchInput {
		# What this human calls themselves
		id: ID
	}

	# A humanoid creature from the Star Wars universe
	input OrderByInput {
		# What this human calls themselves
		field: String!
		# What this human calls themselves
		desc: Boolean!
	}
`
