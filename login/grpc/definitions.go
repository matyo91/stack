package grpc

import (
	"gitlab.com/empowerlab/stack/lib-go/libgrpc"
	// "gitlab.com/empowerlab/stack/lib-go/libmodel"
	//
	"gitlab.com/empowerlab/stack/login/orm"
)

var Definitions *libgrpc.Definitions

func init() {

	Definitions = &libgrpc.Definitions{
		Repository: "gitlab.com/empowerlab/stack/login",
	}

	Definitions.Register(&libgrpc.Definition{
		ORM: orm.Definitions.GetByID("account"),
	})

	Definitions.Register(&libgrpc.Definition{
		ORM: orm.Definitions.GetByID("role"),
	})

	// dataModel := models.Definitions.GetByID("data")
	// Definitions.Register(&libgrpc.Definition{
	// 	Model:       dataModel,
	// 	CustomFuncs: dataModel.CustomFuncsCollection,
	// })

}
