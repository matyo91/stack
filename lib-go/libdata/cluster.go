package libdata

import (
	"context"
	"fmt"
	"os"
)

// ClusterInfo contains the credentials to connect to the database cluster.
type ClusterInfo struct {
	Driver   DatabaseDriver
	URL      string
	Host     string
	Port     int32
	User     string
	Password string
	Database string
}

// Cluster contain the connection to the database cluster.
type Cluster struct {
	Driver        DatabaseDriver
	MultiTenant   bool
	Models        []*ModelDefinition
	WriteInfo     *ClusterInfo
	WriteCluster  interface{}
	ReadInfo      *ClusterInfo
	ReadCluster   interface{}
	CacheDriver   CacheDriver
	TenantDropper func(ctx context.Context, tenant string) error
}

// Register will register the specified model in the specified cluster.
func (c *Cluster) Register(model *ModelDefinition) {
	model.Cluster = c
	c.Models = append(c.Models, model)
}

// InitDB will initialize the connection with the specified credentials and return the cluster.
// nolint: unparam
func InitDB(
	multiTenant bool, writeInfo *ClusterInfo, readInfo *ClusterInfo, memoryInfo *ClusterInfo,
) (*Cluster, error) {
	cluster, err := writeInfo.Driver.InitDB(writeInfo, readInfo)
	if err != nil {
		return nil, err
	}

	cluster.MultiTenant = multiTenant
	cluster.WriteInfo = writeInfo
	cluster.ReadInfo = readInfo
	return cluster, nil
}

// BeginTransaction will create a new transaction for this cluster and the specified tenant.
// nolint: unparam
func (c *Cluster) BeginTransaction(
	ctx context.Context, tenant *Tenant, useCache bool,
) (*Transaction, error) {

	databaseTenant := os.Getenv("DATABASE_TENANT")
	if databaseTenant != "" {
		tenant = &Tenant{
			Name: databaseTenant,
		}
	}

	var tx *Transaction
	tx, err := c.Driver.BeginTransaction(c, tenant, useCache)
	if err != nil {
		return nil, err
	}

	tx.Cluster = c
	tx.Tenant, err = c.Driver.GetTenant(tx, tenant)
	if err != nil {
		return nil, err
	}

	fmt.Printf("tenant %+v\n", tx.Tenant)

	return tx, nil
}
