package libdata

import (
	"fmt"
)

// DBUnrecognizedOperatorError is an error returned when you use a non existing operator in filter.
type DBUnrecognizedOperatorError struct {
	O string
}

// Error return the error message.
func (e *DBUnrecognizedOperatorError) Error() string {
	return fmt.Sprintf("Unable to recognize operator %s.", e.O)
}

// DBRequestError is an error returned when the database return an error.
type DBRequestError struct {
	Q string
	N string
	R string
}

// Error return the error message.
func (e *DBRequestError) Error() string {
	if len(e.N) > 0 {
		return fmt.Sprintf("Couldn't execute request\n%s\n%s\n%s", e.Q, e.N, e.R)
	}
	if len(e.N) > 0 {
		return fmt.Sprintf("Couldn't execute request\n%s\n%s", e.Q, e.R)
	}
	return fmt.Sprintf("Couldn't execute request\n%s", e.Q)
}
