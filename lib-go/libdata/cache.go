package libdata

type CacheDriver interface {
	SetCache(string, []byte) error
	GetCache(string) ([]byte, error)
	InvalidateCache(string) bool
	SetRecordCache(string, string, string, []byte) error
	GetRecordCache(string, string, string) ([]byte, error)
	InvalidateRecordCache(string, string, string) bool
}

// CacheDriver contain the driver which will make the cache operations.
// var CacheDriver CacheDriverInterface

// // SetCache will insert the data in the cache at the specified key.
// func (tx *Transaction) SetCache(key string, data []byte) error {
// 	return cacheDriver.SetCache(key, data)
// }

// // GetCache will return the data from the cache at the specified key.
// func (tx *Transaction) GetCache(key string) ([]byte, error) {
// 	return cacheDriver.GetCache(key)
// }

// // GetCache will return the data from the cache at the specified key.
// func (tx *Transaction) InvalidateCache(key string) ([]byte, error) {
// 	return cacheDriver.GetCache(key)
// }

// // GetCache will return the data from the cache at the specified key.
// func (tx *Transaction) buildRecordKey(model string, uuid string) string {
// 	return fmt.Sprintf(
// 		"%s_%s_%s", tx.Tenant.ID, model, uuid)
// }

// // SetCache will insert the data in the cache at the specified key.
// func (tx *Transaction) SetRecordCache(model string, uuid string, data []byte) error {
// 	return cacheDriver.SetCache(tx.buildRecordKey(model, uuid), data)
// }

// // SetCache will insert the data in the cache at the specified key.
// func (tx *Transaction) GetRecordCache(model string, uuid string) ([]byte, error) {
// 	return cacheDriver.GetCache(tx.buildRecordKey(model, uuid))
// }

// // SetCache will insert the data in the cache at the specified key.
// func (tx *Transaction) InvalidateRecordCache(model string, uuid string) bool {
// 	return cacheDriver.InvalidateCache(tx.buildRecordKey(model, uuid))
// }
