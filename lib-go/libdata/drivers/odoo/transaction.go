package odoo

import (
	// "fmt"
	// "log"
	// "strings"

	"github.com/jmoiron/sqlx"
	// "github.com/pkg/errors"
	// "gitlab.com/empowerlab/stack/lib-go/libdata"
)

// Tx contains all the function needed to be recognized as a libdata Tx
type Tx struct {
	Sqlx *sqlx.Tx
}
