package odoo

import (
	"bytes"
	"fmt"
	"log"
	"text/template"

	// Let's import the pg packages here since we are obviously using the postgres driver
	_ "github.com/jackc/pgx/stdlib"

	// Let's import the pg packages here since we are obviously using the postgres driver
	_ "github.com/lib/pq"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
	// "gitlab.com/empowerlab/stack/lib-go/libdata/fields"
)

// Driver contains all the function needed to be recognized as a libdata driver
type Driver struct{}

// InitDB will connect to the postgres database and return the connection
func (d *Driver) InitDB(
	writeInfo *libdata.ClusterInfo, readInfo *libdata.ClusterInfo,
) (*libdata.Cluster, error) {

	return &libdata.Cluster{
		Driver: d,
	}, nil
}

// CreateTenant will create the tenant in postgres, either a new schema or a new tenant record
func (d *Driver) CreateTenant(cluster *libdata.Cluster, tenant *libdata.Tenant) error {

	return nil
}

// BeginTransaction will create a new transaction and return it.
func (d *Driver) BeginTransaction(
	c *libdata.Cluster, tenant *libdata.Tenant, useCache bool,
) (*libdata.Transaction, error) {

	writeTx := &Tx{}
	// txs := []*Tx{writeTx}
	var readTx *Tx
	// if c.WriteCluster == c.ReadCluster {
	readTx = writeTx
	// } else {
	// 	readTx = &Tx{
	// 		Sqlx: c.ReadCluster.(*sqlx.DB).MustBegin(),
	// 	}
	// 	txs = append(txs, readTx)
	// }

	// for _, tx := range txs {
	// 	q := fmt.Sprintf("SET search_path TO \"%s\"", tenant.Name)
	// 	args := map[string]interface{}{
	// 		// "tenant": tenant.Name,
	// 	}
	// 	if err := tx.Exec(q, args); err != nil {
	// 		return nil, errors.Wrap(err, "Couldn't set search path")
	// 	}
	// }

	return &libdata.Transaction{
		WriteTx:  writeTx,
		ReadTx:   readTx,
		UseCache: useCache,
	}, nil

}

// RollbackTransaction will cancel all the operations in the transaction.
func (d *Driver) RollbackTransaction(tx *libdata.Transaction) error {
	// err := tx.WriteTx.(*Tx).Sqlx.Rollback()
	// if err != nil {
	// 	return errors.Wrap(err, "Couldn't rollback")
	// }
	// if tx.WriteTx != tx.ReadTx {
	// 	err := tx.ReadTx.(*Tx).Sqlx.Rollback()
	// 	if err != nil {
	// 		return errors.Wrap(err, "Couldn't rollback")
	// 	}
	// }
	return nil
}

// CommitTransaction will definitely save all the operations in the transaction.
func (d *Driver) CommitTransaction(tx *libdata.Transaction) error {

	// fmt.Println("commit")
	// err := tx.WriteTx.(*Tx).Sqlx.Commit()
	// if err != nil {
	// 	fmt.Println("commit err ", err)
	// 	return errors.Wrap(err, "Couldn't commit")
	// }
	// if tx.WriteTx != tx.ReadTx {
	// 	err := tx.ReadTx.(*Tx).Sqlx.Commit()
	// 	if err != nil {
	// 		return errors.Wrap(err, "Couldn't commit")
	// 	}
	// }
	return nil
}

//GetTenant will return the tenant.
func (d *Driver) GetTenant(
	tx *libdata.Transaction, tenant *libdata.Tenant,
) (*libdata.Tenant, error) {
	// if tx.Cluster.MultiTenant {
	// 	tenants, err := tx.postgresSelect(tenantModel, []*Filter{{
	// 		Field: "name", Operator: "=", Operande: tenant,
	// 	}}, nil, nil)
	// 	if err != nil {
	// 		return nil, errors.Wrap(err, "Couldn't get tenant")
	// 	}
	// 	tenant := &Tenant{}
	// 	for tenants.Next() {
	// 		err = tenants.StructScan(tenant)
	// 		if err != nil {
	// 			return nil, errors.Wrap(err, "Couldn't scan to tenants")
	// 		}
	// 	}
	// 	if tenant == nil {
	// 		return nil, errors.New("Specified tenant doesn't exist")
	// 	}
	// 	return tenant, nil
	// }

	return tenant, nil
}

func postgresDBExecute(c *libdata.Cluster, q string, args interface{}) error {

	stmt, err := c.WriteCluster.(*sqlx.DB).PrepareNamed(q)
	if err != nil {
		return errors.Wrap(err, (&libdata.DBRequestError{Q: q, R: fmt.Sprintf("%s", args)}).Error())
	}
	_, err = stmt.Exec(args)
	if err != nil {
		return errors.Wrap(err, (&libdata.DBRequestError{Q: q, R: fmt.Sprintf("%s", args)}).Error())
	}

	log.Print(q)
	return nil
}

func (d *Driver) GenerateScanFunc(model *libdata.ModelDefinition) string {
	data := struct {
		Model *libdata.ModelDefinition
	}{
		Model: model,
	}

	buf := &bytes.Buffer{}
	err := scanTemplate.Execute(buf, data)
	if err != nil {
		fmt.Println("execute ", err)
	}
	// content, err := format.Source(buf.Bytes())
	// if err != nil {
	// 	fmt.Println(buf)
	// 	fmt.Println("model ", err)
	// }
	return buf.String()
}

// nolint:lll
var scanTemplate = template.Must(template.New("").Funcs(template.FuncMap{}).Parse(`
func (d *{{.Model.Title}}Pool) scan(
	rows interface{}, collection *liborm.Collection,
) ([]libdata.ModelInterface, error) {
	var records []libdata.ModelInterface
	_ = &ptypes.Timestamp{}
	_ = &sql.NullString{}
	_ = &sqlx.Rows{}

	return records, nil
}
`))
