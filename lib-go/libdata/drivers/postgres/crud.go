package postgres

import (
	"context"
	"fmt"
	"strconv"
	"strings"

	opentracing "github.com/opentracing/opentracing-go"
	otlog "github.com/opentracing/opentracing-go/log"
	"github.com/pkg/errors"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
	fieldmask "gitlab.com/empowerlab/stack/lib-go/ptypes/fieldmask"
)

// Store is an interface containing the functions needed to build the arguments
// specifics to the model type.
type Store interface {
	InsertBuildArgs([]libdata.ModelInterface) ([]string, map[string]interface{})
	UpdateBuildArgs(interface{}, *fieldmask.FieldMask) ([]string, map[string]interface{})
}

// Insert will insert the new record in the database.
func (d *Driver) Insert(
	ctx context.Context, tx *libdata.Transaction,
	t *libdata.ModelDefinition, records []libdata.ModelInterface,
) (interface{}, error) {
	span, _ := opentracing.StartSpanFromContext(ctx, "postgres-"+t.Name+"-insert")
	defer span.Finish()
	ctx = opentracing.ContextWithSpan(ctx, span)

	fmt.Printf("%+v", t.Store)
	fields, args := t.Store.(Store).InsertBuildArgs(records)
	var values []string
	for i := range records {
		var recordValues []string
		if tx.Cluster.MultiTenant {
			tenantName := fmt.Sprintf(":%v_%s", i, tx.Tenant.ID)
			recordValues = append(recordValues, tenantName)
			args[tenantName] = tx.Tenant.ID
		}
		for _, field := range fields {
			recordValues = append(recordValues, fmt.Sprintf(":%v_%s", i, field))
		}
		values = append(values, strings.Join(recordValues, ","))
	}

	fmt.Printf("args %+v", args)

	//nolint:gas
	q := fmt.Sprintf(
		"INSERT INTO %s (%s) VALUES (%s) RETURNING *",
		t.Snake(), strings.Join(fields, ","), strings.Join(values, "),("))
	span.LogFields(
		otlog.String("query", q),
		otlog.Object("args", args),
	)
	rows, err := tx.WriteTx.(*Tx).QueryRows(q, args)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't insert record")
	}
	return rows, nil

}

// Update will update the data in the filtered records.
func (d *Driver) Update(
	ctx context.Context, tx *libdata.Transaction, t *libdata.ModelDefinition,
	filters []*libdata.Filter, record interface{}, updateMask *fieldmask.FieldMask,
) (interface{}, error) {
	span, _ := opentracing.StartSpanFromContext(ctx, "postgres-"+t.Name+"-update")
	defer span.Finish()
	ctx = opentracing.ContextWithSpan(ctx, span)

	set, args := t.Store.(Store).UpdateBuildArgs(record, updateMask)

	filterQuery, filterArgs, err := buildFilters(tx, t, filters)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't build where query")
	}
	for key, arg := range filterArgs {
		args[key] = arg
	}

	//nolint:gas
	q := fmt.Sprintf(
		"UPDATE \"%s\" SET %s %s RETURNING *",
		t.Snake(), strings.Join(set, ","), filterQuery)
	span.LogFields(
		otlog.String("query", q),
		otlog.Object("args", args),
	)
	rows, err := tx.WriteTx.(*Tx).QueryRows(q, args)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't update record")
	}
	return rows, nil

}

// Select will return the filtered records.
func (d *Driver) Select(
	ctx context.Context, tx *libdata.Transaction, model *libdata.ModelDefinition,
	filters []*libdata.Filter, limit *uint, orderBy []*libdata.OrderByInput,
) (interface{}, error) {
	span, _ := opentracing.StartSpanFromContext(ctx, "postgres-"+model.Name+"-select")
	defer span.Finish()
	ctx = opentracing.ContextWithSpan(ctx, span)

	filterQuery, args, err := buildFilters(tx, model, filters)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't build where query")
	}
	limitQuery := ""
	if limit != nil {
		limitQuery = fmt.Sprintf("LIMIT %s", strconv.Itoa(int(*limit)))
	}
	orderByQuery := ""
	if len(orderBy) > 0 {
		for _, o := range orderBy {
			if orderByQuery != "" {
				orderByQuery = fmt.Sprintf("%s, ", orderByQuery)
			}
			orderByQuery = fmt.Sprintf("%s %s", orderByQuery, o.Field)
			if o.Desc {
				orderByQuery = fmt.Sprintf("%s %s", orderByQuery, "DESC")
			}
		}
		orderByQuery = fmt.Sprintf("ORDER BY %s", orderByQuery)
	}

	//nolint:gas
	query := fmt.Sprintf("SELECT * from \"%s\" %s %s %s",
		model.Snake(), filterQuery, orderByQuery, limitQuery)
	span.LogFields(
		otlog.String("query", query),
		otlog.Object("args", args),
	)
	rows, err := tx.WriteTx.(*Tx).QueryRows(query, args)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't get record")
	}
	return rows, nil
}

// Delete will delete the filtered records.
func (d *Driver) Delete(
	ctx context.Context, tx *libdata.Transaction,
	model *libdata.ModelDefinition, filters []*libdata.Filter,
) error {
	span, _ := opentracing.StartSpanFromContext(ctx, "postgres-"+model.Name+"-delete")
	defer span.Finish()
	ctx = opentracing.ContextWithSpan(ctx, span)

	filterQuery, args, err := buildFilters(tx, model, filters)
	if err != nil {
		return errors.Wrap(err, "Couldn't build where query")
	}

	q := fmt.Sprintf(
		"DELETE FROM \"%s\" %s", model.Snake(), filterQuery)
	span.LogFields(
		otlog.String("query", q),
		otlog.Object("args", args),
	)
	// nolint: gas
	err = tx.WriteTx.(*Tx).Exec(q, args)
	if err != nil {
		return errors.Wrap(err, "Couldn't delete record")
	}
	return nil
}
