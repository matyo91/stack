package postgres

import (
	"fmt"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
)

// RegisterEvent will register the event in the event model.
func (d *Driver) RegisterEvent(
	tx *libdata.Transaction, aggregate string, aggregateUUID string, name string, payload []byte,
) (libdata.EventInterface, error) {
	e := libdata.EventModel.Store.(libdata.EventStore).NewEvent(
		aggregate, aggregateUUID, name, payload)
	fmt.Printf("event %+v", e)
	return e, nil
	// return d.Insert(tx, libdata.EventModel, []interface{}{e})
}
