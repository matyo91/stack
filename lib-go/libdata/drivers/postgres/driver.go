package postgres

import (
	"bytes"
	"fmt"
	"log"
	"text/template"

	// Let's import the pg packages here since we are obviously using the postgres driver
	_ "github.com/jackc/pgx/stdlib"

	// Let's import the pg packages here since we are obviously using the postgres driver
	_ "github.com/lib/pq"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libdata/fields"
)

// Driver contains all the function needed to be recognized as a libdata driver
type Driver struct{}

// InitDB will connect to the postgres database and return the connection
func (d *Driver) InitDB(
	writeInfo *libdata.ClusterInfo, readInfo *libdata.ClusterInfo,
) (*libdata.Cluster, error) {
	writeCluster, err := sqlx.Connect(
		"pgx", writeInfo.URL)
	if err != nil {
		fmt.Println("Couldn't connect with postgres TODO")
		// return nil, errors.Wrap(err, "Couldn't connect with postgres")
	}

	var readCluster *sqlx.DB
	if readInfo == writeInfo {
		readCluster = writeCluster
	} else {
		readCluster, err = sqlx.Connect(
			"pgx", readInfo.URL)
		if err != nil {
			fmt.Println("Couldn't connect with postgres TODO")
			// return nil, errors.Wrap(err, "Couldn't connect with postgres")
		}
	}

	return &libdata.Cluster{
		Driver:       d,
		WriteCluster: writeCluster,
		ReadCluster:  readCluster}, nil
}

// CreateTenant will create the tenant in postgres, either a new schema or a new tenant record
func (d *Driver) CreateTenant(cluster *libdata.Cluster, tenant *libdata.Tenant) error {

	q := fmt.Sprintf("CREATE SCHEMA IF NOT EXISTS \"%s\"", tenant.Name)
	if err := postgresDBExecute(cluster, q, map[string]interface{}{}); err != nil {
		return errors.Wrap(err, "Couldn't create schema")
	}

	err := d.updateTenant(cluster, tenant)
	if err != nil {
		return errors.Wrap(err, "Couldn't update tenant")
	}

	if cluster.MultiTenant {
		// nolint:gas
		q = fmt.Sprintf("INSERT INTO tenant (name) VALUES ('%s')", tenant.Name)
		if err := postgresDBExecute(cluster, q, map[string]interface{}{}); err != nil {
			return errors.Wrap(err, "Couldn't insert tenant")
		}
	}

	return nil
}

//nolint: gocyclo
func (d *Driver) updateTenant(cluster *libdata.Cluster, tenant *libdata.Tenant) error {

	q := fmt.Sprintf("SET search_path TO \"%s\"", tenant.Name)
	if err := postgresDBExecute(cluster, q, map[string]interface{}{}); err != nil {
		return errors.Wrap(err, "Couldn't set search path")
	}

	// db := cluster.WriteCluster.(*sqlx.DB)
	// driver, err := postgres.WithInstance(db.DB, &postgres.Config{})
	// if err != nil {
	// 	return errors.Wrap(err, "Couldn't get driver")
	// }

	// fmt.Printf("%+v", driver.config)
	// m, err := migrate.NewWithDatabaseInstance(
	// 	fmt.Sprintf("file://%spostgres", libdata.MigrationPath),
	// 	"postgres", driver)

	// if err != nil {
	// 	return errors.Wrap(err, "Couldn't get migration")
	// }
	// if err := m.Up(); err != nil {
	// 	switch errors.Cause(err).Error() {
	// 	case "no change":
	// 		return nil
	// 	default:
	// 		return errors.Wrap(err, "Couldn't execute migration")
	// 	}
	// }

	// credentials := fmt.Sprintf(
	// 	"host=%s user=%s dbname=%s password=%s",
	// 	cluster.WriteInfo.Host, // cluster.WriteInfo.Port,
	// 	cluster.WriteInfo.User, cluster.WriteInfo.Database, cluster.WriteInfo.Password)
	// fmt.Println("credentials ", credentials)
	// db, err := gorm.Open("postgres", credentials)
	// if err != nil {
	//   return errors.Wrap(err, "failed to connect database")
	// }
	// defer db.Close()

	// db.Exec(fmt.Sprintf("set search_path to %s", tenant.Name))

	for _, model := range cluster.Models {

		if model.DisableDatabaseStore {
			continue
		}

		q := fmt.Sprintf("CREATE TABLE IF NOT EXISTS %s (uuid UUID PRIMARY KEY);", model.Snake())
		fmt.Println(q)
		if err := postgresDBExecute(cluster, q, map[string]interface{}{
			// "name": model.GetTemplateData().Snake,
		}); err != nil {
			return errors.Wrap(err, "Couldn't create model")
		}
	}

	for _, model := range cluster.Models {

		if model.DisableDatabaseStore {
			continue
		}

		for _, field := range model.Fields {

			if field.Type() == fields.One2manyFieldType {
				continue
			}

			fieldDefinition := field.Snake()
			var columnType string
			switch field.Type() {
			case fields.IDFieldType:
				columnType = "UUID"
			case fields.TextFieldType:
				columnType = "TEXT"
			case fields.IntegerFieldType:
				columnType = "INTEGER"
			case fields.FloatFieldType:
				columnType = "FLOAT"
			case fields.BooleanFieldType:
				columnType = "BOOLEAN"
			case fields.DateFieldType:
				columnType = "DATE"
			case fields.DatetimeFieldType:
				columnType = "DATE"
			case fields.Many2oneFieldType:
				columnType = "UUID"
			case fields.JSONFieldType:
				columnType = "JSON"
			}
			fieldDefinition = fieldDefinition + " " + columnType

			if field.Type() == fields.Many2oneFieldType && field.GetReference() != nil {
				if !field.GetReference().DisableDatabaseStore {
					fieldDefinition = fmt.Sprintf(
						"%s REFERENCES \"%s\"",
						fieldDefinition, field.GetReference().Snake())
				}
			}

			if field.GetPrimaryKey() {
				fieldDefinition += " PRIMARY KEY"
			} else if field.GetRequired() {
				fieldDefinition += " NOT NULL"
			}

			q := fmt.Sprintf("ALTER TABLE \"%s\" ADD COLUMN %s;", model.Snake(), fieldDefinition)
			fmt.Println(q)
			if err := postgresDBExecute(cluster, q, map[string]interface{}{}); err != nil {
				return errors.Wrap(err, "Couldn't create model")
			}

		}

		if model.Datetime {
			q := fmt.Sprintf(
				"ALTER TABLE \"%s\" ADD COLUMN created_at TIMESTAMP NOT NULL DEFAULT NOW();",
				model.Snake())
			fmt.Println(q)
			if err := postgresDBExecute(cluster, q, map[string]interface{}{}); err != nil {
				return errors.Wrap(err, "Couldn't create model")
			}
			q = fmt.Sprintf(
				"ALTER TABLE \"%s\" ADD COLUMN updated_at TIMESTAMP NOT NULL DEFAULT NOW();",
				model.Snake())
			fmt.Println(q)
			if err := postgresDBExecute(cluster, q, map[string]interface{}{}); err != nil {
				return errors.Wrap(err, "Couldn't create model")
			}
			if model.SafeDelete {
				q = fmt.Sprintf(
					"ALTER TABLE \"%s\" ADD COLUMN deleted_at TIMESTAMP NOT NULL DEFAULT NOW();",
					model.Snake())
				fmt.Println(q)
				if err := postgresDBExecute(cluster, q, map[string]interface{}{}); err != nil {
					return errors.Wrap(err, "Couldn't create model")
				}
			}
			q = fmt.Sprintf("ALTER TABLE \"%s\" ADD COLUMN external_module TEXT;", model.Snake())
			fmt.Println(q)
			if err := postgresDBExecute(cluster, q, map[string]interface{}{}); err != nil {
				return errors.Wrap(err, "Couldn't create model")
			}
			q = fmt.Sprintf("ALTER TABLE \"%s\" ADD COLUMN external_id TEXT;", model.Snake())
			fmt.Println(q)
			if err := postgresDBExecute(cluster, q, map[string]interface{}{}); err != nil {
				return errors.Wrap(err, "Couldn't create model")
			}
		}

	}

	return nil

}

//nolint: megacheck
func (d *Driver) postgresDropSchema(cluster *libdata.Cluster, schema string) error {

	q := fmt.Sprintf("DROP SCHEMA \"%s\" CASCADE", schema)
	if err := postgresDBExecute(cluster, q, map[string]interface{}{}); err != nil {
		return errors.Wrap(err, "Couldn't drop schema")
	}
	return nil
}

// BeginTransaction will create a new transaction and return it.
func (d *Driver) BeginTransaction(
	c *libdata.Cluster, tenant *libdata.Tenant, useCache bool,
) (*libdata.Transaction, error) {

	writeTx := &Tx{
		Sqlx: c.WriteCluster.(*sqlx.DB).MustBegin()}
	txs := []*Tx{writeTx}
	var readTx *Tx
	if c.WriteCluster == c.ReadCluster {
		readTx = writeTx
	} else {
		readTx = &Tx{
			Sqlx: c.ReadCluster.(*sqlx.DB).MustBegin(),
		}
		txs = append(txs, readTx)
	}

	for _, tx := range txs {
		q := fmt.Sprintf("SET search_path TO \"%s\"", tenant.Name)
		args := map[string]interface{}{
			// "tenant": tenant.Name,
		}
		if err := tx.Exec(q, args); err != nil {
			return nil, errors.Wrap(err, "Couldn't set search path")
		}
	}

	return &libdata.Transaction{
		// Driver:    *d,
		WriteTx:  writeTx,
		ReadTx:   readTx,
		UseCache: useCache}, nil

}

// RollbackTransaction will cancel all the operations in the transaction.
func (d *Driver) RollbackTransaction(tx *libdata.Transaction) error {
	err := tx.WriteTx.(*Tx).Sqlx.Rollback()
	if err != nil {
		return errors.Wrap(err, "Couldn't rollback")
	}
	if tx.WriteTx != tx.ReadTx {
		err := tx.ReadTx.(*Tx).Sqlx.Rollback()
		if err != nil {
			return errors.Wrap(err, "Couldn't rollback")
		}
	}
	return nil
}

// CommitTransaction will definitely save all the operations in the transaction.
func (d *Driver) CommitTransaction(tx *libdata.Transaction) error {

	fmt.Println("commit")
	err := tx.WriteTx.(*Tx).Sqlx.Commit()
	if err != nil {
		fmt.Println("commit err ", err)
		return errors.Wrap(err, "Couldn't commit")
	}
	if tx.WriteTx != tx.ReadTx {
		err := tx.ReadTx.(*Tx).Sqlx.Commit()
		if err != nil {
			return errors.Wrap(err, "Couldn't commit")
		}
	}
	return nil
}

//GetTenant will return the tenant.
func (d *Driver) GetTenant(
	tx *libdata.Transaction, tenant *libdata.Tenant,
) (*libdata.Tenant, error) {
	// if tx.Cluster.MultiTenant {
	// 	tenants, err := tx.postgresSelect(tenantModel, []*Filter{{
	// 		Field: "name", Operator: "=", Operande: tenant,
	// 	}}, nil, nil)
	// 	if err != nil {
	// 		return nil, errors.Wrap(err, "Couldn't get tenant")
	// 	}
	// 	tenant := &Tenant{}
	// 	for tenants.Next() {
	// 		err = tenants.StructScan(tenant)
	// 		if err != nil {
	// 			return nil, errors.Wrap(err, "Couldn't scan to tenants")
	// 		}
	// 	}
	// 	if tenant == nil {
	// 		return nil, errors.New("Specified tenant doesn't exist")
	// 	}
	// 	return tenant, nil
	// }

	return tenant, nil
}

func postgresDBExecute(c *libdata.Cluster, q string, args interface{}) error {

	stmt, err := c.WriteCluster.(*sqlx.DB).PrepareNamed(q)
	if err != nil {
		return errors.Wrap(err, (&libdata.DBRequestError{Q: q, R: fmt.Sprintf("%s", args)}).Error())
	}
	_, err = stmt.Exec(args)
	if err != nil {
		return errors.Wrap(err, (&libdata.DBRequestError{Q: q, R: fmt.Sprintf("%s", args)}).Error())
	}

	log.Print(q)
	return nil
}

func (d *Driver) GenerateScanFunc(model *libdata.ModelDefinition) string {
	data := struct {
		Model *libdata.ModelDefinition
	}{
		Model: model,
	}

	buf := &bytes.Buffer{}
	err := scanTemplate.Execute(buf, data)
	if err != nil {
		fmt.Println("execute ", err)
	}
	// content, err := format.Source(buf.Bytes())
	// if err != nil {
	// 	fmt.Println(buf)
	// 	fmt.Println("model ", err)
	// }
	return buf.String()
}

// nolint:lll
var scanTemplate = template.Must(template.New("").Funcs(template.FuncMap{}).Parse(`
func (d *{{.Model.Title}}Pool) scan(
	rows interface{}, collection *liborm.Collection,
) ([]libdata.ModelInterface, error) {
	var records []libdata.ModelInterface
	for rows.(*sqlx.Rows).Next() {
		var uuid string
		{{- range .Model.StoredFields }}
		{{.Name}} := &{{.DBType.Type}}{}
		{{- end}}
		createdAt := &ptypes.Timestamp{}
		updatedAt := &ptypes.Timestamp{}
		externalModule := &sql.NullString{}
		externalID := &sql.NullString{}
		err := rows.(*sqlx.Rows).Scan(
			&uuid,
			{{- range .Model.StoredFields }}
			{{.Name}},
			{{- end}}
			createdAt,
			updatedAt,
			externalModule,
			externalID,
		)
		if err != nil {
			fmt.Println(err)
			return nil, errors.Wrap(err, "Couldn't scan to templates")
		}

		var {{.Model.Name}} *{{.Model.Title}}
		if collection == nil {
			{{.Model.Name}} = d.new().(*{{.Model.Title}})
		} else {
			{{.Model.Name}} = collection.GetByUUID(uuid).(*{{.Model.Title}})
		}

		{{.Model.Name}}.UUID = uuid
		{{- range .Model.StoredFields }}
		{{if eq .GoType "int32"}}
		{{$.Model.Name}}.{{.Title}} = int32({{.Name}}.{{.DBType.Value}})
		{{else}}
			{{if eq .GoType "float32"}}
			{{$.Model.Name}}.{{.Title}} = float32({{.Name}}.{{.DBType.Value}})
			{{else}}
				{{if eq .GoType "[]byte"}}
					{{$.Model.Name}}.{{.Title}} = []byte({{.Name}}.{{.DBType.Value}})
				{{else}}
					{{if eq .Type "Many2oneType"}}
						{{$.Model.Name}}.{{.Title}} = &pb.{{.TitleWithoutUUID}}{UUID: {{.Name}}.{{.DBType.Value}} }
					{{else}}
						{{$.Model.Name}}.{{.Title}} = {{.Name}}.{{.DBType.Value}}
					{{end}}
				{{end}}
			{{end}}
		{{end}}
		{{- end}}
		{{.Model.Name}}.CreatedAt = createdAt
		{{.Model.Name}}.UpdatedAt = updatedAt
		{{.Model.Name}}.ExternalModule = externalModule.String
		{{.Model.Name}}.ExternalID = externalID.String
		records = append(records, {{.Model.Name}})
		fmt.Printf("record %+v\n", {{.Model.Name}})
	}
	return records, nil
}
`))
