package postgres

import (
	"fmt"
	"log"
	"strings"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
)

// Tx contains all the function needed to be recognized as a libdata Tx
type Tx struct {
	Sqlx *sqlx.Tx
}

// Exec will execute the specified query in this transaction.
func (tx *Tx) Exec(query string, args interface{}) error {
	txStmt, err := tx.Sqlx.PrepareNamed(query)
	if err != nil {
		return errors.Wrap(err, (&libdata.DBRequestError{Q: query, R: fmt.Sprintf("%s", args)}).Error())
	}
	_, err = txStmt.Exec(args)
	if err != nil {
		return errors.Wrap(err, (&libdata.DBRequestError{Q: query, R: fmt.Sprintf("%s", args)}).Error())
	}
	log.Printf("%s\n%+v", query, args)
	return nil
}

// QueryRows will execute the specified query in this transaction and return the records.
func (tx *Tx) QueryRows(q string, args interface{}) (*sqlx.Rows, error) {
	txStmt, err := tx.Sqlx.PrepareNamed(q)
	if err != nil {
		log.Println(err)
		return nil, errors.Wrap(err, (&libdata.DBRequestError{Q: q, R: fmt.Sprintf("%s", args)}).Error())
	}
	rows, err := txStmt.Queryx(args)
	if err != nil {
		log.Println(err)
		return nil, errors.Wrap(err, (&libdata.DBRequestError{Q: q, R: fmt.Sprintf("%s", args)}).Error())
	}

	log.Printf("%s\n%+v", q, args)
	return rows, nil
}

func buildFilters(
	tx *libdata.Transaction, model *libdata.ModelDefinition,
	filters []*libdata.Filter,
) (string, map[string]interface{}, error) {

	query := ""
	args := map[string]interface{}{}
	fmt.Printf("tx %+v\n", tx)
	if model.Cluster.MultiTenant && model != libdata.TenantModel {
		filters = append(filters, &libdata.Filter{
			Field: "tenant_id", Operator: "=", Operande: tx.Tenant.ID,
		})
	}
	for _, w := range filters {
		if query != "" {
			query = fmt.Sprintf("%s AND ", query)
		}

		switch w.Operator {
		case "=", "<", ">":
			query = fmt.Sprintf("%s%s %s :%s", query, w.Field, w.Operator, w.Field)
			args[w.Field] = w.Operande
		case "IN":
			query = fmt.Sprintf("%s%s = ANY (:%s)", query, w.Field, w.Field)
			args[w.Field] = "{" + strings.Join(w.Operande.([]string), ",") + "}"
		case "IS NULL":
			query = fmt.Sprintf("%s%s IS NULL", query, w.Field)
		case "IS NOT NULL":
			query = fmt.Sprintf("%s%s IS NOT NULL", query, w.Field)
		default:
			return "", nil, &libdata.DBUnrecognizedOperatorError{O: w.Operator}
		}
	}

	if query != "" {
		// nolint: gas
		query = fmt.Sprintf("WHERE %s", query)
	}
	return query, args, nil
}
