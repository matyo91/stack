package memory

import (
	"fmt"

	"github.com/coocood/freecache"
)

// Driver contains all the function needed to be recognized as a libdata cache driver
type Driver struct{}

var cache *freecache.Cache

func init() {
	cacheSize := 100 * 1024 * 1024
	cache = freecache.NewCache(cacheSize)
	// debug.SetGCPercent(20)
}

// Store is an interface containing the functions needed to build the arguments
// specifics to the model type.
type Store interface {
	InsertBuildArgs([]interface{}) ([]string, map[string]interface{})
}

// SetCache will insert the data in the cache at the specified key.
func (d *Driver) SetCache(
	key string, data []byte,
) error {
	return cache.Set([]byte(key), data, 60)
}

// GetCache will return the data from the cache at the specified key.
func (d *Driver) GetCache(key string) ([]byte, error) {
	return cache.Get([]byte(key))
}

// GetCache will return the data from the cache at the specified key.
func (d *Driver) InvalidateCache(key string) bool {
	return cache.Del([]byte(key))
}

// SetCache will insert the data in the cache at the specified key.
func (d *Driver) SetRecordCache(tenantUUID string, model string, uuid string, data []byte) error {
	return d.SetCache(buildRecordKey(tenantUUID, model, uuid), data)
}

// SetCache will insert the data in the cache at the specified key.
func (d *Driver) GetRecordCache(tenantUUID string, model string, uuid string) ([]byte, error) {
	return d.GetCache(buildRecordKey(tenantUUID, model, uuid))
}

// SetCache will insert the data in the cache at the specified key.
func (d *Driver) InvalidateRecordCache(tenantUUID string, model string, uuid string) bool {
	return d.InvalidateCache(buildRecordKey(tenantUUID, model, uuid))
}

func buildRecordKey(tenantUUID string, model string, uuid string) string {
	return fmt.Sprintf(
		"%s_%s_%s", tenantUUID, model, uuid)
}
