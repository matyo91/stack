package nats

import (
	nats "github.com/nats-io/nats.go"
)

func GetDriver(url string) (*Driver, error) {

	client, err := nats.Connect(url)
	if err != nil {
		return nil, err
	}

	return &Driver{
		Client: client,
	}, nil

}

type Driver struct {
	Client *nats.Conn
}

func (d *Driver) Publish(subj string, data []byte) error {
	// fmt.Println("Publish ", event.GetAggregate()+strings.Title(event.GetName()))
	return d.Client.Publish(subj, data)
}
