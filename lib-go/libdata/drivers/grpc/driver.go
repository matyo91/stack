package grpc

import (
	"context"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
)

type Driver struct {
	Client interface {
		Get(context.Context, string, string) (interface{}, error)
		List(context.Context, string, []*libdata.Filter) ([]interface{}, error)
	}
	ImportName string
	Import     string
}

func (d *Driver) Get(ctx context.Context, model string, uuid string) (interface{}, error) {
	return d.Client.Get(ctx, model, uuid)
}

func (d *Driver) Select(ctx context.Context, model string, filters []*libdata.Filter) ([]interface{}, error) {
	return d.Client.List(ctx, model, filters)
}

func (d *Driver) GetImportName() string {
	return d.ImportName
}

func (d *Driver) GetImport() string {
	return d.Import
}
