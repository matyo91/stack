package libdata

import (
	"context"
	"os"
	"time"

	"github.com/pkg/errors"
)

// TenantModel contain the definition of the tenant model, used in multi tenant mode.
var TenantModel *ModelDefinition

func init() {
	TenantModel = &ModelDefinition{
		Name:        "tenant",
		Datetime:    true,
		CanAssignID: false,
	}
}

// Tenant is a struct containing the tenant information.
type Tenant struct {
	ID        string    `db:"id"`
	Name      string    `db:"name"`
	CreatedAt time.Time `json:"createdAt" db:"created_at"`
	UpdatedAt time.Time `json:"updatedAt" db:"updated_at"`
}

// IsMultiSchema return if we are in multi schema mode.
func IsMultiSchema() bool {
	return GetSingleTenant() == ""
}

// GetSingleTenant return the database tenant if we are in single tenant mode.
func GetSingleTenant() string {
	return os.Getenv("DATABASE_TENANT")
}

var PostInitTenant func(context.Context) error

// InitSingleTenant will initialize the database in single tenant mode.
// nolint: unparam
func (c *Cluster) InitSingleTenant(ctx context.Context) error {

	if GetSingleTenant() == "" {
		return errors.New("The single tenant is not specified")
	}

	err := c.Driver.CreateTenant(c, &Tenant{Name: GetSingleTenant()})
	if err != nil {
		return err
	}

	if PostInitTenant != nil {
		err = PostInitTenant(ctx)
		if err != nil {
			return errors.Wrap(err, "Couldn't execute PostInitTenant")
		}
	}

	return nil
}

// DropSingleTenant will drop the single tenant.
// nolint: unparam
func (c *Cluster) DropSingleTenant(ctx context.Context) error {
	return nil
}

// CreateTenant will create the new specified tenant in multi tenant mode.
// nolint: unparam
func (c *Cluster) CreateTenant(ctx context.Context, tenant string) error {

	// if !c.MultiTenant {
	// 	return errors.New("This application is not multi-tenant")
	// }

	// if IsMultiSchema() {
	// 	switch c.Type {
	// 	case CassandraType:
	// 		return c.cassandraCreateSchema(tenant)
	// 	case PostgresType:
	// 		return c.postgresCreateSchema(tenant)
	// 	default:
	// 		return &TypeError{}
	// 	}
	// }

	// // if c.MultiTenant {
	// // 	target := tenant
	// // 	if !IsMultiSchema() {
	// // 		target = GetSingleTenant()
	// // 	}
	// // 	txT, err := c.BeginTransaction(ctx, target)
	// // 	if err != nil {
	// // 		return errors.Wrap(err, "Couldn't get transaction")
	// // 	}
	// // 	fmt.Println("test")
	// // 	_, err = txT.Insert(ctx, tenantModel, []interface{}{&Tenant{
	// // 		Name: tenant,
	// // 	}}, []string{"name"})
	// // 	if err != nil {
	// // 		return errors.Wrap(err, "Couldn't insert tenant")
	// // 	}
	// // 	txT.CommitTransaction(ctx, err)
	// // }

	return nil
}

// DropTenant will drop the specified tenant.
// nolint: unparam
func (c *Cluster) DropTenant(ctx context.Context, tenant string) error {

	// if !c.MultiTenant {
	// 	return errors.New("This application is not multi-tenant")
	// }

	// if IsMultiSchema() {
	// 	fmt.Printf("c %+v", c)
	// 	switch c.Type {
	// 	case CassandraType:
	// 		err := c.cassandraDropSchema(tenant)
	// 		if err != nil {
	// 			return errors.Wrap(err, "Couldn't drop schema")
	// 		}
	// 	case PostgresType:
	// 		err := c.postgresDropSchema(tenant)
	// 		if err != nil {
	// 			return errors.Wrap(err, "Couldn't drop schema")
	// 		}
	// 	default:
	// 		return &TypeError{}
	// 	}
	// } else {
	// 	err := c.TenantDropper(ctx, tenant)
	// 	if err != nil {
	// 		return errors.Wrap(err, "Couldn't drop tenant")
	// 	}
	// }

	// if c.MultiTenant && !IsMultiSchema() && GetSingleTenant() != tenant {
	// 	tx, err := c.BeginTransaction(ctx, GetSingleTenant())
	// 	if err != nil {
	// 		return errors.Wrap(err, "Couldn't get transaction")
	// 	}
	// 	err = tx.Delete(ctx, tenantModel, []*Filter{{
	// 		Field: "name", Operator: "=", Operande: tenant,
	// 	}})
	// 	if err != nil {
	// 		return errors.Wrap(err, "Couldn't delete tenant")
	// 	}
	// 	err = tx.CommitTransaction(ctx, err)
	// 	if err != nil {
	// 		return errors.Wrap(err, "Couldn't commit")
	// 	}
	// }

	return nil
}
