package libdata

import (
	"context"

	fieldmask "gitlab.com/empowerlab/stack/lib-go/ptypes/fieldmask"
)

// Driver is an interface for the drivers providing crud operations.
type DatabaseDriver interface {
	InitDB(writeInfo *ClusterInfo, readInfo *ClusterInfo) (*Cluster, error)
	CreateTenant(cluster *Cluster, tenant *Tenant) error
	BeginTransaction(c *Cluster, tenant *Tenant, useCache bool) (*Transaction, error)
	RollbackTransaction(*Transaction) error
	CommitTransaction(*Transaction) error
	GetTenant(*Transaction, *Tenant) (*Tenant, error)
	GenerateScanFunc(*ModelDefinition) string
	Select(context.Context, *Transaction, *ModelDefinition, []*Filter, *uint, []*OrderByInput) (interface{}, error)
	Insert(context.Context, *Transaction, *ModelDefinition, []ModelInterface) (interface{}, error)
	Update(
		context.Context, *Transaction, *ModelDefinition, []*Filter, interface{}, *fieldmask.FieldMask,
	) (interface{}, error)
	Delete(context.Context, *Transaction, *ModelDefinition, []*Filter) error
}

// Driver is an interface for the drivers providing crud operations.
type ExternalDriver interface {
	Get(context.Context, string, string) (interface{}, error)
	Select(context.Context, string, []*Filter) ([]interface{}, error)
	GetImportName() string
	GetImport() string
}
