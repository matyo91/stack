package fields

import (
	// "strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
)

// DateFieldType contains the field type for Date
const DateFieldType = libdata.FieldType("DateType")

/*Date is the field type you can use in definition to declare a Date field.
 */
type Date struct {
	Name       string
	String     string
	Required   bool
	Unique     bool
	PrimaryKey bool
	Store      bool
	TitleName  string
	DBName     string
}

// GetName return the name of the field.
func (f *Date) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *Date) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Date) NameWithoutUUID() string {
	return f.Name
}

// Title return the title of the field.
func (f *Date) TitleWithoutUUID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Date) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

// Type return the type of the field.
func (f *Date) Type() libdata.FieldType {
	return DateFieldType
}

// Type return the type of the field.
func (f *Date) GoType() string {
	return "time.Time"
}

// ProtoType return the protobuf type for this field.
func (f *Date) ProtoType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *Date) DBType() *libdata.DBType {
	return &libdata.DBType{
		Type:  "ptypes.Timestamp",
		Value: "",
	}
}

// Type return the type of the field.
func (f *Date) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Date) GraphqlSchemaType() string {
	return "String"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Date) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Date) GetReference() *libdata.ModelDefinition {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Date) SetReference(*libdata.ModelDefinition) {
}

// GetInverseField return the inverse field, if applicable.
func (f *Date) GetInverseField() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *Date) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Date) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *Date) IsStored() bool {
	return true
}

func (f *Date) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *Date) GetFieldData() *libdata.FieldData {
// 	return &libdata.FieldData{
// 		Name:            f.Name,
// 		NameWithoutUUID: f.Name,
// 		// Type:            DateFieldType.GoType(),
// 		// GoType:          DateFieldType.GoType(),
// 		// ProtoType:       DateFieldType.ProtoType(),
// 		DBType: &libdata.DBType{
// 			Type:  "ptypes.Timestamp",
// 			Value: "",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutUUID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseField:     "",
// 		// Definition:       f,
// 	}
// }
