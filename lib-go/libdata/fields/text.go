package fields

import (
	// "strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
)

// TextFieldType contains the field type for Text
const TextFieldType = libdata.FieldType("TextType")

/*Text is the field type you can use in definition to declare a Text field.
 */
type Text struct {
	Name       string
	String     string
	Required   bool
	Unique     bool
	PrimaryKey bool
	Store      bool
	TitleName  string
	DBName     string
}

// GetName return the name of the field.
func (f *Text) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *Text) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Text) NameWithoutUUID() string {
	return f.Name
}

// Title return the title of the field.
func (f *Text) TitleWithoutUUID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Text) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

// Type return the type of the field.
func (f *Text) Type() libdata.FieldType {
	return TextFieldType
}

// Type return the type of the field.
func (f *Text) GoType() string {
	return libdata.STRING
}

// ProtoType return the protobuf type for this field.
func (f *Text) ProtoType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *Text) DBType() *libdata.DBType {
	return &libdata.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *Text) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Text) GraphqlSchemaType() string {
	return "String"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Text) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Text) GetReference() *libdata.ModelDefinition {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Text) SetReference(*libdata.ModelDefinition) {
}

// GetInverseField return the inverse field, if applicable.
func (f *Text) GetInverseField() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *Text) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Text) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *Text) IsStored() bool {
	return true
}

func (f *Text) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *Text) GetFieldData() *libdata.FieldData {
// 	return &libdata.FieldData{
// 		Name:            f.Name,
// 		NameWithoutUUID: f.Name,
// 		// Type:            TextFieldType.GoType(),
// 		// GoType:          TextFieldType.GoType(),
// 		// ProtoType:       TextFieldType.ProtoType(),
// 		DBType: &libdata.DBType{
// 			Type:  "sql.NullString",
// 			Value: "String",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutUUID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseField:     "",
// 		// Definition:       f,
// 	}
// }
