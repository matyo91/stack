package fields

import (
	// "strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
)

// DatetimeFieldType contains the field type for Datetime
const DatetimeFieldType = libdata.FieldType("DatetimeType")

/*Datetime is the field type you can use in definition to declare a Datetime field.
 */
type Datetime struct {
	Name       string
	String     string
	Required   bool
	Unique     bool
	PrimaryKey bool
	Store      bool
	TitleName  string
	DBName     string
}

// GetName return the name of the field.
func (f *Datetime) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *Datetime) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Datetime) NameWithoutUUID() string {
	return f.Name
}

// Title return the title of the field.
func (f *Datetime) TitleWithoutUUID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Datetime) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

// Type return the type of the field.
func (f *Datetime) Type() libdata.FieldType {
	return DatetimeFieldType
}

// Type return the type of the field.
func (f *Datetime) GoType() string {
	return "time.Time"
}

// ProtoType return the protobuf type for this field.
func (f *Datetime) ProtoType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *Datetime) DBType() *libdata.DBType {
	return &libdata.DBType{
		Type:  "ptypes.Timestamp",
		Value: "",
	}
}

// Type return the type of the field.
func (f *Datetime) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Datetime) GraphqlSchemaType() string {
	return "String"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Datetime) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Datetime) GetReference() *libdata.ModelDefinition {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Datetime) SetReference(*libdata.ModelDefinition) {
}

// GetInverseField return the inverse field, if applicable.
func (f *Datetime) GetInverseField() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *Datetime) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Datetime) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *Datetime) IsStored() bool {
	return true
}

func (f *Datetime) IsNested() bool {
	return false
}

// //GetFieldData return the field information in a format exploimodel by templates.
// func (f *Datetime) GetFieldData() *libdata.FieldData {
// 	return &libdata.FieldData{
// 		Name:            f.Name,
// 		NameWithoutUUID: f.Name,
// 		// Type:            DatetimeFieldType.GoType(),
// 		// GoType:          DatetimeFieldType.GoType(),
// 		// ProtoType:       DatetimeFieldType.ProtoType(),
// 		DBType: &libdata.DBType{
// 			Type:  "ptypes.Timestamp",
// 			Value: "",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutUUID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseField:     "",
// 		// Definition:       f,
// 	}
// }
