package fields

import (
	// "strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
)

// FloatFieldType contains the field type for Float
const FloatFieldType = libdata.FieldType("FloatType")

/*Float is the field type you can use in definition to declare a Float field.
 */
type Float struct {
	Name       string
	String     string
	Required   bool
	Unique     bool
	PrimaryKey bool
	Store      bool
	TitleName  string
	DBName     string
}

// GetName return the name of the field.
func (f *Float) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *Float) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Float) NameWithoutUUID() string {
	return f.Name
}

// Title return the title of the field.
func (f *Float) TitleWithoutUUID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Float) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

// Type return the type of the field.
func (f *Float) Type() libdata.FieldType {
	return FloatFieldType
}

// Type return the type of the field.
func (f *Float) GoType() string {
	return "float64"
}

// ProtoType return the protobuf type for this field.
func (f *Float) ProtoType() string {
	return "double"
}

// ProtoType return the protobuf type for this field.
func (f *Float) DBType() *libdata.DBType {
	return &libdata.DBType{
		Type:  "sql.NullFloat64",
		Value: "Float64",
	}
}

// Type return the type of the field.
func (f *Float) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Float) GraphqlSchemaType() string {
	return "Float"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Float) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Float) GetReference() *libdata.ModelDefinition {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Float) SetReference(*libdata.ModelDefinition) {
}

// GetInverseField return the inverse field, if applicable.
func (f *Float) GetInverseField() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *Float) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Float) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *Float) IsStored() bool {
	return true
}

func (f *Float) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *Float) GetFieldData() *libdata.FieldData {
// 	return &libdata.FieldData{
// 		Name:            f.Name,
// 		NameWithoutUUID: f.Name,
// 		// Type:            FloatFieldType.GoType(),
// 		// GoType:          FloatFieldType.GoType(),
// 		// ProtoType:       FloatFieldType.ProtoType(),
// 		DBType: &libdata.DBType{
// 			Type:  "sql.NullFloat64",
// 			Value: "Float64",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutUUID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseField:     "",
// 		// Definition:       f,
// 	}
// }
