package fields

import (
	// "strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
)

// BooleanFieldType contains the field type for Boolean
const BooleanFieldType = libdata.FieldType("BooleanType")

/*Boolean is the field type you can use in definition to declare a Boolean field.
 */
type Boolean struct {
	Name       string
	String     string
	Required   bool
	Unique     bool
	PrimaryKey bool
	Store      bool
	TitleName  string
	DBName     string
}

// GetName return the name of the field.
func (f *Boolean) GetName() string {
	return f.Name
}

// GetTitleName return the title of the field.
func (f *Boolean) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Boolean) NameWithoutUUID() string {
	return f.Name
}

// Title return the title of the field.
func (f *Boolean) TitleWithoutUUID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Boolean) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

// Type return the type of the field.
func (f *Boolean) Type() libdata.FieldType {
	return BooleanFieldType
}

// Type return the type of the field.
func (f *Boolean) GoType() string {
	return "bool"
}

// ProtoType return the protobuf type for this field.
func (f *Boolean) ProtoType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *Boolean) DBType() *libdata.DBType {
	return &libdata.DBType{
		Type:  "sql.NullBool",
		Value: "Bool",
	}
}

// Type return the type of the field.
func (f *Boolean) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Boolean) GraphqlSchemaType() string {
	return "Boolean"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Boolean) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Boolean) GetReference() *libdata.ModelDefinition {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Boolean) SetReference(*libdata.ModelDefinition) {
}

// GetInverseField return the inverse field, if applicable.
func (f *Boolean) GetInverseField() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *Boolean) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Boolean) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *Boolean) IsStored() bool {
	return true
}

func (f *Boolean) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *Boolean) GetFieldData() *libdata.FieldData {
// 	return &libdata.FieldData{
// 		Name:            f.Name,
// 		NameWithoutUUID: f.Name,
// 		// Type:            BooleanFieldType.GoType(),
// 		// GoType:          BooleanFieldType.GoType(),
// 		// ProtoType:       BooleanFieldType.ProtoType(),
// 		DBType: &libdata.DBType{
// 			Type:  "sql.NullBool",
// 			Value: "Bool",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutUUID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseField:     "",
// 		// Definition:       f,
// 	}
// }
