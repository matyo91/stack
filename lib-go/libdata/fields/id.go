package fields

import (
	// "strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
)

// IDFieldType contains the field type for ID
const IDFieldType = libdata.FieldType("IDType")

/*ID is the field type you can use in definition to declare an ID field.
 */
type ID struct {
	Name       string
	String     string
	Required   bool
	Unique     bool
	PrimaryKey bool
	Store      bool
	TitleName  string
	DBName     string
}

// GetName return the name of the field.
func (f *ID) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *ID) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *ID) NameWithoutUUID() string {
	return f.Name
}

// Title return the title of the field.
func (f *ID) TitleWithoutUUID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *ID) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

// Type return the type of the field.
func (f *ID) Type() libdata.FieldType {
	return IDFieldType
}

// Type return the type of the field.
func (f *ID) GoType() string {
	return libdata.STRING
}

// ProtoType return the protobuf type for this field.
func (f *ID) ProtoType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *ID) DBType() *libdata.DBType {
	return &libdata.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *ID) GraphqlType() string {
	return "graphql.ID"
}

// Type return the type of the field.
func (f *ID) GraphqlSchemaType() string {
	return "ID"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *ID) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *ID) GetReference() *libdata.ModelDefinition {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *ID) SetReference(*libdata.ModelDefinition) {
}

// GetInverseField return the inverse field, if applicable.
func (f *ID) GetInverseField() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *ID) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *ID) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *ID) IsStored() bool {
	return true
}

func (f *ID) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *ID) GetFieldData() *libdata.FieldData {
// 	return &libdata.FieldData{
// 		Name:            f.Name,
// 		NameWithoutUUID: f.Name,
// 		// Type:            IDFieldType.GoType(),
// 		// GoType:          IDFieldType.GoType(),
// 		// ProtoType:       IDFieldType.ProtoType(),
// 		DBType: &libdata.DBType{
// 			Type:  "sql.NullString",
// 			Value: "String",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutUUID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseField:     "",
// 		// Definition:       f,
// 	}
// }
