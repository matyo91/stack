package fields

import (
	// "strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
)

// JSONFieldType contains the field type for JSON
const JSONFieldType = libdata.FieldType("JSONType")

/*JSON is the field type you can use in definition to declare a JSON field.
 */
type JSON struct {
	Name       string
	String     string
	Required   bool
	Unique     bool
	PrimaryKey bool
	Store      bool
	TitleName  string
	DBName     string
}

// GetName return the name of the field.
func (f *JSON) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *JSON) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *JSON) NameWithoutUUID() string {
	return f.Name
}

// Title return the title of the field.
func (f *JSON) TitleWithoutUUID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *JSON) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

// Type return the type of the field.
func (f *JSON) Type() libdata.FieldType {
	return JSONFieldType
}

// Type return the type of the field.
func (f *JSON) GoType() string {
	return "[]byte"
}

// ProtoType return the protobuf type for this field.
func (f *JSON) ProtoType() string {
	return "bytes"
}

// ProtoType return the protobuf type for this field.
func (f *JSON) DBType() *libdata.DBType {
	return &libdata.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *JSON) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *JSON) GraphqlSchemaType() string {
	return "String"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *JSON) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *JSON) GetReference() *libdata.ModelDefinition {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *JSON) SetReference(*libdata.ModelDefinition) {
}

// GetInverseField return the inverse field, if applicable.
func (f *JSON) GetInverseField() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *JSON) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *JSON) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *JSON) IsStored() bool {
	return true
}

func (f *JSON) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *JSON) GetFieldData() *libdata.FieldData {
// 	return &libdata.FieldData{
// 		Name:            f.Name,
// 		NameWithoutUUID: f.Name,
// 		// Type:            JSONFieldType.GoType(),
// 		// GoType:          JSONFieldType.GoType(),
// 		// ProtoType:       JSONFieldType.ProtoType(),
// 		DBType: &libdata.DBType{
// 			Type:  "sql.NullString",
// 			Value: "String",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutUUID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseField:     "",
// 		// Definition:       f,
// 	}
// }
