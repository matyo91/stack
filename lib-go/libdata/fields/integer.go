package fields

import (
	// "strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
)

// IntegerFieldType contains the field type for Interger
const IntegerFieldType = libdata.FieldType("IntegerType")

/*Integer is the field type you can use in definition to declare an Integer field.
 */
type Integer struct {
	Name       string
	String     string
	Required   bool
	Unique     bool
	PrimaryKey bool
	Store      bool
	TitleName  string
	DBName     string
}

// GetName return the name of the field.
func (f *Integer) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *Integer) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Integer) NameWithoutUUID() string {
	return f.Name
}

// Title return the title of the field.
func (f *Integer) TitleWithoutUUID() string {
	return f.Title()
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Integer) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

// Type return the type of the field.
func (f *Integer) Type() libdata.FieldType {
	return IntegerFieldType
}

// Type return the type of the field.
func (f *Integer) GoType() string {
	return "int32"
}

// ProtoType return the protobuf type for this field.
func (f *Integer) ProtoType() string {
	return f.GoType()
}

// ProtoType return the protobuf type for this field.
func (f *Integer) DBType() *libdata.DBType {
	return &libdata.DBType{
		Type:  "sql.NullInt64",
		Value: "Int64",
	}
}

// Type return the type of the field.
func (f *Integer) GraphqlType() string {
	return f.GoType()
}

// Type return the type of the field.
func (f *Integer) GraphqlSchemaType() string {
	return "Int"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Integer) GetReferenceName() string {
	return ""
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Integer) GetReference() *libdata.ModelDefinition {
	return nil
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Integer) SetReference(*libdata.ModelDefinition) {
}

// GetInverseField return the inverse field, if applicable.
func (f *Integer) GetInverseField() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *Integer) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Integer) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *Integer) IsStored() bool {
	return true
}

func (f *Integer) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *Integer) GetFieldData() *libdata.FieldData {
// 	return &libdata.FieldData{
// 		Name:            f.Name,
// 		NameWithoutUUID: f.Name,
// 		// Type:            IntegerFieldType.GoType(),
// 		// GoType:          IntegerFieldType.GoType(),
// 		// ProtoType:       IntegerFieldType.ProtoType(),
// 		DBType: &libdata.DBType{
// 			Type:  "sql.NullInt64",
// 			Value: "Int64",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutUUID: f.Title(),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		Reference:        nil,
// 		InverseField:     "",
// 		// Definition:       f,
// 	}
// }
