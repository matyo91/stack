package fields

import (
	"strings"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
)

// Many2oneFieldType contains the field type for Many2one
const Many2oneFieldType = libdata.FieldType("Many2oneType")

/*Many2one is the field type you can use in definition to declare a Many2one field.
 */
type Many2one struct {
	Name                string
	String              string
	Reference           string
	ReferenceDefinition *libdata.ModelDefinition
	Required            bool
	Unique              bool
	PrimaryKey          bool
	Store               bool
	TitleName           string
	DBName              string
}

// GetName return the name of the field.
func (f *Many2one) GetName() string {
	return f.Name
}

// Title return the title of the field.
func (f *Many2one) Title() string {
	titleName := strcase.ToCamel(f.Name)
	if f.TitleName != "" {
		titleName = f.TitleName
	}
	return titleName
}

// Title return the title of the field.
func (f *Many2one) NameWithoutUUID() string {
	return strings.Replace(f.Name, "UUID", "", -1)
}

// Title return the title of the field.
func (f *Many2one) TitleWithoutUUID() string {
	return strings.Replace(f.Title(), "UUID", "", -1)
}

// Snake return the name of the field, in snake_case. This is essentially used for database.
func (f *Many2one) Snake() string {
	dbName := strcase.ToSnake(f.Name)
	if f.DBName != "" {
		dbName = f.DBName
	}
	return dbName
}

// Type return the type of the field.
func (f *Many2one) Type() libdata.FieldType {
	return Many2oneFieldType
}

// Type return the type of the field.
func (f *Many2one) GoType() string {
	return libdata.STRING
}

// ProtoType return the protobuf type for this field.
func (f *Many2one) ProtoType() string {
	return f.ReferenceDefinition.Title()
}

// ProtoType return the protobuf type for this field.
func (f *Many2one) DBType() *libdata.DBType {
	return &libdata.DBType{
		Type:  "sql.NullString",
		Value: "String",
	}
}

// Type return the type of the field.
func (f *Many2one) GraphqlType() string {
	return "graphql.ID"
}

// Type return the type of the field.
func (f *Many2one) GraphqlSchemaType() string {
	return "ID"
}

// GetReference return the name of referenced model, if this field is linked to another model.
func (f *Many2one) GetReferenceName() string {
	return f.Reference
}

// GetReferenceDefinition return the definition of referenced model,
// if this field is linked to another model.
func (f *Many2one) GetReference() *libdata.ModelDefinition {
	return f.ReferenceDefinition
}

// SetReferenceDefinition set the definition of referenced model,
// if this field is linked to another model.
func (f *Many2one) SetReference(d *libdata.ModelDefinition) {
	f.ReferenceDefinition = d
}

// GetInverseField return the inverse field, if applicable.
func (f *Many2one) GetInverseField() string {
	return ""
}

// GetRequired return if this field is required or not.
func (f *Many2one) GetRequired() bool {
	return f.Required
}

// GetPrimaryKey return if this field is a primary key or not.
func (f *Many2one) GetPrimaryKey() bool {
	return f.PrimaryKey
}

func (f *Many2one) IsStored() bool {
	return true
}

func (f *Many2one) IsNested() bool {
	return false
}

//GetFieldData return the field information in a format exploimodel by templates.
// func (f *Many2one) GetFieldData() *libdata.FieldData {

// 	// var reference *ReferenceData
// 	// referenceDefinition := f.GetReferenceDefinition()
// 	// if referenceDefinition != nil {
// 	// 	reference = &ReferenceData{}
// 	// 	if referenceDefinition != f.Model {
// 	// 		reference = &ReferenceData{
// 	// 			Name:  referenceDefinition.GetTemplateData().Name,
// 	// 			Title: referenceDefinition.GetTemplateData().Title,
// 	// 		}
// 	// 	} else {
// 	// 		reference = &ReferenceData{
// 	// 			Name:  result.Name,
// 	// 			Title: result.Title,
// 	// 		}
// 	// 	}
// 	// }

// 	return &libdata.FieldData{
// 		Name:            f.Name,
// 		NameWithoutUUID: strings.Replace(f.Name, "UUID", "", -1),
// 		Type:            "Many2one",
// 		// GoType:          Many2oneFieldType.GoType(),
// 		// ProtoType:       Many2oneFieldType.ProtoType(),
// 		DBType: &libdata.DBType{
// 			Type:  "sql.NullString",
// 			Value: "String",
// 		},
// 		Title:            f.Title(),
// 		TitleWithoutUUID: strings.Replace(f.Title(), "UUID", "", -1),
// 		LesserTitle:      strings.Title(f.GetName()),
// 		Snake:            f.Snake(),
// 		Required:         f.Required,
// 		Nested:           false,
// 		InverseField:     "",
// 		// Definition:       f,
// 	}
// }
