package libdata

// CreatedAtField contains the name of the created_at field.
const CreatedAtField = "created_at"

// UpdatedAtField contains the name of the updated_at field.
const UpdatedAtField = "updated_at"

// STRING is a contains reprensenting the widely used string type.
const STRING = "string"

// FieldType is the generic type for a data field.
type FieldType string

// DBType contains the type and the value.
type DBType struct {
	Type  string
	Value string
}

// Field is an interface to get the data from the field.
type Field interface {
	GetName() string
	NameWithoutUUID() string
	Title() string
	TitleWithoutUUID() string
	Snake() string
	Type() FieldType
	GoType() string
	ProtoType() string
	DBType() *DBType
	GraphqlType() string
	GraphqlSchemaType() string
	GetReferenceName() string
	GetReference() *ModelDefinition
	// GetReferenceDefinition() *ModelDefinition
	SetReference(*ModelDefinition)
	GetInverseField() string
	GetRequired() bool
	GetPrimaryKey() bool
	// GetFieldData() *FieldData
	IsStored() bool
	IsNested() bool
}

// FieldData containt the data in a format exploimodel by templates.
// type FieldData struct {
// 	Name             string
// 	NameWithoutUUID  string
// 	Type             string
// 	GoType           string
// 	ProtoType        string
// 	DBType           *DBType
// 	Title            string
// 	TitleWithoutUUID string
// 	LesserTitle      string
// 	Snake            string
// 	Required         bool
// 	Nested           bool
// 	Reference        *ReferenceData
// 	InverseField     string
// 	Model            *ModelData
// 	Definition       Field
// }
