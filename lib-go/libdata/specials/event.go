package specials

import (
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	nats "github.com/nats-io/nats.go"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libdata/fields"
)

var EventModel = &libdata.ModelDefinition{
	Name: "event",
	Fields: []libdata.Field{
		&fields.Text{Name: "aggregate", String: "Aggregate", Required: true},
		&fields.Text{Name: "aggregateUUID", String: "Aggregate UUID", Required: true},
		&fields.Text{Name: "name", String: "Name", Required: true},
		&fields.JSON{Name: "payload", String: "Payload", Required: true},
		&fields.Text{Name: "idempotencyKey", String: "Idempotency Key"},
		&fields.Boolean{Name: "published", String: "Published", Required: true},
	},
	Datetime:    true,
	CanAssignID: false,
	StoreCustomFunctions: `//InsertBuildArgs todo
		func (s *EventStore) NewEvent(
			aggregate string, aggregateUUID string, name string, payload []byte,
		) libdata.EventInterface {
			
			return &pb.Event{
				UUID: uuid.New().String(),
				Aggregate: aggregate,
				AggregateUUID: aggregateUUID,
				Name: name,
				Payload: payload,
			}
		}
		
		func (e *Event) GetName() string {
			return e.Name 
		}
		func (e *Event) GetAggregate() string {
			return e.Aggregate
		}
		func (e *Event) GetPayload() []byte {
			return e.Payload
		}`,
}

func init() {

	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}

	fmt.Println(nats.DefaultURL)
	fmt.Println(os.Getenv("NATS_HOST"))
	libdata.NatsClientToRemoveInFavorOfDriver, err = nats.Connect(os.Getenv("NATS_HOST"))
	if err != nil {
		fmt.Printf("Impossible to connect to NATS: %v\n", err.Error())
	}

	libdata.EventModel = EventModel

}
