package libdata

import (
	"context"
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
	nats "github.com/nats-io/nats.go"
	"github.com/pkg/errors"

	fieldmask "gitlab.com/empowerlab/stack/lib-go/ptypes/fieldmask"
)

var NatsClientToRemoveInFavorOfDriver *nats.Conn

// EventModel contain the definition of the event model.
var EventModel *ModelDefinition

type EventInterface interface {
	ModelInterface
	GetName() string
	GetAggregate() string
	GetPayload() []byte
}

type eventDriver interface {
	RegisterEvent(*Transaction, string, string, string, []byte) (EventInterface, error)
}

// EventDriver is the driver which shall be used to generate event.
var EventDriver eventDriver

// EventStore ensure the EventModel contain the needed function.
type EventStore interface {
	NewEvent(string, string, string, []byte) EventInterface
}

// Transaction contains the information on the transaction.
type Transaction struct {
	Type     string
	WriteTx  interface{}
	ReadTx   interface{}
	MemoryTx interface{}
	Cluster  *Cluster
	Tenant   *Tenant
	events   []EventInterface
	UseCache bool
}

// CommitTransaction will commit the transaction.
// nolint: unparam
func (tx *Transaction) CommitTransaction(ctx context.Context, err error) error {
	if err != nil {
		fmt.Println("Rollback: ", err)
		return tx.Cluster.Driver.RollbackTransaction(tx)
	}

	err = tx.InsertEvents(ctx)
	if err != nil {
		return errors.Wrap(err, "Couldn't insert events")
	}

	err = tx.Cluster.Driver.CommitTransaction(tx)
	if err != nil {
		return errors.Wrap(err, "Couldn't commit")
	}

	tx.PublishEvents()

	return nil

}

// Select will query the cluster to return the records, filtered by arguments.
func (tx *Transaction) Select(
	ctx context.Context, t *ModelDefinition, filters []*Filter, limit *uint, orderBy []*OrderByInput,
) (interface{}, error) {
	return t.Select(ctx, tx, filters, limit, orderBy)
}

// Insert will insert the records in the cluster.
func (tx *Transaction) Insert(
	ctx context.Context, t *ModelDefinition, records []ModelInterface) (interface{}, error) {

	return t.Insert(ctx, tx, records)
}

// Update will update the records, filtered by arguments, with the specified data.
func (tx *Transaction) Update(
	ctx context.Context, t *ModelDefinition, filters []*Filter,
	record interface{}, updateMask *fieldmask.FieldMask) (interface{}, error) {

	return t.Update(ctx, tx, filters, record, updateMask)
}

// Delete will delete the records, filtered by arguments.
func (tx *Transaction) Delete(
	ctx context.Context, t *ModelDefinition, filters []*Filter) error {

	return t.Delete(ctx, tx, filters)
}

// RegisterEvent will register the event in the transaction event queue.
func (tx *Transaction) RegisterEvent(
	aggregate string, aggregateUUID string, name string, payload []byte,
) error {
	e, err := EventDriver.RegisterEvent(tx, aggregate, aggregateUUID, name, payload)
	tx.events = append(tx.events, e)
	return err
}

// InsertEvents will insert the events in the database at the end of the transaction.
func (tx *Transaction) InsertEvents(ctx context.Context) error {
	for _, event := range tx.events {
		rows, err := EventModel.Insert(ctx, tx, []ModelInterface{event})
		if err != nil {
			return err
		}
		err = rows.(*sqlx.Rows).Close()
		if err != nil {
			return err
		}
	}
	return nil
}

func (tx *Transaction) PublishEvents() {
	for _, event := range tx.events {
		fmt.Println("Publish ", event.GetAggregate()+strings.Title(event.GetName()))
		err := NatsClientToRemoveInFavorOfDriver.Publish(event.GetAggregate()+strings.Title(event.GetName()), event.GetPayload())
		if err != nil {
			fmt.Printf("Error publishing event: %v", err)
		}
	}
}
