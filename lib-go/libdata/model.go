package libdata

import (
	"context"
	"fmt"
	"strings"
	// "time"

	"github.com/iancoleman/strcase"

	fieldmask "gitlab.com/empowerlab/stack/lib-go/ptypes/fieldmask"
)

// MigrationPath contains the path to the directory containing the migrations.
var MigrationPath = "migrations/"

// Filter is the struct used to declare a filter.
type Filter struct {
	Field    string
	Operator string
	Operande interface{}
}

// OrderByInput is the struct used to declare the order by of our request.
type OrderByInput struct {
	Field string
	Desc  bool
}

// ModelInterface is an interface containing the function on our model.
type ModelInterface interface {
	GetUUID() string
	Marshal() ([]byte, error)
	Unmarshal([]byte) error
}

// ModelDefinition is the struct used to declare a new model definition.
//nolint: maligned
type ModelDefinition struct {
	Cluster              *Cluster
	Store                interface{}
	Name                 string
	Fields               []Field
	Datetime             bool
	SafeDelete           bool
	CanAssignID          bool
	NoID                 bool
	UseOneOf             bool
	Gorm                 interface{}
	StoreCustomFunctions string
	DisableDatabaseStore bool
	ExternalDriver       ExternalDriver
}

// Select can be called from the ModelDefinition and will return the records from the cluster,
// filtered by the arguments.
// nolint: unparam
func (t *ModelDefinition) Select(
	ctx context.Context, tx *Transaction, filters []*Filter, limit *uint, orderBy []*OrderByInput,
) (interface{}, error) {
	return t.Cluster.Driver.Select(ctx, tx, t, filters, limit, orderBy)
}

// Insert can be called from the ModelDefinition and will insert the records from the cluster,
// and return it.
// nolint: unparam
func (t *ModelDefinition) Insert(
	ctx context.Context, tx *Transaction, records []ModelInterface) (interface{}, error) {

	fmt.Printf("t %+v\n", t)
	fmt.Printf("t.Cluster %+v\n", t.Cluster)
	fmt.Printf("tx %+v\n", tx)
	fmt.Printf("records %+v\n", records)

	return t.Cluster.Driver.Insert(ctx, tx, t, records)
}

// Update can be called from the ModelDefinition and will update the records in the cluster,
// filtered by the arguments.
// nolint: unparam
func (t *ModelDefinition) Update(
	ctx context.Context, tx *Transaction, filters []*Filter,
	record interface{}, updateMask *fieldmask.FieldMask) (interface{}, error) {

	return t.Cluster.Driver.Update(ctx, tx, t, filters, record, updateMask)
}

// Delete can be called from the ModelDefinition and will delete the records in the cluster,
// filtered by the arguments.
// nolint: unparam
func (t *ModelDefinition) Delete(ctx context.Context, tx *Transaction, filters []*Filter) error {
	return t.Cluster.Driver.Delete(ctx, tx, t, filters)
}

// GetStoreCustomFunctions return the custom functions defined in this model.
func (t *ModelDefinition) GetStoreCustomFunctions() string {
	if t.StoreCustomFunctions == "" {
		return t.StoreCustomFunctions
	}
	return ""
}

func (t *ModelDefinition) StoredFields() []Field {
	var storedFields []Field
	for _, field := range t.Fields {
		if field.IsStored() {
			storedFields = append(storedFields, field)
		}
	}
	return storedFields
}

func (t *ModelDefinition) NestedFields() []Field {
	var nestedFields []Field
	for _, field := range t.Fields {
		if !field.IsStored() {
			nestedFields = append(nestedFields, field)
		}
	}
	return nestedFields
}

func (t *ModelDefinition) SourceFields() string {
	var sourceFields []string
	for _, sourceField := range t.Fields {
		source := fmt.Sprintf("source.%s", sourceField.Title())
		if string(sourceField.Type()) == "Many2oneType" {
			source = fmt.Sprintf("&mpb.%s{UUID: string(source.%s)}", sourceField.GetReference().Title(), sourceField.Title())
		}
		sourceFields = append(sourceFields, fmt.Sprintf(
			"%s: %s,",
			sourceField.Title(), source,
		))
	}
	return strings.Join(sourceFields, "\n")
}

func (t *ModelDefinition) Title() string {
	return strings.Title(t.Name)
}

func (t *ModelDefinition) Snake() string {
	return strcase.ToSnake(t.Name)
}

// ModelData contains the model information, in a format exploimodel by templates.
// type ModelData struct {
// 	Name                 string
// 	Title                string
// 	Snake                string
// 	Fields               []*FieldData
// 	StoredFields         []*FieldData
// 	NestedFields         []*FieldData
// 	Timestamp            time.Time
// 	UseTime              bool
// 	Definition           *ModelDefinition
// 	StoreCustomFunctions string
// }

// ReferenceData contain the reference information, in a format exploimodel by templates.
// type ReferenceData struct {
// 	Name         string
// 	Title        string
// 	Snake        string
// 	SourceFields string
// 	Definition   *ModelDefinition
// }

// GetTemplateData return the model information, in a format exploimodel by templates.
// func (t *ModelDefinition) GetTemplateData() *ModelData {

// 	result := &ModelData{
// 		Name:                 t.Name,
// 		Title:                strings.Title(t.Name),
// 		Snake:                strcase.ToSnake(t.Name),
// 		Timestamp:            time.Now(),
// 		Definition:           t,
// 		StoreCustomFunctions: t.GetStoreCustomFunctions(),
// 	}

// 	var fields []*FieldData
// 	var storedFields []*FieldData
// 	var nestedFields []*FieldData
// 	for _, field := range t.Fields {

// 		var reference *ReferenceData
// 		// referenceDefinition := field.GetReferenceDefinition()
// 		// if referenceDefinition != nil {

// 		// 	var sourceFields []string
// 		// 	for _, sourceField := range referenceDefinition.Fields {
// 		// 		source := fmt.Sprintf("source.%s", sourceField.GetTitleName())
// 		// 		if string(sourceField.Type()) == "Many2oneType" {
// 		// 			source = fmt.Sprintf("string(source.%s)", sourceField.GetTitleName())
// 		// 		}
// 		// 		sourceFields = append(sourceFields, fmt.Sprintf(
// 		// 			"%s: %s,",
// 		// 			sourceField.GetTitleName(), source,
// 		// 		))
// 		// 	}

// 		// 	if referenceDefinition != t {
// 		// 		reference = &ReferenceData{
// 		// 			// Name:         field.GetReference(),
// 		// 			// Title:        strings.Title(field.GetReference()),
// 		// 			SourceFields: strings.Join(sourceFields, "\n"),
// 		// 			Definition:   referenceDefinition,
// 		// 		}
// 		// 	} else {
// 		// 		reference = &ReferenceData{
// 		// 			Name:         result.Name,
// 		// 			Title:        result.Title,
// 		// 			Snake:        result.Snake,
// 		// 			SourceFields: strings.Join(sourceFields, "\n"),
// 		// 			Definition:   t,
// 		// 		}
// 		// 	}
// 		// }

// 		fieldData := field.GetFieldData()
// 		fieldData.Reference = reference
// 		fieldData.Model = result

// 		// if field.Type() == DateFieldType || field.Type() == DatetimeFieldType {
// 		// 	result.UseTime = true
// 		// }

// 		fields = append(fields, fieldData)

// 		if field.IsStored() {
// 			storedFields = append(storedFields, fieldData)
// 		} else {
// 			nestedFields = append(nestedFields, fieldData)
// 		}
// 	}

// 	result.Fields = fields
// 	result.StoredFields = storedFields
// 	result.NestedFields = nestedFields
// 	return result
// }
