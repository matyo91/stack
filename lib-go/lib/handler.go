package lib

import (
	"bytes"
	"fmt"
	"net/http"
)

// nolint: unparam
func respond(w http.ResponseWriter, body []byte, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	_, err := w.Write(body)
	if err != nil {
		fmt.Printf("err %s\n", err)
	}
}

// nolint: unparam
func errorJSON(msg string) []byte {
	buf := bytes.Buffer{}
	_, err := fmt.Fprintf(&buf, `{"error": "%s"}`, msg)
	if err != nil {
		fmt.Printf("err %s\n", err)
	}
	return buf.Bytes()
}
