package libgraphql

import (
	"fmt"
	"strings"

	"gitlab.com/empowerlab/stack/lib-go/libgrpc"
	"gitlab.com/empowerlab/stack/lib-go/liborm"
)

// Definitions contains all the graphql definitions in the service.
type Definitions struct {
	Name       string
	Repository string
	slice      []*Definition
	byIds      map[string]*Definition
}

// GetTitle is used to return the name of the definition with InitCap
func (ds *Definitions) GetTitle() string {
	return strings.Title(ds.Name)
}

// Register is used to register a new definition into the service.
func (ds *Definitions) Register(d *Definition) {

	if ds.byIds == nil {
		ds.byIds = map[string]*Definition{}
	}

	ds.slice = append(ds.slice, d)
	ds.byIds[d.Grpc.ORM.Model.Name] = d
}

// Slice return the definitions as a slice.
func (ds *Definitions) Slice() []*Definition {
	return ds.slice
}

// GetByID return the specified definition by its ID.
func (ds *Definitions) GetByID(id string) *Definition {
	d := ds.byIds[id]
	if d == nil {
		panic(fmt.Sprintf("The graphql definition %s doesn't exist", id))
	}
	return d
}

// Definition is used to declare the information of a model, so it can generate its code.
type Definition struct {
	Grpc          *libgrpc.Definition
	CustomFuncs   []*liborm.CustomFunc
	DisableSelect bool
	DisableCreate bool
	DisableUpdate bool
	DisableDelete bool
}

// GetCustomFuncsData return the custom function information for collections and pools,
// in a format usable by templates.
// func (d *Definition) GetCustomFuncsData() []*liborm.CustomFuncData {
// 	var result []*liborm.CustomFuncData
// 	for _, c := range d.CustomFuncs {
// 		result = append(result, c.GetCustomFuncData())
// 	}
// 	return result
// }
