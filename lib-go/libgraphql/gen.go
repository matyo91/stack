package libgraphql

import (
	"bytes"
	"fmt"
	"go/format"
	"io/ioutil"
	"os"
	"text/template"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libdata/fields"
	"gitlab.com/empowerlab/stack/lib-go/liborm"
)

// type data struct {
// 	Name string
// 	NameInitCap string
// 	FieldArgs string
// 	FieldsResolvers string
// 	FieldsStruct string
// 	CreateFunc string
// }

const graphqlID = "graphql.ID"

// Gen will generate the graphql files.
func (defs *Definitions) Gen() {

	// tCreateFunc := template.Must(template.New("createFunc").Parse(createFuncTemplate))
	// tgraphqlTenant := template.Must(template.New("graphqlTenant").Parse(graphqlTenantTemplate))

	// fmt.Println(definitions.Slice())
	// for _, definition := range definitions.Slice() {

	d := struct {
		Repository string
		Title      string
	}{
		Repository: defs.Repository,
		Title:      defs.GetTitle(),
	}
	// 		Name: definition.Model.Model.Name,
	// 		NameInitCap: strings.Title(definition.Model.Model.Name),
	// 	}

	// 	var fieldArgs []string
	// 	var fieldsResolvers []string
	// 	for _, field := range definition.Model.Model.Fields {
	// 		var fieldType string
	// 		var content string
	// 		switch field.Type {
	// 		case libdata.IDType:
	// 			fieldType = "graphql.ID"
	// 			content = fmt.Sprintf("return graphql.ID(tr.t.%s)", strings.Title(field.Name))
	// 		case libdata.TextType:
	// 			fieldType = "string"
	// 			content = fmt.Sprintf("return tr.t.%s", strings.Title(field.Name))
	// 		case libdata.BooleanType:
	// 			fieldType = "bool"
	// 			content = fmt.Sprintf("return tr.t.%s", strings.Title(field.Name))
	// 		}

	// 		fieldsResolvers = append(fieldsResolvers, fmt.Sprintf(
	// 			"// %s todo\nfunc (tr *%sResolver) %s() %s {\n%s\n}",
	// 			strings.Title(field.Name), d.NameInitCap, strings.Title(field.Name),
	// 			fieldType, content))
	// 		fieldArgs = append(fieldArgs, fmt.Sprintf("%s %s", strings.Title(field.Name), fieldType))
	// 	}

	// 	d.FieldsResolvers = strings.Join(fieldsResolvers, "\n\n")
	// 	d.FieldArgs = strings.Join(fieldArgs, "\n")

	// 	// createFuncbuf := &bytes.Buffer{}
	// 	// tCreateFunc.Execute(createFuncbuf, d)
	// 	// d.CreateFunc = createFuncbuf.String()

	// 	fmt.Println(d.CreateFunc)

	err := os.MkdirAll("gen/graphql", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	// file := fmt.Sprintf("grpc/gen/%s.go", d.Name)
	buf := &bytes.Buffer{}
	err = initTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(err)
	}
	content, err := format.Source(buf.Bytes())
	if err != nil {
		fmt.Println(buf.String())
		fmt.Println("init graphql ", err)
	}
	err = ioutil.WriteFile(
		"gen/graphql/init.gen.go",
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(defs.Slice())
	for _, definition := range defs.Slice() {

		d := struct {
			Repository    string
			ServiceTitle  string
			Model         *libdata.ModelDefinition
			CustomFuncs   []*liborm.CustomFunc
			DisableSelect bool
			DisableCreate bool
			DisableUpdate bool
			DisableDelete bool
		}{
			Repository:    defs.Repository,
			ServiceTitle:  defs.GetTitle(),
			Model:         definition.Grpc.ORM.Model,
			CustomFuncs:   definition.CustomFuncs,
			DisableSelect: definition.DisableSelect,
			DisableCreate: definition.DisableCreate,
			DisableUpdate: definition.DisableUpdate,
			DisableDelete: definition.DisableDelete,
		}

		// for _, field := range d.Model.Fields {
		// 	if field.Type() == fields.IDFieldType {
		// 		field.Type = graphqlID
		// 	}
		// 	if field.Definition.Type() == libdata.Many2oneFieldType {
		// 		field.Type = graphqlID
		// 	}
		// 	if field.Nested {
		// 		field.Type = field.Definition.GetReferenceDefinition().GetTemplateData().Title
		// 	}
		// }

		// var fieldArgs []string
		// var fieldsResolvers []string
		// for _, field := range definition.Model.Model.Fields {
		// 	var fieldType string
		// 	var content string
		// 	switch field.Type {
		// 	case libdata.IDFieldType:
		// 		fieldType = "graphql.ID"
		// 		content = fmt.Sprintf("return graphql.ID(tr.t.%s)", strings.Title(field.Name))
		// 	case libdata.TextFieldType:
		// 		fieldType = "string"
		// 		content = fmt.Sprintf("return tr.t.%s", strings.Title(field.Name))
		// 	case libdata.BooleanFieldType:
		// 		fieldType = "bool"
		// 		content = fmt.Sprintf("return tr.t.%s", strings.Title(field.Name))
		// 	}

		// 	fieldsResolvers = append(fieldsResolvers, fmt.Sprintf(
		// 		"// %s todo\nfunc (tr *%sResolver) %s() %s {\n%s\n}",
		// 		strings.Title(field.Name), d.NameInitCap, strings.Title(field.Name),
		// 		fieldType, content))
		// 	fieldArgs = append(fieldArgs, fmt.Sprintf("%s %s", strings.Title(field.Name), fieldType))
		// }

		// d.FieldsResolvers = strings.Join(fieldsResolvers, "\n\n")
		// d.FieldArgs = strings.Join(fieldArgs, "\n")

		// createFuncbuf := &bytes.Buffer{}
		// tCreateFunc.Execute(createFuncbuf, d)
		// d.CreateFunc = createFuncbuf.String()

		// fmt.Println(d.CreateFunc)

		// file := fmt.Sprintf("grpc/gen/%s.go", d.Name)
		buf := &bytes.Buffer{}
		err = graphqlTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(err)
		}
		// content, err := format.Source(buf.Bytes())
		// if err != nil {
		// 	fmt.Println(buf.String())
		// 	fmt.Println("graphql ", err)
		// }
		err = ioutil.WriteFile(
			fmt.Sprintf("gen/graphql/%s.gen.go", d.Model.Snake()),
			buf.Bytes(), 0644)
		if err != nil {
			fmt.Println(err)
		}

	}
}

var graphqlTemplate = template.Must(template.New("").Funcs(template.FuncMap{
	"prepareNullableID": func(field libdata.Field, update bool) string {

		var content string
		var updateMask string
		if update || !field.GetRequired() {

			if field.GraphqlType() == graphqlID {
				content = `
				var {{.Field.Name}} string
				if args.Data.{{.Field.Title}} != nil {
					{{.Field.Name}} = string(*args.Data.{{.Field.Title}}){{.UpdateMask}}
				}
				`
			} else if field.IsNested() {
				if !update {
					content = `
					{{.Field.Name}} := &mpb.Create{{.Field.GetReference.Title}}ManyRequest{}
					if args.Data.{{.Field.Title}}.Create != nil {
						for _, source := range *args.Data.{{.Field.Title}}.Create {
							{{.Field.Name}}.Create = append(
								{{.Field.Name}}.Create,
								&mpb.Create{{.Field.GetReference.Title}}Request{
									{{.Field.GetReference.Title}}: &mpb.{{.Field.GetReference.Title}}{
										{{.Field.GetReference.SourceFields}}
									},
								},
							)
						}
					}
					`
				} else {
					content = `var {{.Field.Name}} *mpb.Update{{.Field.GetReference.Title}}ManyRequest`
				}
			} else {
				content = `
				var {{.Field.Name}} {{.Field.GoType}}
				if args.Data.{{.Field.Title}} != nil {
					{{.Field.Name}} = *args.Data.{{.Field.Title}}{{.UpdateMask}}
				}
				`
			}
			if update {
				updateMask = `
				updateMask = append(updateMask, "` + field.GetName() + `")`
			}

		}

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, &struct {
			Field      libdata.Field
			UpdateMask string
		}{
			Field:      field,
			UpdateMask: updateMask,
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"convertToResolver": func(model *libdata.ModelDefinition, field libdata.Field) string {
		var content string
		if !field.GetRequired() {

			if field.Type() == fields.Many2oneFieldType {
				content = `
				// {{.Field.TitleWithoutUUID}} return the value of the graphql field {{.Field.TitleWithoutUUID}}
				// for a {{.Model.Name}} object.
				func (tr *{{.Model.Title}}Resolver) {{.Field.TitleWithoutUUID}}() *{{.Field.GetReference.Title}}Resolver {
					{{.Field.Reference.Name}}, _ := GrpcClient.Get{{.Field.GetReference.Title}}(
						context.Background(), string(tr.t.{{.Field.Title}}))
					return &{{.Field.GetReference.Title}}Resolver{
						t: {{.Field.GetReference.Name}},
					}
				}`
			} else if field.IsNested() {
				content = `
				// {{.Field.TitleWithoutUUID}} return the value of the graphql field {{.Field.TitleWithoutUUID}}
				// for a {{.Model.Name}} object.
				func (tr *{{.Model.Title}}Resolver) {{.Field.TitleWithoutUUID}}() []*{{.Field.GraphqlType}}Resolver {
					var result []*{{.Field.GraphqlType}}Resolver
					{{.Field.Title}}, _ := GrpcClient.Get{{.Field.Title}}By{{.Model.Title}}(context.Background(), tr.t.UUID)
					for _, {{.Field.GetReference.Name}} := range {{.Field.Title}} {
						result = append(result, &{{.Field.GraphqlType}}Resolver{
							t: {{.Field.GetReference.Name}},
						})
					}
					return result
				}`
			} else if field.GraphqlType() == graphqlID {
				content = `
				// {{.Field.TitleWithoutUUID}} return the value of the graphql field {{.Field.TitleWithoutUUID}}
				// for a {{.Model.Name}} object.
				func (tr *{{.Model.Title}}Resolver) {{.Field.TitleWithoutUUID}}() *graphql.ID {
					var result *graphql.ID
					if tr.t.{{.Field.Title}} != "" {
						v := graphql.ID(tr.t.{{.Field.Title}})
						result = &v
					}
					return result
				}`
			} else {
				content = `
				// {{.Field.TitleWithoutUUID}} return the value of the graphql field {{.Field.TitleWithoutUUID}}
				// for a {{.Model.Name}} object.
				func (tr *{{.Model.Title}}Resolver) {{.Field.TitleWithoutUUID}}() *{{.Field.GoType}} {
					var result *{{.Field.GraphqlType}}
					if tr.t.{{.Field.Title}} != "" {
						v := tr.t.{{.Field.Title}}
						result = &v
					}
					return result
				}`
			}
		} else if field.Type() == fields.Many2oneFieldType {
			content = `
			// {{.Field.TitleWithoutUUID}} return the value of the graphql field {{.Field.TitleWithoutUUID}}
			// for a {{.Model.Name}} object.
			func (tr *{{.Model.Title}}Resolver) {{.Field.TitleWithoutUUID}}() *{{.Field.GetReference.Title}}Resolver {
				{{.Field.GetReference.Name}}, _ := GrpcClient.Get{{.Field.GetReference.Title}}By{{.Model.Title}}(
					context.Background(), tr.t.{{.Field.Title}}.UUID)
				return &{{.Field.GetReference.Title}}Resolver{
					t: {{.Field.GetReference.Name}},
				}
			}`
		} else if field.Type() == graphqlID {
			content = `
			// {{.Field.TitleWithoutUUID}} return the value of the graphql field {{.Field.TitleWithoutUUID}}
			// for a {{.Model.Name}} object.
			func (tr *{{.Model.Title}}Resolver) {{.Field.TitleWithoutUUID}}() graphql.ID {
				return graphql.ID(tr.t.{{.Field.Title}})
			}`
		} else {
			content = `
			// {{.Field.TitleWithoutUUID}} return the value of the graphql field {{.Field.TitleWithoutUUID}}
			// for a {{.Model.Name}} object.
			func (tr *{{.Model.Title}}Resolver) {{.Field.TitleWithoutUUID}}() {{.Field.GoType}} {
				return tr.t.{{.Field.Title}}
			}`
		}

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, &struct {
			Field libdata.Field
			Model *libdata.ModelDefinition
		}{
			Field: field,
			Model: model,
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"convertToModel": func(field libdata.Field) string {
		var content string
		if !field.GetRequired() {
			content = `{{.Name}}`
		} else if field.GraphqlType() == graphqlID {
			if field.Type() == fields.Many2oneFieldType {
				content = `&mpb.{{.GetReference.Title}}{UUID: string(args.Data.{{.Title}})}`
			} else {
				content = `string(args.Data.{{.Title}})`
			}
		} else {
			content = `args.Data.{{.Title}}`
		}

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, field)
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
}).Parse(`package graphql

import (
	"time"
	"fmt"

	graphql "github.com/graph-gophers/graphql-go"
	"github.com/pkg/errors"
	"golang.org/x/net/context"
	"github.com/gogo/protobuf/types"
	"gitlab.com/empowerlab/stack/lib-go/libdata"
	opentracing "github.com/opentracing/opentracing-go"

	fieldmask "gitlab.com/empowerlab/stack/lib-go/ptypes/fieldmask"
	mpb "{{.Repository}}/gen/orm/pb"
	"{{.Repository}}/gen/orm"
)

var dummyTime{{.Model.Title}} time.Time

//random stuff to avoid import problems
func init() {
	var tracer opentracing.Tracer
	duration := &types.Duration{}
	filter := &libdata.Filter{}
	request := mpb.GetRequest{}
	fieldmask := &fieldmask.FieldMask{}
	_ = fmt.Sprintf("%v %v %v %v %v", tracer, duration, filter, request, fieldmask)
}

/*{{.Model.Title}}Resolver is a struct which will contains functions
 *to resolve the fields of the {{.Model.Title}} objects.
 */
type {{.Model.Title}}Resolver struct {
	t *orm.{{.Model.Title}}
}

// ID return the value of the graphql field ID for a {{.Model.Name}} object.
func (tr *{{.Model.Title}}Resolver) ID() graphql.ID {
	return graphql.ID(tr.t.UUID)
}

{{- range .Model.Fields }}

{{convertToResolver $.Model .}}

{{- end }}

/*{{.Model.Title}}ConnectionResolver is used to resolve metainformation 
 *for the {{.Model.Title}} objects.
 */
type {{.Model.Title}}ConnectionResolver struct {
	pageInfo *PageInfoResolver
	edges *[]*{{.Model.Title}}EdgeResolver
	aggregate *AggregateResolver
}
// PageInfo is not implemented.
func (tr *{{.Model.Title}}ConnectionResolver) PageInfo() *PageInfoResolver {
	return tr.pageInfo
}
// Edges is not implemented.
func (tr *{{.Model.Title}}ConnectionResolver) Edges() *[]*{{.Model.Title}}EdgeResolver {
	return tr.edges
}
// Aggregate is not implemented.
func (tr *{{.Model.Title}}ConnectionResolver) Aggregate() *AggregateResolver {
	return tr.aggregate
}

/*{{.Model.Title}}EdgeResolver is not implemented.
 */
type {{.Model.Title}}EdgeResolver struct {
	node *{{.Model.Title}}Resolver
	cursor string
}
// Node is not implemented.
func (tr *{{.Model.Title}}EdgeResolver) Node() *{{.Model.Title}}Resolver {
	return tr.node
}
// Cursor is not implemented.
func (tr *{{.Model.Title}}EdgeResolver) Cursor() string {
	return tr.cursor
}

/*{{.Model.Title}}SubscriptionPayloadResolver is not implemented.
 */
type {{.Model.Title}}SubscriptionPayloadResolver struct {
	mutation string
	node *{{.Model.Title}}Resolver
	updatedFields *[]string
	previousValues *{{.Model.Title}}Resolver
}
// Mutation is not implemented.
func (tr *{{.Model.Title}}SubscriptionPayloadResolver) Mutation() string {
	return tr.mutation
}
// Node is not implemented.
func (tr *{{.Model.Title}}SubscriptionPayloadResolver) Node() *{{.Model.Title}}Resolver {
	return tr.node
}
// UpdatedFields is not implemented.
func (tr *{{.Model.Title}}SubscriptionPayloadResolver) UpdatedFields() *[]string {
	return tr.updatedFields
}
// PreviousValues is not implemented.
func (tr *{{.Model.Title}}SubscriptionPayloadResolver) PreviousValues() *{{.Model.Title}}Resolver {
	return tr.previousValues
}

/*{{.Model.Title}}CreateInput is the base graphql input for creating {{.Model.Title}} objects,
 *containing all his fields.
 */
type {{.Model.Title}}CreateInput struct {
	{{- range .Model.Fields }}
	{{.Title}} {{if not .GetRequired}}*{{end}}{{.GraphqlType}}{{if .IsNested}}CreateManyInput{{end}}
	{{- end }}
}

// {{.Model.Title}}CreateManyInput is a graphql input used to declare relational operations
// for {{.Model.Title}} objects.
type {{.Model.Title}}CreateManyInput struct {
	Create *[]*{{.Model.Title}}CreateInput
	Connect *[]*{{.Model.Title}}WhereUniqueInput
}

/*{{.Model.Title}}UpdateInput is the base graphql input for updating {{.Model.Title}} objects,
 *containing all his fields.
 */
type {{.Model.Title}}UpdateInput struct {
	{{- range .Model.Fields }}
	{{.Title}} *{{.GraphqlType}}{{if .IsNested}}UpdateManyInput{{end}}
	{{- end }}
}

// {{.Model.Title}}UpdateManyInput is a graphql input used to declare relational operations
// for {{.Model.Title}} objects.
type {{.Model.Title}}UpdateManyInput struct {
	Create *[]*{{.Model.Title}}CreateInput
	Connect *[]*{{.Model.Title}}WhereUniqueInput
	Set *[]*{{.Model.Title}}WhereUniqueInput
	Disconnect *[]*{{.Model.Title}}WhereUniqueInput
	Delete *[]*{{.Model.Title}}WhereUniqueInput
	Update *[]*{{.Model.Title}}UpdateWithWhereUniqueNestedInput
	UpdateMany *[]*{{.Model.Title}}UpdateManyWithWhereNestedInput
	DeleteMany *[]*{{.Model.Title}}WhereInput
	Upsert *[]*{{.Model.Title}}UpsertWithWhereUniqueNestedInput
}

// {{.Model.Title}}UpdateManyMutationInput is an alternative base graphql input for 
// updating {{.Model.Title}} objects, containing all his fields.
type {{.Model.Title}}UpdateManyMutationInput struct {
	{{- range .Model.Fields }}
	{{.Title}} {{if not .GetRequired}}*{{end}}{{.GraphqlType}}{{if .IsNested}}UpdateManyInput{{end}}
	{{- end }}
}

// {{.Model.Title}}UpdateManyWithWhereNestedInput is used to update several {{.Model.Title}} records
// in a nested context.
type {{.Model.Title}}UpdateManyWithWhereNestedInput struct {
	Where {{.Model.Title}}WhereInput
	Data {{.Model.Title}}UpdateManyInput
}

// {{.Model.Title}}UpdateWithWhereUniqueNestedInput is used to update 
// one {{.Model.Title}} record in a nested context.
type {{.Model.Title}}UpdateWithWhereUniqueNestedInput struct {
	Where {{.Model.Title}}WhereUniqueInput
	Data {{.Model.Title}}UpdateInput
}

// {{.Model.Title}}UpsertWithWhereUniqueNestedInput is used to create or update one
// {{.Model.Title}} record in a nested context.
type {{.Model.Title}}UpsertWithWhereUniqueNestedInput struct {
	Where {{.Model.Title}}WhereUniqueInput
	Update {{.Model.Title}}UpdateInput
	Create {{.Model.Title}}CreateInput
}

/*{{.Model.Title}}WhereInput is used to specify filter on a request using a {{.Model.Title}} object.
 */
//nolint: golint
type {{.Model.Title}}WhereInput struct {
	AND *[]{{.Model.Title}}WhereInput
	OR *[]{{.Model.Title}}WhereInput
	NOT *[]{{.Model.Title}}WhereInput
	ID *graphql.ID
	ID_not *graphql.ID
	ID_in *[]graphql.ID
	ID_not_in *[]graphql.ID
	ID_lt *graphql.ID
	ID_lte *graphql.ID
	ID_gt *graphql.ID
	ID_gte *graphql.ID
	ID_contains *graphql.ID
	ID_not_contains *graphql.ID
	ID_starts_with *graphql.ID
	ID_not_starts_with *graphql.ID
	ID_ends_with *graphql.ID
	ID_not_ends_with *graphql.ID
	{{- range .Model.StoredFields }}
	{{.Title}} *{{.GoType}}
	{{.Title}}_not *{{.GoType}}
	{{.Title}}_in *[]{{.GoType}}
	{{.Title}}_not_in *[]{{.GoType}}
	{{.Title}}_lt *{{.GoType}}
	{{.Title}}_lte *{{.GoType}}
	{{.Title}}_gt *{{.GoType}}
	{{.Title}}_gte *{{.GoType}}
	{{.Title}}_contains *{{.GoType}}
	{{.Title}}_not_contains *{{.GoType}}
	{{.Title}}_starts_with *{{.GoType}}
	{{.Title}}_not_starts_with *{{.GoType}}
	{{.Title}}_ends_with *{{.GoType}}
	{{.Title}}_not_ends_with *{{.GoType}}
	{{- end }}
}

// {{.Model.Title}}WhereUniqueInput is used to specify filter on a 
// request using a {{.Model.Title}} object. It is designed to return only one record.
type {{.Model.Title}}WhereUniqueInput struct {
	ID graphql.ID
}

// {{.Model.Title}}SubscriptionWhereInput is not implemented.
//nolint: golint
type {{.Model.Title}}SubscriptionWhereInput struct {
	And *[]{{.Model.Title}}SubscriptionWhereInput
	Or *[]{{.Model.Title}}SubscriptionWhereInput
	Not *[]{{.Model.Title}}SubscriptionWhereInput
	Mutation_in *[]string
	UpdatedFields_contains *string
	UpdatedFields_contains_every *[]string
	UpdatedFields_contains_some *[]string
	Node *{{.Model.Title}}WhereInput
}

{{if not .DisableSelect}}
/*{{.Model.Title}} resolve the main query returning a {{.Model.Title}} record,
 *specified by a unique field.
 */
func (r *Resolver{{.ServiceTitle}}) {{.Model.Title}}(ctx context.Context, args *struct {
	Where  *{{.Model.Title}}WhereUniqueInput
}) (*{{.Model.Title}}Resolver, error) {

	{{.Model.Name}}, err := GrpcClient.Get{{.Model.Title}}(ctx, string(args.Where.ID))
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't list {{.Model.Name}}")
	}

	return &{{.Model.Title}}Resolver{ {{.Model.Name}} }, nil
}

/*{{.Model.Title}}s resolve the main query searching through all {{.Model.Title}} records,
 *returning several of them.
 */
func (r *Resolver{{.ServiceTitle}}) {{.Model.Title}}s(ctx context.Context, args *struct {
	Where *{{.Model.Title}}WhereInput
	OrderBy *string
	Skip *float64
	After *string
	Before *string
	First *float64
	Last *float64
}) ([]*{{.Model.Title}}Resolver, error) {


	{{.Model.Name}}s, err := GrpcClient.List{{.Model.Title}}(ctx, []*libdata.Filter{})
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't list {{.Model.Name}}")
	}

	var resolvers []*{{.Model.Title}}Resolver
	for _, {{.Model.Name}} := range {{.Model.Name}}s {
		resolvers = append(resolvers, &{{.Model.Title}}Resolver{ {{.Model.Name}} })
	}

	return resolvers, nil
}

/*{{.Model.Title}}sConnection resolve main query returning the metadata of {{.Model.Title}} records.
 *It use the same arguments than {{.Model.Title}}s.
 */
func (r *Resolver{{.ServiceTitle}}) {{.Model.Title}}sConnection(ctx context.Context, args *struct {
	Where *{{.Model.Title}}WhereInput
	OrderBy *string
	Skip *float64
	After *string
	Before *string
	First *float64
	Last *float64
}) (*{{.Model.Title}}ConnectionResolver, error) {


	{{.Model.Name}}s, err := r.{{.Model.Title}}s(ctx, args)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't list {{.Model.Name}}")
	}

	return &{{.Model.Title}}ConnectionResolver{
		aggregate: &AggregateResolver{count: int32(len({{.Model.Name}}s))}}, nil
}
{{end}}

{{if not .DisableCreate}}
/*Create{{.Model.Title}} resolve the create mutation of {{.Model.Title}} object.
 *It takes the fields as argument, and return the new {{.Model.Name}} created.
 */
func (r *Resolver{{.ServiceTitle}}) Create{{.Model.Title}}(ctx context.Context, args *struct {
	Data {{.Model.Title}}CreateInput
}) (rr *{{.Model.Title}}Resolver, err error) {

	{{- range .Model.Fields }}
	{{prepareNullableID . false}}
	{{- end}}

	{{.Model.Name}}, err := GrpcClient.Create{{.Model.Title}}(
		ctx, 
		&orm.{{.Model.Title}}{
			{{.Model.Title}}: &mpb.{{.Model.Title}}{
				{{- range .Model.StoredFields }}
				{{.Title}}: {{convertToModel .}},
				{{- end }}
			},
		},
		{{- range .Model.NestedFields }}
		{{.Name}},
		{{- end }}
		)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't create {{.Model.Name}}")
	}

	return &{{.Model.Title}}Resolver{t: {{.Model.Name}} }, nil
}
{{end}}

{{if not .DisableUpdate}}
/*Update{{.Model.Title}} resolve the update mutation of {{.Model.Title}} object.
 *It takes the fields and the unique filter as argument, 
 *and return the {{.Model.Name}} record after update.
 */
func (r *Resolver{{.ServiceTitle}}) Update{{.Model.Title}}(ctx context.Context, args *struct {
	Data {{.Model.Title}}UpdateInput
	Where {{.Model.Title}}WhereUniqueInput
}) (rr *{{.Model.Title}}Resolver, err error) {

	var updateMask []string
	{{- range .Model.Fields }}
	{{prepareNullableID . true}}
	{{- end}}

	{{.Model.Name}}, err := GrpcClient.Update{{.Model.Title}}(
		ctx, 
		string(args.Where.ID),
		&orm.{{.Model.Title}}{
			{{.Model.Title}}: &mpb.{{.Model.Title}}{
				{{- range .Model.StoredFields }}
				{{ if eq .Type "Many2oneType" }}
				{{.Title}}: &mpb.{{.GetReference.Title}}{UUID: {{.Name}} },
				{{else}}
				{{.Title}}: {{.Name}},
				{{end}}
				{{- end }}
			},
		},
		&fieldmask.FieldMask{
			FieldMask: &types.FieldMask{
				Paths: updateMask,
			},
		},
		{{- range .Model.NestedFields }}
		{{.Name}},
		{{- end }}
		)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't create {{.Model.Name}}")
	}

	return &{{.Model.Title}}Resolver{t: {{.Model.Name}} }, nil

}
{{end}}

{{if not .DisableDelete}}
/*Delete{{.Model.Title}} resolve the delete mutation of {{.Model.Title}} object. It takes the
 *unique filter as argument, and return the {{.Model.Name}} record as it was before deletion.
 */
func (r *Resolver{{.ServiceTitle}}) Delete{{.Model.Title}}(ctx context.Context, args *struct {
	Where  *{{.Model.Title}}WhereUniqueInput
}) (rr *{{.Model.Title}}Resolver, err error) {

	{{.Model.Name}}, err := GrpcClient.Delete{{.Model.Title}}(ctx, string(args.Where.ID))
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't delete {{.Model.Name}}")
	}

	return &{{.Model.Title}}Resolver{t: {{.Model.Name}}}, nil
}
{{end}}

/*Upsert{{.Model.Title}} is not implemented.
 */
func (r *Resolver{{.ServiceTitle}}) Upsert{{.Model.Title}}(ctx context.Context, args *struct {
	Where {{.Model.Title}}WhereUniqueInput
	Create {{.Model.Title}}UpdateInput
	Update {{.Model.Title}}UpdateInput
}) (rr *{{.Model.Title}}Resolver, err error) {
	return nil, nil
}

/*UpdateMany{{.Model.Title}}s is not implemented.
 */
func (r *Resolver{{.ServiceTitle}}) UpdateMany{{.Model.Title}}s(ctx context.Context, args *struct {
	Data {{.Model.Title}}UpdateInput
	Where *{{.Model.Title}}WhereInput
}) (*BatchPayload, error) {
	return &BatchPayload{}, nil
}

/*DeleteMany{{.Model.Title}}s is not implemented.
 */
func (r *Resolver{{.ServiceTitle}}) DeleteMany{{.Model.Title}}s(ctx context.Context, args *struct {
	Where  *{{.Model.Title}}WhereInput
}) (*BatchPayload, error) {

	_, err := GrpcClient.Delete{{.Model.Title}}(ctx, string(*args.Where.ID))
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't delete {{.Model.Name}}")
	}

	return &BatchPayload{}, nil
}

/*{{.Model.Title}}Subscription is not implemented.
 */
func (r *Resolver{{.ServiceTitle}}) {{.Model.Title}}Subscription(ctx context.Context, args *struct {
	Where  {{.Model.Title}}WhereInput
}) (rr *{{.Model.Title}}Resolver, err error) {

	return &{{.Model.Title}}Resolver{}, nil
}

{{- range .CustomFuncs }}
/*{{.Title}} resolve the {{.Title}} query, 
 *which is a custom function of the {{.Model.Title}} object.
 */
func (r *Resolver{{.ServiceTitle}}) {{.Title}}(ctx context.Context, args *struct {
	{{- range .Args }}
	{{.Title}}  {{if not .Required}}*{{end}}{{.GoType}}
	{{- end}}
}) (bool, error) {

	err := GrpcClient.{{.Title}}(
		ctx,
		{{- range .Args }}
		args.{{.Title}},
		{{- end}})
	if err != nil {
		return false, errors.Wrap(err, "Couldn't execute")
	}

	return true, nil
}
{{- end}}

`))

var initTemplate = template.Must(template.New("").Parse(`package graphql

import (
	"github.com/pkg/errors"
	"golang.org/x/net/context"

	"{{.Repository}}/gen/grpc/client"
)

var GrpcClient *client.Client

/*Resolver is the main resolver object call by the graphql engine.
 *You have to call it from the main file of the application.
 */
type Resolver{{.Title}} struct{}

// // InitResolver todo
// type InitResolver struct {
// }

// Init{{.Title}}SingleTenant is a mutation you can call to initiate the database of the 
// targeted service.
func (r *Resolver{{.Title}}) Init{{.Title}}SingleTenant(ctx context.Context) (bool, error) {

	err := GrpcClient.InitSingleTenant(ctx)
	if err != nil {
		return false, errors.Wrap(err, "Couldn't initiate tenant")
	}

	return true, nil
}

/*AggregateResolver is a resolver object for metadata, returning aggregation related information.
 */
type AggregateResolver struct {
	count int32
}

// Count return the number of records in the query.
func (tr *AggregateResolver) Count() int32 {
	return tr.count
}

/*PageInfoResolver is a resolver object for metadata, returning page related information.
 */
type PageInfoResolver struct {
	hasNextPage bool
	hasPreviousPage bool
	startCursor *string
	endCursor *string
}

// HasNextPage is not implemented.
func (tr *PageInfoResolver) HasNextPage() bool {
	return tr.hasNextPage
}

// HasPreviousPage is not implemented.
func (tr *PageInfoResolver) HasPreviousPage() bool {
	return tr.hasPreviousPage
}

// StartCursor is not implemented.
func (tr *PageInfoResolver) StartCursor() *string {
	return tr.startCursor
}

// EndCursor is not implemented.
func (tr *PageInfoResolver) EndCursor() *string {
	return tr.endCursor
}

/*BatchPayload is a struct which is return in some batch related mutations.
 */
type BatchPayload struct {
	count int32
}

// Count return the number of record used in the mutation.
func (tr *BatchPayload) Count() int32 {
	return tr.count
}


`))
