package libgraphql

import (
	"bytes"
	"fmt"
	"strings"
	"text/template"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libdata/fields"
	"gitlab.com/empowerlab/stack/lib-go/liborm"
)

const idType = "ID"
const stringType = "String"
const intType = "Int"
const floatType = "Float"
const booleanType = "Boolean"

// SchemaData is the main struct to use to declare the GraphQL Schema
type SchemaData struct {
	Name        string
	Title       string
	Definitions *Definitions
	Data        []struct {
		Def         *Definition
		Model       *libdata.ModelDefinition
		CustomFuncs []*liborm.CustomFunc
	}
}

// GenSchema will generate the GraphqlQL schema from a list of SchemaData
func GenSchema(data []*SchemaData) string {

	for _, d := range data {

		d.Title = strings.Title(d.Name)

		data2 := []struct {
			Def         *Definition
			Model       *libdata.ModelDefinition
			CustomFuncs []*liborm.CustomFunc
		}{}
		for _, definition := range d.Definitions.Slice() {

			data3 := struct {
				Def         *Definition
				Model       *libdata.ModelDefinition
				CustomFuncs []*liborm.CustomFunc
			}{
				Def:         definition,
				Model:       definition.Grpc.ORM.Model,
				CustomFuncs: definition.CustomFuncs,
			}

			// for _, field := range data3.Model.Fields {
			// 	fmt.Println(field.Type())
			// 	switch field.Type() {
			// 	case fields.IDFieldType:
			// 		fmt.Println("test id")
			// 		field.Type = idType
			// 	case fields.TextFieldType:
			// 		field.Type = stringType
			// 	case fields.IntegerFieldType:
			// 		field.Type = intType
			// 	case fields.FloatFieldType:
			// 		field.Type = floatType
			// 	case fields.BooleanFieldType:
			// 		field.Type = booleanType
			// 	case fields.Many2oneFieldType:
			// 		field.Type = idType
			// 	case fields.DatetimeFieldType:
			// 		field.Type = stringType
			// 	case fields.One2manyFieldType:
			// 		field.Type = field.Reference.Title
			// 	}

			// }

			// for _, c := range data3.CustomFuncs {
			// 	for _, a := range c.Args {
			// 		switch a.Arg.Type {
			// 		case fields.IDFieldType:
			// 			a.Type = idType
			// 		case fields.TextFieldType:
			// 			a.Type = stringType
			// 		case fields.IntegerFieldType:
			// 			a.Type = intType
			// 		case fields.FloatFieldType:
			// 			a.Type = floatType
			// 		case fields.BooleanFieldType:
			// 			a.Type = booleanType
			// 		case fields.Many2oneFieldType:
			// 			a.Type = idType
			// 		case fields.DatetimeFieldType:
			// 			a.Type = stringType
			// 		}
			// 	}
			// 	for _, r := range c.Results {
			// 		switch r.Arg.Type {
			// 		case fields.IDFieldType:
			// 			r.Type = idType
			// 		case fields.TextFieldType:
			// 			r.Type = stringType
			// 		case fields.IntegerFieldType:
			// 			r.Type = intType
			// 		case fields.FloatFieldType:
			// 			r.Type = floatType
			// 		case fields.BooleanFieldType:
			// 			r.Type = booleanType
			// 		case fields.Many2oneFieldType:
			// 			r.Type = idType
			// 		case fields.DatetimeFieldType:
			// 			r.Type = stringType
			// 		}
			// 	}

			// }

			data2 = append(data2, data3)
		}
		d.Data = data2

	}

	buf := &bytes.Buffer{}
	err := schemaTemplate.Execute(buf, data)
	if err != nil {
		fmt.Println(err)
	}

	// fmt.Println(buf.String())

	return buf.String()

}

var schemaTemplate = template.Must(template.New("").Funcs(template.FuncMap{
	"getType": func(field libdata.Field) string {

		fieldType := field.GraphqlSchemaType()
		if field.Type() == fields.Many2oneFieldType {
			fieldType = field.GetReference().Title()
		}
		if field.IsNested() {
			fieldType = "[" + field.GraphqlType() + "!]"
		}

		content := `{{.Type}}{{if or .Required .Nested}}!{{end}}`

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Type     string
			Required bool
			Nested   bool
			// Result         string
			// ResultRequired string
		}{
			Type:     fieldType,
			Required: field.GetRequired(),
			Nested:   field.IsNested(),
			// Result:         string(customFunc.Result.Type),
			// ResultRequired: resultRequired,
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"buildFunc": func(customFunc *liborm.CustomFunc) string {

		var args []string
		for _, arg := range customFunc.Args {
			required := ""
			if arg.GetRequired() {
				required = "!"
			}
			args = append(args, fmt.Sprintf("%s: %s%s", arg.GetName(), arg.Type(), required))
		}

		// resultRequired := ""
		// if customFunc.Result.Required {
		// 	resultRequired = "!"
		// }

		content := `{{.Name}}({{.Args}}): Boolean!`

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Name string
			Args string
			// Result         string
			// ResultRequired string
		}{
			Name: customFunc.Name,
			Args: strings.Join(args, ","),
			// Result:         string(customFunc.Result.Type),
			// ResultRequired: resultRequired,
		})
		fmt.Println("test- ", buf.String(), err)
		return buf.String()
	},
}).Parse(
	//nolint: lll
	`
	schema {
		query: Query
		mutation: Mutation
	}
	# The query type, represents all of the entry points into our object graph
	type Query {
		{{- range . }}
		{{- range .Data }}
		{{if not .Def.DisableSelect}}
		{{.Model.Name}}(where: {{.Model.Title}}WhereUniqueInput!): {{.Model.Title}}
		{{.Model.Name}}s(
			where: {{.Model.Title}}WhereInput, orderBy: {{.Model.Title}}OrderByInput, 
			skip: Int, after: String, before: String, first: Int, last: Int): [{{.Model.Title}}]!
		{{.Model.Name}}sConnection(
			where: {{.Model.Title}}WhereInput, orderBy: {{.Model.Title}}OrderByInput,
			skip: Int, after: String, before: String, first: Int, last: Int): {{.Model.Title}}Connection!
		{{end}}
		{{- end }}
		{{- end }}
	}
	# The mutation type, represents all updates we can make to our data
	type Mutation {
		{{- range . }}
		init{{.Title}}SingleTenant(): Boolean!
		{{- range .Data }}
		{{if not .Def.DisableCreate}}
		create{{.Model.Title}}(
			data : {{.Model.Title}}CreateInput!
		): {{.Model.Title}}!
		{{end}}
		{{if not .Def.DisableUpdate}}
		update{{.Model.Title}}(
			data: {{.Model.Title}}UpdateInput!
			where: {{.Model.Title}}WhereUniqueInput!
		): {{.Model.Title}}
		{{end}}
		{{if not .Def.DisableDelete}}
		delete{{.Model.Title}}(where: {{.Model.Title}}WhereUniqueInput!): {{.Model.Title}}
		{{end}}
		{{if not .Def.DisableUpdate}}
		# upsert{{.Model.Title}}(
		#	where: {{.Model.Title}}WhereUniqueInput!
		#	create: {{.Model.Title}}CreateInput!
		#	update: {{.Model.Title}}UpdateInput!
		#): {{.Model.Title}}!
		#updateMany{{.Model.Title}}s(
		#	data: {{.Model.Title}}UpdateManyMutationInput!
		#	where: {{.Model.Title}}WhereInput
		# ): BatchPayload!
		{{end}}
		{{if not .Def.DisableDelete}}
		#deleteMany{{.Model.Title}}s(where: {{.Model.Title}}WhereInput): BatchPayload!
		{{end}}
		{{- range .CustomFuncs }}
		{{buildFunc .}}
		{{- end }}
		{{- end }}
		{{- end }}
	}
	type Subscription {
		{{- range . }}
		{{- range .Data }}
		{{.Model.Name}}Subscription(where: {{.Model.Title}}SubscriptionWhereInput!): {{.Model.Title}}SubscriptionPayload
		{{- end }}
		{{- end }}
	}

	type BatchPayload {
		count: Int!
	}
	  
	scalar Long

	enum MutationType {
		CREATED
		UPDATED
		DELETED
	}

	type PageInfo {
		hasNextPage: Boolean!
		hasPreviousPage: Boolean!
		startCursor: String
		endCursor: String
	}

	type Aggregate {
		count: Int!
	}
	
	{{- range . }}
	{{- range .Data }}
	{{if not .Def.DisableSelect}}
	type {{.Model.Title}} {
		id: ID!
		{{- range .Model.Fields }}
		{{.NameWithoutUUID}}: {{ getType . }}
		{{- end }}
	}
	type {{.Model.Title}}Connection {
		pageInfo: PageInfo
		edges: [{{.Model.Title}}Edge]
		aggregate: Aggregate
	}
	type {{.Model.Title}}Edge {
		node: {{.Model.Title}}!
		cursor: String!
	}
	enum {{.Model.Title}}OrderByInput {
		id_ASC
		id_DESC
		{{- range .Model.Fields }}
		{{.Name}}_ASC
		{{.Name}}_DESC
		{{- end }}
		updatedAt_ASC
		updatedAt_DESC
		createdAt_ASC
		createdAt_DESC
	}
	type {{.Model.Title}}SubscriptionPayload {
		mutation: MutationType!
		node: {{.Model.Title}}
		updatedFields: [String!]
		previousValues: {{.Model.Title}}
	}
	input {{.Model.Title}}SubscriptionWhereInput {
		AND: [{{.Model.Title}}SubscriptionWhereInput!]
		OR: [{{.Model.Title}}SubscriptionWhereInput!]
		NOT: [{{.Model.Title}}SubscriptionWhereInput!]
		mutation_in: [MutationType!]
		updatedFields_contains: String
		updatedFields_contains_every: [String!]
		updatedFields_contains_some: [String!]
		node: {{.Model.Title}}WhereInput
	}

	input {{.Model.Title}}WhereInput {
		AND: [{{.Model.Title}}WhereInput!]
		OR: [{{.Model.Title}}WhereInput!]
		NOT: [{{.Model.Title}}WhereInput!]
		id: ID
		id_not: ID
		id_in: [ID!]
		id_not_in: [ID!]
		id_lt: ID
		id_lte: ID
		id_gt: ID
		id_gte: ID
		id_contains: ID
		id_not_contains: ID
		id_starts_with: ID
		id_not_starts_with: ID
		id_ends_with: ID
		id_not_ends_with: ID
		{{- range .Model.StoredFields }}
		{{.Name}}: {{.GraphqlSchemaType}}
		{{.Name}}_not: {{.GraphqlSchemaType}}
		{{.Name}}_in: [{{.GraphqlSchemaType}}!]
		{{.Name}}_not_in: [{{.GraphqlSchemaType}}!]
		{{.Name}}_lt: {{.GraphqlSchemaType}}
		{{.Name}}_lte: {{.GraphqlSchemaType}}
		{{.Name}}_gt: {{.GraphqlSchemaType}}
		{{.Name}}_gte: {{.GraphqlSchemaType}}
		{{.Name}}_contains: {{.GraphqlSchemaType}}
		{{.Name}}_not_contains: {{.GraphqlSchemaType}}
		{{.Name}}_starts_with: {{.GraphqlSchemaType}}
		{{.Name}}_not_starts_with: {{.GraphqlSchemaType}}
		{{.Name}}_ends_with: {{.GraphqlSchemaType}}
		{{.Name}}_not_ends_with: {{.GraphqlSchemaType}}
		{{- end }}
	}
	input {{.Model.Title}}WhereUniqueInput {
		id: ID!
	}
	{{end}}
	{{if not .Def.DisableCreate}}
	input {{.Model.Title}}CreateInput {
		{{- range .Model.Fields }}
		{{.Name}}: {{.GraphqlSchemaType}}{{if .IsNested}}CreateManyInput{{end}}{{if .GetRequired}}!{{end}}
		{{- end }}
	}
	input {{.Model.Title}}CreateManyInput {
		create: [{{.Model.Title}}CreateInput!]
		connect: [{{.Model.Title}}WhereUniqueInput!]
	}
	{{end}}
	{{if not .Def.DisableUpdate}}
	input {{.Model.Title}}UpdateInput {
		{{- range .Model.Fields }}
		{{.Name}}: {{.GraphqlSchemaType}}{{if .IsNested}}UpdateManyInput{{end}}
		{{- end }}
	}
	input {{.Model.Title}}UpdateManyInput {
		create: [{{.Model.Title}}CreateInput!]
		connect: [{{.Model.Title}}WhereUniqueInput!]
		set: [{{.Model.Title}}WhereUniqueInput!]
		disconnect: [{{.Model.Title}}WhereUniqueInput!]
		delete: [{{.Model.Title}}WhereUniqueInput!]
		update: [{{.Model.Title}}UpdateWithWhereUniqueNestedInput!]
		updateMany: [{{.Model.Title}}UpdateManyWithWhereNestedInput!]
		deleteMany: [{{.Model.Title}}WhereInput!]
		upsert: [{{.Model.Title}}UpsertWithWhereUniqueNestedInput!]
	}
	input {{.Model.Title}}UpdateManyMutationInput {
		{{- range .Model.Fields }}
		{{.Name}}: {{.GraphqlSchemaType}}{{if .IsNested}}UpdateManyInput{{end}}
		{{- end }}
	}
	input {{.Model.Title}}UpdateManyWithWhereNestedInput {
		where: {{.Model.Title}}WhereInput!
		data: {{.Model.Title}}UpdateManyInput!
	}
	input {{.Model.Title}}UpdateWithWhereUniqueNestedInput {
		where: {{.Model.Title}}WhereUniqueInput!
		data: {{.Model.Title}}UpdateInput!
	}
	input {{.Model.Title}}UpsertWithWhereUniqueNestedInput {
		where: {{.Model.Title}}WhereUniqueInput!
		update: {{.Model.Title}}UpdateInput!
		create: {{.Model.Title}}CreateInput!
	}
	{{end}}
	
	{{- end }}
	{{- end }}
	
	input OrderByInput {
		field: String!
		desc: Boolean!
	}

	type Connection {
		aggregate: Aggregate
	}

	type Aggregate {
		count: Int!
	}
`))
