package libexec

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"regexp"
	"time"

	"github.com/pkg/errors"
	//nolint: goimports
	"github.com/satori/go.uuid"
)

// Provider todo
type Provider struct {
	Host     string
	Login    string
	Password string
}

// ExecuteDeployer todo
//nolint:unparam
func ExecuteDeployer(deployerID string, provider *Provider, args map[string]interface{}) error {

	args["host"] = provider.Host
	args["user"] = provider.Login
	args["password"] = provider.Password

	res, _ := json.Marshal(args)
	id, _ := uuid.NewV4()
	// if err != nil {
	// 	return errors.Wrap(err, "Couldn't create uuid")
	// }

	if os.Getenv("OPENWHISK") == "True" {
		// msg2 := map[string]interface{}{"host": consoleHostname+":8443", "user":
		// "admin", "password": "admin",
		// 	"project": "empower", "action": "rawDeploy", "rawContent": string(template)}
		// res, _ = json.Marshal(msg2)
		// fmt.Println(string(res))
		// executeLocal("serverless", "invoke", "--function", "inf_openshift",
		// "--data", fmt.Sprintf("'%s'", string(res)))
		// executeLocal("wsk", "activation", "logs", "-i", "--last")
		// id, err = uuid.NewV4()
		// if err != nil {
		// 	fmt.Printf("Something went wrong: %s", err)
		// }
		err := ioutil.WriteFile("/tmp/"+id.String(), res, 0644)
		if err != nil {
			return err
		}

		output, err := ExecuteLocal(
			"wsk", "action", "invoke", "/whisk.system/inf_openshift-dev-inf_openshift",
			"-i", "-b",
			"-P", "/tmp/"+id.String())
		if err != nil {
			return err
		}
		_, err = ExecuteLocal("rm", "/tmp/"+id.String())
		if err != nil {
			return err
		}
		re := regexp.MustCompile(`(^ok.*)`)
		output = re.ReplaceAllString(output, "")
		outputJSON := map[string]interface{}{}
		fmt.Println(output)
		err = json.Unmarshal([]byte(output), &outputJSON)
		if err != nil {
			fmt.Println(err)
		}

		// executeLocal("serverless", "invoke", "--function", "inf_openshift",
		// "--data", fmt.Sprintf("'%s'", string(res)))
		fmt.Println(outputJSON)
		fmt.Println(outputJSON["activationId"].(string))
		i := 0
		err = nil
		for i < 50 && (err != nil || i == 0) {
			_, err = ExecuteLocal("wsk", "activation", "logs", "-i", outputJSON["activationId"].(string))
			if err != nil {
				time.Sleep(1 * time.Second)
			}
			i++
		}

	} else {

		_, err := ExecuteLocal("oc", "login", args["host"].(string),
			"--username", args["user"].(string),
			"--password", args["password"].(string),
			"--insecure-skip-tls-verify")
		if err != nil {
			return errors.Wrap(err, "Couldn't login")
		}
		_, err = ExecuteLocal("oc", "project", "command")
		if err != nil {
			return err
		}

		// args := []string{}
		// args = append(args, "run")
		// args = append(args, "deployer-" + id.String())
		// args = append(args, "--image=172.30.1.1:5000/shared/deployer")
		// // args = append(args, "--image=docker-registry.default.svc:5000/empower/deployer")
		// args = append(args, "--attach")
		// args = append(args, "--restart=Never")
		// args = append(args, "/opt/run")
		// args = append(args, string(res))
		// _, err := executeLocal("oc", args...)
		// executeLocal("oc", "project", "shared")
		_, err = ExecuteLocal("sh", "-c", fmt.Sprintf(
			"oc process %s POD_NAME='%s' ARGS='%s' | oc create -f -",
			"shared//deployer-template", "deployer-"+id.String(), string(res)))
		if err != nil {
			return errors.Wrap(err, "Couldn't execute deployer")
		}

		i := 0
		err = nil
		for i < 50 && (err != nil || i == 0) {
			_, err = ExecuteLocal("oc", "logs", "-f", "deployer-"+id.String())
			if err != nil {
				fmt.Println(fmt.Sprintf("%v/50", i))
				time.Sleep(2 * time.Second)
			}
			i++
		}

		_, err = ExecuteLocal("oc", "delete", "pod", "deployer-"+id.String())
		if err != nil {
			return errors.Wrap(err, "Couldn't delete pod")
		}
		// executeLocal("oc", "project", args["project"].(string))
	}
	return nil
}
