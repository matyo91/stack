package libexec

import (
	"bufio"
	"bytes"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"sync"
)

// ExecuteLocal todo
func ExecuteLocal(name string, args ...string) (string, error) {
	fmt.Printf("\n%v %v\n", name, strings.Join(args, " "))

	// old := os.Stdout // keep backup of the real stdout
	r, w, err := os.Pipe()
	if err != nil {
		fmt.Println(err)
	}
	// os.Stdout = w

	// print()

	// outC := make(chan string)
	// copy the output in a separate goroutine so printing can't block indefinitely
	// go func() {
	//     var buf bytes.Buffer
	// 	io.Copy(&buf, r)
	// 	fmt.Println(buf.String())
	//     outC <- buf.String()
	// }()
	var wg sync.WaitGroup
	var out string
	wg.Add(1)
	go func() {
		defer wg.Done()
		scanner := bufio.NewScanner(r)
		for scanner.Scan() {
			line := scanner.Text()

			fmt.Println(line)
			// Log the stdout line to my event logger
			if out != "" {
				out = out + "\n" + line
			} else {
				out = line
			}
			// event.Log(event.Event{Id: id, Msg: line})
		}
	}()

	// nolint: gas
	cmd := exec.Command(name, args...)
	// var out bytes.Buffer
	cmd.Stdout = w
	var stdErr bytes.Buffer
	cmd.Stderr = &stdErr
	err = cmd.Run()
	if err != nil {
		fmt.Printf("%v\n", stdErr.String())
		fmt.Printf("%v\n", err)
		// log.Fatal(err)
		return "", err
	}
	// output, err := cmd.Output()
	// if err != nil {
	// 	fmt.Printf("%v\n", err)
	// }

	// back to normal state
	err = w.Close()
	if err != nil {
		fmt.Printf("%v\n", err)
	}
	// os.Stdout = old // restoring the real stdout
	// out := <-outC
	wg.Wait()

	return out, nil
	// fmt.Printf("%v\n", out.String())
}
