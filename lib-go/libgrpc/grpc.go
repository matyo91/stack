package libgrpc

import (
	// "context"
	"fmt"
	"net"

	"github.com/grpc-ecosystem/grpc-opentracing/go/otgrpc"
	opentracing "github.com/opentracing/opentracing-go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/health"
	healthpb "google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc/reflection"
	// "gitlab.com/empowerlab/backup/models"
	// "gitlab.com/empowerlab/backup/grpc/pb"
)

// Server need to be call from the main file of the service, to launch the grpc server.
type Server struct {
	// storage       Storage
}

// GrpcServer need to be call from the main file of the service, to launch the grpc server.
var GrpcServer *grpc.Server

func NewServer(tracer opentracing.Tracer) {
	GrpcServer = grpc.NewServer(
		grpc.UnaryInterceptor(
			otgrpc.OpenTracingServerInterceptor(tracer)),
		grpc.StreamInterceptor(
			otgrpc.OpenTracingStreamServerInterceptor(tracer)))

	healthServer := health.NewServer()
	healthpb.RegisterHealthServer(GrpcServer, healthServer)
}

// Listen start the grpc server on the specified port.
func (s *Server) Listen(port int) error {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		return err
	}
	reflection.Register(GrpcServer)
	return GrpcServer.Serve(lis)
}
