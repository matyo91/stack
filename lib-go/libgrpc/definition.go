package libgrpc

import (
	"fmt"
	"strings"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/liborm"
)

// Definitions contains all the grpc definitions in the service.
type Definitions struct {
	Prefix     string
	Repository string
	slice      []*Definition
	byIds      map[string]*Definition
}

// Register is used to register a new definition into the service.
func (ds *Definitions) Register(d *Definition) {

	if d.customFuncsByName == nil {
		d.customFuncsByName = map[string]*liborm.CustomFunc{}
	}
	for _, c := range d.CustomFuncs {
		d.customFuncsByName[c.Name] = c
	}

	if ds.byIds == nil {
		ds.byIds = map[string]*Definition{}
	}

	ds.slice = append(ds.slice, d)
	ds.byIds[d.ORM.Model.Name] = d

}

// Slice return the definitions as a slice.
func (ds *Definitions) Slice() []*Definition {
	return ds.slice
}

// GetByID return the specified definition by its ID.
func (ds *Definitions) GetByID(id string) *Definition {
	d := ds.byIds[id]
	if d == nil {
		panic(fmt.Sprintf("The grpc definition %s doesn't exist", id))
	}
	return d
}

// Definition is used to declare the information of a model, so it can generate its code.
type Definition struct {
	ORM               *liborm.Definition
	CustomFuncs       []*liborm.CustomFunc
	customFuncsByName map[string]*liborm.CustomFunc
}

// GetCustomFuncByName return the custom function by it's name
func (d *Definition) GetCustomFuncByName(name string) *liborm.CustomFunc {
	c := d.customFuncsByName[name]
	if c == nil {
		panic(fmt.Sprintf("The custom func %s doesn't exist", name))
	}
	return c
}

// GetCustomFuncsData return the custom function information for collections and pools,
// in a format usable by templates.
// func (d *Definition) GetCustomFuncsData() []*liborm.CustomFuncData {
// 	var result []*liborm.CustomFuncData
// 	for _, c := range d.CustomFuncs {
// 		result = append(result, c.GetCustomFuncData())
// 	}
// 	return result
// }

// Arg represent the argument of a custom function.
type Arg struct {
	Name     string
	Type     libdata.FieldType
	Required bool
}

// CustomFunc represent a custom function.
type CustomFunc struct {
	Name   string
	Args   []*Arg
	Result *Arg
}

// GetCustomFuncData return the custom function information, in a format usable by templates.
func (c *CustomFunc) GetCustomFuncData() *CustomFuncData {

	var args []*ArgData
	for _, arg := range c.Args {
		args = append(args, &ArgData{
			Name:  arg.Name,
			Title: strings.Title(arg.Name),
			// Type:     arg.Type.GoType(),
			Required: arg.Required,
			Arg:      arg,
		})
	}

	return &CustomFuncData{
		Name:  c.Name,
		Title: strings.Title(c.Name),
		Args:  args,
		Result: &ArgData{
			Name:  c.Result.Name,
			Title: strings.Title(c.Result.Name),
			// Type:     c.Result.Type.GoType(),
			Required: c.Result.Required,
			Arg:      c.Result,
		},
	}
}

// ArgData represent the argument of a custom function, in a format usable by templates.
type ArgData struct {
	Name     string
	Title    string
	Type     string
	Required bool
	Arg      *Arg
}

// CustomFuncData represent a custom function, in a format usable by templates.
type CustomFuncData struct {
	Name   string
	Title  string
	Args   []*ArgData
	Result *ArgData
}
