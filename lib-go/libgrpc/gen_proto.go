package libgrpc

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"text/template"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libdata/fields"
	"gitlab.com/empowerlab/stack/lib-go/liborm"
)

// GenProtos will generate the grpc proto files.
func (defs *Definitions) GenProtos() {

	// tPostFunc := template.Must(template.New("postFunc").Parse(postFuncProtoTemplate))
	// tproto := template.Must(template.New("protot").Parse(protoTemplate))

	type definitionData struct {
		Prefix      string
		Model       *libdata.ModelDefinition
		CustomFuncs []*liborm.CustomFunc
	}

	data := struct {
		Prefix      string
		Repository  string
		Definitions []*definitionData
	}{
		Prefix:     strings.Title(defs.Prefix),
		Repository: defs.Repository,
	}
	for _, definition := range defs.Slice() {

		d := &definitionData{
			Prefix:      strings.Title(defs.Prefix),
			Model:       definition.ORM.Model,
			CustomFuncs: definition.CustomFuncs,
		}

		// d.Model = append(d.Models, definition.Model.Model.GetTemplateData())

		// for _, model := range d.Models {
		// for _, field := range d.Model.Fields {
		// 	if field.Definition.Type() == libdata.DateFieldType {
		// 		field.Type = "Datetime"
		// 	}
		// 	if field.Definition.Type() == libdata.DatetimeFieldType {
		// 		field.Type = "Datetime"
		// 	}
		// }

		data.Definitions = append(data.Definitions, d)

	}

	err := os.MkdirAll("gen/grpc/pb", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	buf := &bytes.Buffer{}
	err = protoTemplate.Execute(buf, data)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("grpc proto ", err)
	}
	err = ioutil.WriteFile(
		"gen/grpc/pb/main.proto",
		buf.Bytes(), 0644)
	if err != nil {
		fmt.Println(err)
	}

}

var protoTemplate = template.Must(template.New("").Funcs(template.FuncMap{
	"add": func(i int, j int) int {
		return i + j
	},
	"getField": func(model *libdata.ModelDefinition, field libdata.Field, i int, j int) string {

		var content string
		if field.GetRequired() {
			//nolint: lll
			content = `required {{.Field.Type}} {{.Field.Name}} = {{.Position}} [(gogoproto.nullable) = false];`
		} else {
			if model.UseOneOf {
				//nolint: lll
				content = `{{oneof has{{.Field.Title}} { {{.Field.Type}} {{.Field.Name}} = {{.Position}};}`
			} else {
				//nolint: lll
				content = `optional {{.Field.Type}} {{.Field.Name}} = {{.Position}} [(gogoproto.nullable) = true];`
			}
		}

		if field.Type() == fields.DatetimeFieldType {
			//nolint: lll
			content = `optional google.protobuf.Timestamp {{.Field.Title}} = {{.Position}} [(gogoproto.stdtime) = true];`
		}

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Field    libdata.Field
			Position int
		}{
			Field:    field,
			Position: i + j,
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
}).Parse(`syntax = "proto3";
package pb;

// import "github.com/gogo/protobuf/gogoproto/gogo.proto";
import "{{.Repository}}/gen/orm/pb/main.proto";

message InitSingleTenantRequest {}
message InitSingleTenantResponse {}	

{{- range .Definitions }}
message GRPCUpdate{{.Model.Title}}Request {
	GetRequest where = 1;
	Update{{.Model.Title}}Request data = 2;
}

{{- end }}

{{- range .Definitions }}
{{- range .CustomFuncs}}
message {{.Title}}Request {
	{{- range $i, $arg := .Args }}
	{{$arg.ProtoType}} {{$arg.Name}} = {{add $i 1}};
	{{- end }}
}
message {{.Title}}Response {
	{{- range $i, $arg := .Results }}
	{{$arg.ProtoType}} {{$arg.Name}} = {{add $i 1}};
	{{- end }}
}

{{- end }}
{{- end }}

service GRPCService {
	rpc InitSingleTenant (InitSingleTenantRequest) returns (InitSingleTenantResponse) {}

	{{- range .Definitions }}
	{{$def := .}}
	rpc Get{{.Model.Title}}s (GetRequest) returns ({{.Model.Title}}) {}
	rpc List{{.Model.Title}}s (ListRequest) returns ({{.Model.Title}}s) {}
	rpc Create{{.Model.Title}} (Create{{.Model.Title}}Request) returns ({{.Model.Title}}) {}
	rpc Update{{.Model.Title}} (GRPCUpdate{{.Model.Title}}Request) returns ({{.Model.Title}}) {}
	rpc Delete{{.Model.Title}} (GetRequest) returns ({{.Model.Title}}) {}
	{{- range .Model.Fields }}
	{{if .GetReference}}{{if not .IsNested}}
	rpc Get{{.TitleWithoutUUID}}By{{$def.Model.Title}} (GetRequest) returns ({{.GetReference.Title}}) {}
	{{else}}
	rpc Get{{.Title}}By{{$def.Model.Title}} (GetRequest) returns ({{.GetReference.Title}}s) {}
	{{end}}{{end}}
	{{- end }}
	{{- range .CustomFuncs}}
	rpc {{.Title}} ({{.Title}}Request) returns ({{.Title}}Response) {}
	{{- end }}
	{{- end }}	
}
`))
