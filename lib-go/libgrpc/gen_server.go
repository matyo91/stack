package libgrpc

import (
	"bytes"
	"fmt"
	"go/format"
	"io/ioutil"
	"os"
	"text/template"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/liborm"
)

// GenServer will generate the grpc server files.
//nolint: dupl
func (defs *Definitions) GenServer() {

	d := struct {
		Repository string
	}{
		Repository: defs.Repository,
	}

	err := os.MkdirAll("gen/grpc/server", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	buf := &bytes.Buffer{}
	err = initServerTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(err)
	}
	content, err := format.Source(buf.Bytes())
	if err != nil {
		fmt.Println(buf)
		fmt.Println("init grpc server ", err)
	}
	err = ioutil.WriteFile("gen/grpc/server/init.gen.go", content, 0644)
	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(defs.Slice())
	for _, definition := range defs.Slice() {

		d := struct {
			Repository  string
			Model       *libdata.ModelDefinition
			CustomFuncs []*liborm.CustomFunc
		}{
			Repository:  defs.Repository,
			Model:       definition.ORM.Model,
			CustomFuncs: definition.CustomFuncs,
		}

		buf := &bytes.Buffer{}
		err := serverTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(err)
		}
		// content, err := format.Source(buf.Bytes())
		// if err != nil {
		// 	fmt.Println(buf)
		// 	fmt.Println("grpc server ", err)
		// }
		err = ioutil.WriteFile(
			fmt.Sprintf("gen/grpc/server/%s.gen.go", d.Model.Snake()),
			buf.Bytes(), 0644)
		if err != nil {
			fmt.Println(err)
		}

	}

}

var serverTemplate = template.Must(template.New("").Funcs(template.FuncMap{
	"prepareModel": func(field libdata.Field) string {

		valueContent := "r.{{.Model.Title}}.Get{{.LesserTitle}}()"
		if field.Type() == timeType {
			valueContent = `time.Date(
				int(r.{{.Model.Title}}.Get{{.LesserTitle}}().Year),
				time.Month(r.{{.Model.Title}}.Get{{.LesserTitle}}().Month),
				int(r.{{.Model.Title}}.Get{{.LesserTitle}}().Day),
				int(r.{{.Model.Title}}.Get{{.LesserTitle}}().Hour),
				int(r.{{.Model.Title}}.Get{{.LesserTitle}}().Minute),
				int(r.{{.Model.Title}}.Get{{.LesserTitle}}().Second),
				int(r.{{.Model.Title}}.Get{{.LesserTitle}}().Nanosecond),
				nil)`
		}
		value := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(valueContent)).Execute(value, field)
		if err != nil {
			fmt.Println(err)
		}

		var content string
		if !field.GetRequired() {
			content = `
			var {{.Field.Name}} *{{.Field.Type}}
			if r.{{.Field.Model.Title}}.GetHas{{.Field.Title}}() != nil {
				v := {{.Value}}
				{{.Field.Name}} = &v
			}
			`
		}

		buf := &bytes.Buffer{}
		err = template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Field libdata.Field
			Value string
		}{
			Field: field,
			Value: value.String(),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"convertToModel": func(field libdata.Field) string {

		var content string
		if field.GetRequired() && field.Type() == timeType {
			content = `time.Date(
				int(r.{{.Model.Title}}.{{.LesserTitle}}.Year),
				time.Month(r.{{.Model.Title}}.{{.LesserTitle}}.Month),
				int(r.{{.Model.Title}}{{.LesserTitle}}.Day),
				int(r.{{.Model.Title}}.{{.LesserTitle}}.Hour),
				int(r.{{.Model.Title}}.{{.LesserTitle}}.Minute),
				int(r.{{.Model.Title}}.{{.LesserTitle}}.Second),
				int(r.{{.Model.Title}}.{{.LesserTitle}}.Nanosecond),
				nil),`
		} else if field.GetRequired() {
			content = `r.{{.Model.Title}}.{{.LesserTitle}}`
		} else {
			content = `{{.Name}}`
		}

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, field)
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"prepareResult": func(model *libdata.ModelDefinition, field libdata.Field) string {

		if !model.UseOneOf {
			return ""
		}

		valueContent := "*{{.Model.Name}}.{{.Title}}"
		if field.Type() == timeType {
			valueContent = `&pb.Datetime{
				Year: int32({{.Model.Name}}.{{.Title}}.Year()),
				Month: int32({{.Model.Name}}.{{.Title}}.Month()),
				Day: int32({{.Model.Name}}.{{.Title}}.Day()),
				Hour: int32({{.Model.Name}}.{{.Title}}.Hour()),
				Minute: int32({{.Model.Name}}.{{.Title}}.Minute()),
				Second: int32({{.Model.Name}}.{{.Title}}. Second()),
				Nanosecond: int32({{.Model.Name}}.{{.Title}}.Nanosecond()),
			}`
		}
		value := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(valueContent)).Execute(value, field)
		if err != nil {
			fmt.Println(err)
		}

		var content string
		if !field.GetRequired() {
			content = `
			var {{.Field.Name}}Result *pb.{{.Field.Model.Title}}_{{.Field.Title}}
			if {{.Field.Model.Name}}.{{.Field.Title}} != nil {
				{{.Field.Name}}Result = &pb.{{.Field.Model.Title}}_{{.Field.Title}}{
					{{.Field.Title}}: {{.Value}},
				}
			}
			`
		}

		buf := &bytes.Buffer{}
		err = template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Field libdata.Field
			Value string
		}{
			Field: field,
			Value: value.String(),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"convertToResult": func(field libdata.Field) string {

		var content string
		if field.GetRequired() && field.Type() == timeType {
			content = `{{.LesserTitle}}:           &pb.Datetime{
				Year: int32({{.Model.Name}}.{{.Title}}.Year()),
				Month: int32({{.Model.Name}}.{{.Title}}.Month()),
				Day: int32({{.Model.Name}}.{{.Title}}.Day()),
				Hour: int32({{.Model.Name}}.{{.Title}}.Hour()),
				Minute: int32({{.Model.Name}}.{{.Title}}.Minute()),
				Second: int32({{.Model.Name}}.{{.Title}}. Second()),
				Nanosecond: int32({{.Model.Name}}.{{.Title}}.Nanosecond()),
			},`
		} else if field.GetRequired() {
			content = `{{.LesserTitle}}:           {{.Model.Name}}.{{.Title}},`
		} else {
			content = `Has{{.LesserTitle}}: {{.Name}}Result,`
		}

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, field)
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
}).Parse(`
//nolint: dupl
package grpc

import (
	"context"
	// "os"
	"strings"
	"fmt"
	"time"

	"github.com/pkg/errors"
	"google.golang.org/grpc/metadata"
	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/liborm"

	"{{.Repository}}/gen/grpc/pb"
	mpb "{{.Repository}}/gen/orm/pb"
	"{{.Repository}}/gen/orm"
)

var dummyTime{{.Model.Title}} time.Time

/*Get{{.Model.Title}}s return the specified {{.Model.Name}}.
 */
func (s *Server) Get{{.Model.Title}}s(
	ctx context.Context,
	r *mpb.GetRequest,
) (*mpb.{{.Model.Title}}, error) {

	tx, err := liborm.Cluster.BeginTransaction(ctx, &libdata.Tenant{}, true)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't begin transaction")
	}
	defer func() {
		err = tx.CommitTransaction(ctx, err)
	}()

	fmt.Println("test")


	{{.Model.Name}}, err := orm.{{.Model.Title}}Definition.NewPool(&liborm.Env{
		Context: ctx,
		Transaction: tx,
	}).GetByUUID(r.UUID)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't get {{.Model.Name}}")
	}

	return {{.Model.Name}}.{{.Model.Title}}, nil
}

/*List{{.Model.Title}}s return a list of {{.Model.Name}}s, filtered by arguments.
 */
func (s *Server) List{{.Model.Title}}s(
	ctx context.Context,
	r *mpb.ListRequest,
) (*mpb.{{.Model.Title}}s, error) {

	md, _ := metadata.FromIncomingContext(ctx)
	fmt.Println(md["token"])
	fmt.Println(md["userUUID"])
	fmt.Println(md["groups"])

	var filters []*libdata.Filter
	for _, where := range r.Where {
		var operande interface{}
		if where.Operator == "IN" {
			operande = strings.Split(where.Operande, ",")
		} else {
			operande = where.Operande
		}
		filters = append(filters, &libdata.Filter{
			Field:    where.Field,
			Operator: where.Operator,
			Operande: operande,
		})
	}

	tx, err := liborm.Cluster.BeginTransaction(ctx, &libdata.Tenant{}, true)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't begin transaction")
	}
	defer func() {
		err = tx.CommitTransaction(ctx, err)
	}()


	{{.Model.Name}}s, err := orm.{{.Model.Title}}Definition.NewPool(&liborm.Env{
		Context: ctx,
		Transaction: tx,
	}).Select(filters, nil, nil, nil)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't create storage")
	}

	var pb{{.Model.Title}}s []*mpb.{{.Model.Title}}
	for _, {{.Model.Name}} := range {{.Model.Name}}s.Slice() {
		pb{{.Model.Title}}s = append(pb{{.Model.Title}}s, {{.Model.Name}}.{{.Model.Title}})
	}

	return &mpb.{{.Model.Title}}s{
		{{.Model.Title}}s: pb{{.Model.Title}}s,
	}, nil
}

/*Create{{.Model.Title}} create a new {{.Model.Name}} and return it.
 */
func (s *Server) Create{{.Model.Title}}(
	ctx context.Context,
	r *mpb.Create{{.Model.Title}}Request,
) (*mpb.{{.Model.Title}}, error) {

	tx, err := liborm.Cluster.BeginTransaction(ctx, &libdata.Tenant{}, false)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't begin transaction")
	}
	defer func() {
		err = tx.CommitTransaction(ctx, err)
	}()

	{{.Model.Name}}s, err := orm.{{.Model.Title}}Definition.NewPool(&liborm.Env{
		Context: ctx,
		Transaction: tx,
	}).Create([]*mpb.Create{{.Model.Title}}Request{r})
		// []*orm.{{.Model.Title}}{&orm.{{.Model.Title}}{
		// 	{{.Model.Title}}: r.{{.Model.Title}},
		// } })
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't create storage")
	}

	{{.Model.Name}} := {{.Model.Name}}s.Slice()[0]
	return {{.Model.Name}}.{{.Model.Title}}, nil
}

/*Update{{.Model.Title}} update the specified {{.Model.Name}}, and return the end result.
 */
//nolint: dupl
func (s *Server) Update{{.Model.Title}}(
	ctx context.Context,
	r *pb.GRPCUpdate{{.Model.Title}}Request,
) (*mpb.{{.Model.Title}}, error) {

	fmt.Printf("%+v %+v\n", r.Data.{{.Model.Title}}, r.Data.UpdateMask)

	tx, err := liborm.Cluster.BeginTransaction(ctx, &libdata.Tenant{}, false)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't begin transaction")
	}
	defer func() {
		err = tx.CommitTransaction(ctx, err)
	}()

	{{.Model.Name}}s, err := orm.{{.Model.Title}}Definition.NewPool(&liborm.Env{
		Context: ctx,
		Transaction: tx,
	}).Select([]*libdata.Filter{
		{Field: "uuid", Operator: "=", Operande: r.Where.UUID}}, nil, nil, nil)

	err = {{.Model.Name}}s.Update(r.Data)
		// []*orm.{{.Model.Title}}{&orm.{{.Model.Title}}{
		// 	{{.Model.Title}}: r.{{.Model.Title}},
		// } })
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't create storage")
	}

	{{.Model.Name}} := {{.Model.Name}}s.Slice()[0]
	return {{.Model.Name}}.{{.Model.Title}}, nil
}

/*Delete{{.Model.Title}} delete the specified {{.Model.Name}}, 
 *and return the record before deletion.
 */
func (s *Server) Delete{{.Model.Title}}(
	ctx context.Context,
	r *mpb.GetRequest,
) (*mpb.{{.Model.Title}}, error) {

	tx, err := liborm.Cluster.BeginTransaction(ctx, &libdata.Tenant{}, false)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't begin transaction")
	}
	defer func() {
		err = tx.CommitTransaction(ctx, err) 
	}()

	{{.Model.Name}}s, err := orm.{{.Model.Title}}Definition.NewPool(&liborm.Env{
		Context: ctx,
		Transaction: tx,
	}).Select([]*libdata.Filter{
		{Field: "uuid", Operator: "=", Operande: r.UUID}}, nil, nil, nil)

	err = {{.Model.Name}}s.Delete()
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't create {{.Model.Name}}")
	}

	{{.Model.Name}} := {{.Model.Name}}s.Slice()[0]
	return {{.Model.Name}}.{{.Model.Title}}, nil
}

{{- range .Model.Fields }}
{{if .GetReference}}
{{if not .IsNested}}
/*Get{{.TitleWithoutUUID}}By{{$.Model.Title}} return the record behind the
 *{{.TitleWithoutUUID}} field, for the specified {{$.Model.Name}}.
 */
func (s *Server) Get{{.TitleWithoutUUID}}By{{$.Model.Title}}(
	ctx context.Context,
	r *mpb.GetRequest,
) (*mpb.{{.GetReference.Title}}, error) {

	fmt.Println("grpc test")

	tx, err := liborm.Cluster.BeginTransaction(ctx, &libdata.Tenant{}, true)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't begin transaction")
	}
	defer func() {
		err = tx.CommitTransaction(ctx, err) 
	}()

	{{$.Model.Name}}, err := orm.{{$.Model.Title}}Definition.NewPool(&liborm.Env{
		Context: ctx,
		Transaction: tx,
	}).GetByUUID(r.UUID)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't get {{$.Model.Name}}")
	}

	if {{$.Model.Name}} != nil {
		{{.Name}}, err := {{$.Model.Name}}.{{.TitleWithoutUUID}}()
		if err != nil {
			return nil, errors.Wrap(err, "Couldn't get {{$.Model.Name}}")
		}

		// var pb{{.Title}} *mpb.{{.GetReference.Title}}
		// for _, {{.GetReference.Name}} := range {{.Name}}.Slice() {
		// 	pb{{.Title}} = append(pb{{.Title}}, {{.GetReference.Name}}.{{.GetReference.Title}})
		// }

		return {{.Name}}.{{.GetReference.Title}}, nil
	}

	return &mpb.{{.GetReference.Title}}{}, nil

}
{{else}}
/*Get{{.Title}}By{{$.Model.Title}} return the records behind the {{.Title}} field,
 *for the specified {{$.Model.Name}}.
 */
func (s *Server) Get{{.Title}}By{{$.Model.Title}}(
	ctx context.Context,
	r *mpb.GetRequest,
) (*mpb.{{.GetReference.Title}}s, error) {

	fmt.Println("grpc test")

	tx, err := liborm.Cluster.BeginTransaction(ctx, &libdata.Tenant{}, true)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't begin transaction")
	}
	defer func() {
		err = tx.CommitTransaction(ctx, err) 
	}()

	{{$.Model.Name}}, err := orm.{{$.Model.Title}}Definition.NewPool(&liborm.Env{
		Context: ctx,
		Transaction: tx,
	}).GetByUUID(r.UUID)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't get {{$.Model.Name}}")
	}

	{{.Name}}, err := {{$.Model.Name}}.{{.Title}}()
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't get {{$.Model.Name}}")
	}

	var pb{{.Title}} []*mpb.{{.GetReference.Title}}
	for _, {{.GetReference.Name}} := range {{.Name}}.Slice() {
		pb{{.Title}} = append(pb{{.Title}}, {{.GetReference.Name}}.{{.GetReference.Title}})
	}

	return &mpb.{{.GetReference.Title}}s{
		{{.GetReference.Title}}s: pb{{.Title}},
	}, nil

}
{{end}}
{{end}}
{{- end }}

{{- range .CustomFuncs}}

var {{.Title}} func(
	context.Context, *libdata.Transaction, *pb.{{.Title}}Request,
) (*pb.{{.Title}}Response, error)

/*{{.Title}} resolve the corresponding custom function, linked to {{$.Model.Name}} object.
 */
func (s *Server) {{.Title}}(
	ctx context.Context,
	r *pb.{{.Title}}Request,
) (*pb.{{.Title}}Response, error) {

	fmt.Printf("grpc test %+v\n", r)

	// tx, err := liborm.Cluster.BeginTransaction(ctx, &libdata.Tenant{}, true)
	// if err != nil {
	// 	return nil, errors.Wrap(err, "Couldn't begin transaction")
	// }
	// defer func() {
	// 	err = tx.CommitTransaction(ctx, err) 
	// }()

	tx := &libdata.Transaction{}

	// orm.DataDefinition.LoadData(&liborm.Env{
	// 	Context: ctx,
	// 	Transaction: tx,
	// }, []byte("test"))

	if {{.Title}} == nil {
		return nil, errors.New("You have to implement the function {{.Title}}")
	}

	return {{.Title}}(ctx, tx, r)
}
{{- end }}
`))

var initServerTemplate = template.Must(template.New("").Parse(`package grpc

import (
	"context"

	"github.com/pkg/errors"
	"gitlab.com/empowerlab/stack/lib-go/libgrpc"
	"gitlab.com/empowerlab/stack/lib-go/liborm"
	opentracing "github.com/opentracing/opentracing-go"

	"{{.Repository}}/gen/grpc/pb"
	// "{{.Repository}}/gen/orm"
)

/*Server is the main struct resolving the grpc server function. You have to create it from the
 *main file of the application.
 */
type Server struct{}

func NewServer(tracer opentracing.Tracer) {
	libgrpc.NewServer(tracer)
	server := &Server{}
	pb.RegisterGRPCServiceServer(libgrpc.GrpcServer, server)
}

/*InitSingleTenant initialize the database.
 */
func (s *Server) InitSingleTenant(
	ctx context.Context,
	r *pb.InitSingleTenantRequest,
) (*pb.InitSingleTenantResponse, error) {

	err := liborm.Cluster.InitSingleTenant(ctx)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't initialize single tenant")
	}

	return &pb.InitSingleTenantResponse{}, nil
}
`))
