package libgrpc

import (
	"bytes"
	"fmt"
	"go/format"
	"io/ioutil"
	"os"
	"text/template"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/liborm"
)

// type data struct {
// 	Name string
// 	NameInitCap string
// 	FieldArgs string
// 	FieldsMap string
// 	FieldsStruct string
// 	PostFunc string
// }

const timeType = "time.time"

// GenClient will generate the grpc client files.
//nolint: dupl
func (defs *Definitions) GenClient() {

	err := os.MkdirAll("gen/grpc/client", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	var definitionsData []*libdata.ModelDefinition
	fmt.Println(defs.Slice())
	for _, definition := range defs.Slice() {

		modelData := definition.ORM.Model
		definitionsData = append(definitionsData, modelData)

		d := struct {
			Repository  string
			Model       *libdata.ModelDefinition
			CustomFuncs []*liborm.CustomFunc
		}{
			Repository:  defs.Repository,
			Model:       modelData,
			CustomFuncs: definition.CustomFuncs,
		}

		buf := &bytes.Buffer{}
		err = clientTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println(err)
		}
		// content, err := format.Source(buf.Bytes())
		// if err != nil {
		// 	fmt.Println(buf)
		// 	fmt.Println("grpc client ", err)
		// }
		err = ioutil.WriteFile(
			fmt.Sprintf("gen/grpc/client/%s.gen.go", d.Model.Snake()),
			buf.Bytes(), 0644)
		if err != nil {
			fmt.Println(err)
		}

	}

	d := struct {
		Repository  string
		Definitions []*libdata.ModelDefinition
	}{
		Repository:  defs.Repository,
		Definitions: definitionsData,
	}

	buf := &bytes.Buffer{}
	err = initClientTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(err)
	}
	content, err := format.Source(buf.Bytes())
	if err != nil {
		fmt.Println(buf)
		fmt.Println("init grpc client ", err)
	}
	err = ioutil.WriteFile(
		"gen/grpc/client/init.gen.go",
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}

}

var clientTemplate = template.Must(template.New("").Funcs(template.FuncMap{
	"prepareRequest": func(field libdata.Field) string {

		valueContent := "*{{.Model.Name}}.{{.Title}}"
		if field.Type() == timeType {
			valueContent = `&pb.Datetime{
				Year: int32(data.{{.Title}}.Year()),
				Month: int32(data.{{.Title}}.Month()),
				Day: int32(data.{{.Title}}.Day()),
				Hour: int32(data.{{.Title}}.Hour()),
				Minute: int32(data.{{.Title}}.Minute()),
				Second: int32(data.{{.Title}}. Second()),
				Nanosecond: int32(data.{{.Title}}.Nanosecond()),
			}`
		}
		value := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(valueContent)).Execute(value, field)
		if err != nil {
			fmt.Println(err)
		}

		var content string
		if !field.GetRequired() {
			content = `
			var {{.Field.Name}} *pb.{{.Field.Model.Title}}_{{.Field.Title}}
			if {{.Field.Model.Name}}.{{.Field.Title}} != nil {
				{{.Field.Name}} = &pb.{{.Field.Model.Title}}_{{.Field.Title}}{
					{{.Field.Title}}: {{.Value}},
				}
			}
			`
		}

		buf := &bytes.Buffer{}
		err = template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Field libdata.Field
			Value string
		}{
			Field: field,
			Value: value.String(),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"convertToRequest": func(field libdata.Field) string {

		var content string
		if field.GetRequired() && field.Type() == timeType {
			content = `{{.LesserTitle}}:           &pb.Datetime{
				Year: int32({{.Model.Name}}.{{.Title}}.Year()),
				Month: int32({{.Model.Name}}.{{.Title}}.Month()),
				Day: int32({{.Model.Name}}.{{.Title}}.Day()),
				Hour: int32({{.Model.Name}}.{{.Title}}.Hour()),
				Minute: int32({{.Model.Name}}.{{.Title}}.Minute()),
				Second: int32({{.Model.Name}}.{{.Title}}. Second()),
				Nanosecond: int32({{.Model.Name}}.{{.Title}}.Nanosecond()),
			},`
		} else if field.GetRequired() {
			content = `{{.LesserTitle}}:           {{.Model.Name}}.{{.Title}},`
		} else {
			content = `Has{{.LesserTitle}}: {{.Name}},`
		}

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, field)
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"prepareResult": func(field libdata.Field) string {

		valueContent := "pb{{.Model.Title}}.Get{{.LesserTitle}}()"
		if field.Type() == timeType {
			valueContent = `time.Date(
				int(pb{{.Model.Title}}.Get{{.LesserTitle}}().Year),
				time.Month(pb{{.Model.Title}}.Get{{.LesserTitle}}().Month),
				int(pb{{.Model.Title}}.Get{{.LesserTitle}}().Day),
				int(pb{{.Model.Title}}.Get{{.LesserTitle}}().Hour),
				int(pb{{.Model.Title}}.Get{{.LesserTitle}}().Minute),
				int(pb{{.Model.Title}}.Get{{.LesserTitle}}().Second),
				int(pb{{.Model.Title}}.Get{{.LesserTitle}}().Nanosecond),
				nil)`
		}
		value := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(valueContent)).Execute(value, field)
		if err != nil {
			fmt.Println(err)
		}

		var content string
		if !field.GetRequired() {
			content = `
			var {{.Field.Name}}Result *{{.Field.Type}}
			if pb{{.Field.Model.Title}}.GetHas{{.Field.Title}}() != nil {
				v := {{.Value}}
				{{.Field.Name}}Result = &v
			}
			`
		}

		buf := &bytes.Buffer{}
		err = template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Field libdata.Field
			Value string
		}{
			Field: field,
			Value: value.String(),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"convertToResult": func(field libdata.Field) string {

		var content string
		if field.GetRequired() && field.Type() == timeType {
			content = `time.Date(
				int(pb{{.Model.Title}}.{{.LesserTitle}}.Year),
				time.Month(pb{{.Model.Title}}.{{.LesserTitle}}.Month),
				int(pb{{.Model.Title}}.{{.LesserTitle}}.Day),
				int(pb{{.Model.Title}}.{{.LesserTitle}}.Hour),
				int(pb{{.Model.Title}}.{{.LesserTitle}}.Minute),
				int(pb{{.Model.Title}}.{{.LesserTitle}}.Second),
				int(pb{{.Model.Title}}.{{.LesserTitle}}.Nanosecond),
				nil)`
		} else if field.GetRequired() {
			content = `pb{{.Model.Title}}.{{.LesserTitle}}`
		} else {
			content = `{{.Name}}Result`
		}

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, field)
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
}).Parse(`
//nolint: dupl
package client

import (
	"context"
	"strings"
	"time"

	fieldmask "gitlab.com/empowerlab/stack/lib-go/ptypes/fieldmask"
	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"google.golang.org/grpc/metadata"

	"{{.Repository}}/gen/grpc/pb"
	mpb "{{.Repository}}/gen/orm/pb"
	"{{.Repository}}/gen/orm"
)

var dummyTime{{.Model.Title}} time.Time

/*Get{{.Model.Title}} call the remove service to get the specified {{.Model.Name}}, by its uuid.
 */
func (c *Client) Get{{.Model.Title}}(
	ctx context.Context,
	uuid string,
) (*orm.{{.Model.Title}}, error) {

	md := metadata.MD{}
	if ctx.Value("traceID") != nil {
		md.Append("traceID", ctx.Value("traceID").(string))
	}
	if ctx.Value("token") != nil {
		md.Append("token", ctx.Value("token").(string))
	}
	ctx = metadata.NewOutgoingContext(ctx, md)

	{{.Model.Name}}, err := c.service.Get{{.Model.Title}}s(
		ctx,
		&mpb.GetRequest{UUID: uuid},
	)
	if err != nil {
		return nil, err
	}

	return &orm.{{.Model.Title}}{
		{{.Model.Title}}: {{.Model.Name}},
	}, nil
}

/*List{{.Model.Title}} call the remote service to get the specified {{.Model.Name}}s, 
 *filtered by the arguments.
 */
func (c *Client) List{{.Model.Title}}(
	ctx context.Context,
	filters []*libdata.Filter,
) ([]*orm.{{.Model.Title}}, error) {

	md := metadata.MD{}
	if ctx.Value("traceID") != nil {
		md.Append("traceID", ctx.Value("traceID").(string))
	}
	if ctx.Value("token") != nil {
		md.Append("token", ctx.Value("token").(string))
	}
	if ctx.Value("userUUID") != nil {
		md.Append("userUUID", ctx.Value("userUUID").(string))
	}
	if ctx.Value("groups") != nil {
		md.Append("groups", ctx.Value("groups").(string))
	}
	ctx = metadata.NewOutgoingContext(ctx, md)

	var where []*mpb.Where
	for _, filter := range filters {
		var operande string
		if filter.Operator == "IN" {
			operande = strings.Join(filter.Operande.([]string), ",")
		} else {
			operande = filter.Operande.(string)
		}
		where = append(where, &mpb.Where{
			Field:    filter.Field,
			Operator: filter.Operator,
			Operande: operande,
		})
	}

	response, err := c.service.List{{.Model.Title}}s(
		ctx,
		&mpb.ListRequest{
			Where: where,
		},
	)
	if err != nil {
		return nil, err
	}

	var {{.Model.Name}}s []*orm.{{.Model.Title}}
	for _, {{.Model.Name}} := range response.{{.Model.Title}}s {

		{{.Model.Name}}s = append({{.Model.Name}}s, &orm.{{.Model.Title}}{
			{{.Model.Title}}: {{.Model.Name}},
		})
	}
	return {{.Model.Name}}s, nil
}

/*Create{{.Model.Title}} call the remote service to create a {{.Model.Name}}.
 *Creation of nested objects is supported.
 */
func (c *Client) Create{{.Model.Title}}(
	ctx context.Context, 
	{{.Model.Name}} *orm.{{.Model.Title}},
	{{- range .Model.NestedFields }}
	{{.Name}} *mpb.Create{{.GetReference.Title}}ManyRequest,
	{{- end }}
) (*orm.{{.Model.Title}}, error) {

	md := metadata.MD{}
	if ctx.Value("traceID") != nil {
		md.Append("traceID", ctx.Value("traceID").(string))
	}
	if ctx.Value("token") != nil {
		md.Append("token", ctx.Value("token").(string))
	}
	if ctx.Value("userUUID") != nil {
		md.Append("userUUID", ctx.Value("userUUID").(string))
	}
	if ctx.Value("groups") != nil {
		md.Append("groups", ctx.Value("groups").(string))
	}
	ctx = metadata.NewOutgoingContext(ctx, md)

	pb{{.Model.Title}}, err := c.service.Create{{.Model.Title}}(
		ctx,
		&mpb.Create{{.Model.Title}}Request{
			{{.Model.Title}}: {{.Model.Name}}.{{.Model.Title}},
			{{- range .Model.NestedFields }}
			{{.Title}}: {{.Name}},
			{{- end }}
		},
	)
	if err != nil {
		return nil, err
	}

	return &orm.{{.Model.Title}}{
		{{.Model.Title}}: pb{{.Model.Title}},
	}, nil
}

/*Update{{.Model.Title}} call the remote service to update the specified {{.Model.Name}}, 
 *by its uuid. Nested update are supported.
 */
//nolint: dupl
func (c *Client) Update{{.Model.Title}}(
	ctx context.Context, 
	uuid string,
	{{.Model.Name}} *orm.{{.Model.Title}},
	updateMask *fieldmask.FieldMask,
	{{- range .Model.NestedFields }}
	{{.Name}} *mpb.Update{{.GetReference.Title}}ManyRequest,
	{{- end }}
) (*orm.{{.Model.Title}}, error) {

	md := metadata.MD{}
	if ctx.Value("traceID") != nil {
		md.Append("traceID", ctx.Value("traceID").(string))
	}
	if ctx.Value("token") != nil {
		md.Append("token", ctx.Value("token").(string))
	}
	if ctx.Value("userUUID") != nil {
		md.Append("userUUID", ctx.Value("userUUID").(string))
	}
	if ctx.Value("groups") != nil {
		md.Append("groups", ctx.Value("groups").(string))
	}
	ctx = metadata.NewOutgoingContext(ctx, md)

	pb{{.Model.Title}}, err := c.service.Update{{.Model.Title}}(
		ctx,
		&pb.GRPCUpdate{{.Model.Title}}Request {
			Where: &mpb.GetRequest{UUID: uuid},
			Data: &mpb.Update{{.Model.Title}}Request{
				{{.Model.Title}}: {{.Model.Name}}.{{.Model.Title}},
				UpdateMask: updateMask,
				{{- range .Model.NestedFields }}
				{{.Title}}: {{.Name}},
				{{- end }}
			},
		},		
	)
	if err != nil {
		return nil, err
	}

	return &orm.{{.Model.Title}}{
		{{.Model.Title}}: pb{{.Model.Title}},
	}, nil
}

/*Delete{{.Model.Title}} call the remote service to delete the specified {{.Model.Name}}, 
 *by its uuid.
 */
func (c *Client) Delete{{.Model.Title}}(
	ctx context.Context, uuid string,
) (*orm.{{.Model.Title}}, error) {

	md := metadata.MD{}
	if ctx.Value("traceID") != nil {
		md.Append("traceID", ctx.Value("traceID").(string))
	}
	if ctx.Value("token") != nil {
		md.Append("token", ctx.Value("token").(string))
	}
	if ctx.Value("userUUID") != nil {
		md.Append("userUUID", ctx.Value("userUUID").(string))
	}
	if ctx.Value("groups") != nil {
		md.Append("groups", ctx.Value("groups").(string))
	}
	ctx = metadata.NewOutgoingContext(ctx, md)

	{{.Model.Name}}, err := c.service.Delete{{.Model.Title}}(
		ctx,
		&mpb.GetRequest{
			UUID: uuid,
		},
	)
	if err != nil {
		return nil, err
	}
	
	return &orm.{{.Model.Title}}{
		{{.Model.Title}}: {{.Model.Name}},
	}, nil
}

{{- range .Model.Fields }}
{{if .GetReference}}
{{if not .IsNested}}
/*Get{{.TitleWithoutUUID}}By{{$.Model.Title}} call the remove service to return the
 *{{.TitleWithoutUUID}} linked to many2one field, of the specified {{$.Model.Name}} record.
 */
func (c *Client) Get{{.TitleWithoutUUID}}By{{$.Model.Title}}(
	ctx context.Context,
	uuid string,
) (*orm.{{.GetReference.Title}}, error) {

	md := metadata.MD{}
	if ctx.Value("traceID") != nil {
		md.Append("traceID", ctx.Value("traceID").(string))
	}
	if ctx.Value("token") != nil {
		md.Append("token", ctx.Value("token").(string))
	}
	if ctx.Value("userUUID") != nil {
		md.Append("userUUID", ctx.Value("userUUID").(string))
	}
	if ctx.Value("groups") != nil {
		md.Append("groups", ctx.Value("groups").(string))
	}
	ctx = metadata.NewOutgoingContext(ctx, md)

	{{.GetReference.Name}}, err := c.service.Get{{.TitleWithoutUUID}}By{{$.Model.Title}}(
		ctx,
		&mpb.GetRequest{UUID: uuid},
	)
	if err != nil {
		return nil, err
	}

	return &orm.{{.GetReference.Title}}{
		{{.GetReference.Title}}: {{.GetReference.Name}},
	}, nil

}
{{else}}
/*Get{{.Title}}By{{$.Model.Title}} call the remove service to return the {{.TitleWithoutUUID}}s 
 *linked to nested field, of the specified {{$.Model.Name}} record.
 */
func (c *Client) Get{{.Title}}By{{$.Model.Title}}(
	ctx context.Context,
	uuid string,
) ([]*orm.{{.GetReference.Title}}, error) {

	md := metadata.MD{}
	if ctx.Value("traceID") != nil {
		md.Append("traceID", ctx.Value("traceID").(string))
	}
	if ctx.Value("token") != nil {
		md.Append("token", ctx.Value("token").(string))
	}
	if ctx.Value("userUUID") != nil {
		md.Append("userUUID", ctx.Value("userUUID").(string))
	}
	if ctx.Value("groups") != nil {
		md.Append("groups", ctx.Value("groups").(string))
	}
	ctx = metadata.NewOutgoingContext(ctx, md)

	response, err := c.service.Get{{.Title}}By{{$.Model.Title}}(
		ctx,
		&mpb.GetRequest{UUID: uuid},
	)
	if err != nil {
		return nil, err
	}

	var {{.Name}} []*orm.{{.GetReference.Title}}
	for _, {{.GetReference.Name}} := range response.{{.GetReference.Title}}s {

		{{.Name}} = append({{.Name}}, &orm.{{.GetReference.Title}}{
			{{.GetReference.Title}}: {{.GetReference.Name}},
		})
	}
	return {{.Name}}, nil
}
{{end}}
{{end}}
{{- end }}

{{- range .CustomFuncs}}
/*{{.Title}} call the {{.Title}} custom function on the remote service,
 *linked to {{$.Model.Name}} object.
 */
func (c *Client) {{.Title}}(
	ctx context.Context, 
	{{- range .Args}}
	{{.Name}} {{.Type}},
	{{- end }}
) (
	{{- range .Results}}
	{{.Name}} {{.Type}},
	{{- end }}
	error,
) {

	md := metadata.MD{}
	if ctx.Value("traceID") != nil {
		md.Append("traceID", ctx.Value("traceID").(string))
	}
	if ctx.Value("token") != nil {
		md.Append("token", ctx.Value("token").(string))
	}
	if ctx.Value("userUUID") != nil {
		md.Append("userUUID", ctx.Value("userUUID").(string))
	}
	if ctx.Value("groups") != nil {
		md.Append("groups", ctx.Value("groups").(string))
	}
	ctx = metadata.NewOutgoingContext(ctx, md)

	_, err := c.service.{{.Title}}(
		ctx,
		&pb.{{.Title}}Request{
			{{- range .Args}}
			{{.Title}}: {{.Name}},
			{{- end }}
		},
	)
	if err != nil {
		return err
	}
	
	return nil
}
{{- end }}
`))

var initClientTemplate = template.Must(template.New("").Parse(`package client

import (
	"fmt"
	"context"

	"google.golang.org/grpc"
	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"github.com/pkg/errors"
	"github.com/grpc-ecosystem/grpc-opentracing/go/otgrpc"
	opentracing "github.com/opentracing/opentracing-go"

	"{{.Repository}}/gen/grpc/pb"
)


/*Client contains the credentials to contact a remote service.
 */
type Client struct {
	conn    *grpc.ClientConn
	service pb.GRPCServiceClient
}

//Close close the client connection.
func (c *Client) Close() {
	c.conn.Close()
}

/*NewClient return a new client for the remote service.
 */
func NewClient(url string, tracer opentracing.Tracer) (*Client, error) {
	conn, err := grpc.Dial(
		url, 
		grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(
			otgrpc.OpenTracingClientInterceptor(tracer)),
		grpc.WithStreamInterceptor(
			otgrpc.OpenTracingStreamClientInterceptor(tracer)))
	if err != nil {
		return nil, err
	}
	c := pb.NewGRPCServiceClient(conn)
	return &Client{conn, c}, nil
}

/*InitSingleTenant call the remote service to initiate the database.
 */
func (c *Client) InitSingleTenant(ctx context.Context) error {
	_, err := c.service.InitSingleTenant(
		ctx,
		&pb.InitSingleTenantRequest{},
	)
	if err != nil {
		return err
	}
	return nil
}

func (c *Client) Get(ctx context.Context, model string, uuid string) (interface{}, error) {

	{{- range .Definitions }}
	if model == "{{.Name}}" {
		return c.Get{{.Title}}(ctx, uuid)
	}
	{{- end }}

	return nil, errors.New(fmt.Sprintf("The model %v doesn't exist", model))
}

func (c *Client) List(ctx context.Context, model string, filters []*libdata.Filter) ([]interface{}, error) {

	{{- range .Definitions }}
	if model == "{{.Name}}" {
		{{.Name}}s, err := c.List{{.Title}}(ctx, filters)
		if err != nil {
			return nil, err
		}

		var records []interface{}
		for _, {{.Name}} := range {{.Name}}s {
			records = append(records, {{.Name}})
		}
		return records, nil
	}
	{{- end }}

	return nil, errors.New(fmt.Sprintf("The model %v doesn't exist", model))
}
`))
