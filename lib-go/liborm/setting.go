package liborm

// import (
// 	"context"
// 	"fmt"
// 	// "os"
// 	"time"

// 	// "github.com/jmoiron/sqlx"
// 	"github.com/pkg/errors"
// 	// "github.com/scylladb/gocqlx"
// 	"gitlab.com/empowerlab/stack/lib-go/libdata"
// 	// "gitlab.com/empowerlab/stack/lib-go/models"
// )

// // var settingModel = db.Model{
// // 	Model:    "setting",
// // 	Datetime: true,
// // }

// // Setting todo
// type Setting struct {
// 	ID        string    `json:"id" db:"id"`
// 	TenantID  string    `json:"tenantID" db:"tenant_id"`
// 	Value     string    `json:"value" db:"value"`
// 	CreatedAt time.Time `json:"createdAt" db:"created_at"`
// 	UpdatedAt time.Time `json:"updatedAt" db:"updated_at"`
// }

// // GetID todo
// func (a *Setting) GetID() string {
// 	return a.ID
// }

// // SetID todo
// func (a *Setting) SetID(id string) {
// 	a.ID = id
// }

// // GetCreatedAt todo
// func (a *Setting) GetCreatedAt() time.Time {
// 	return a.CreatedAt
// }

// // GetUpdatedAt todo
// func (a *Setting) GetUpdatedAt() time.Time {
// 	return a.UpdatedAt
// }

// // SetTenant todo
// func (a *Setting) SetTenant(tenant string) {
// 	a.TenantID = tenant
// }

// // SettingDefinition todo
// type SettingDefinition struct {
// 	Definition
// }

// var settingDefinition = SettingDefinition{
// 	Definition: Definition{
// 		DBInfo: &libdata.Model{
// 			Model:       "setting",
// 			Datetime:    true,
// 			CanAssignID: true,
// 		},
// 	},
// }

// func init() {
// 	settingDefinition.Init()
// 	settingDefinition.Definition.Newer = settingDefinition.newRecord
// }
// func (d *SettingDefinition) newRecord() interface{} {
// 	return &Setting{}
// }

// // SettingUnsetError todo
// type SettingUnsetError struct {
// 	ID string
// }

// func (e *SettingUnsetError) Error() string {
// 	return fmt.Sprintf("Unset setting %s", e.ID)
// }

// // SettingGet todo
// func SettingGet(ctx context.Context, tx *libdata.Transaction, id string) (*string, error) {

// 	_, settingByIds, err := settingDefinition.Select(ctx, tx, []*libdata.Filter{{
// 		Field: "id", Operator: "=", Operande: id,
// 	}}, []string{}, nil, nil, nil)
// 	if err != nil {
// 		return nil, errors.Wrap(err, "Couldn't select settings")
// 	}
// 	// settings := map[string]*Setting{}
// 	// switch os.Getenv("DATABASE_TYPE") {
// 	// case db.CassandraType:
// 	// 	settingMaps, err := rows.(*gocqlx.Iterx).Iter.SliceMap()
// 	// 	if err != nil {
// 	// 		return nil, errors.Wrap(err, "Couldn't scan to map")
// 	// 	}
// 	// 	for _, r := range settingMaps {
// 	// 		settings[r["id"].(string)] = &Setting{
// 	// 			ID:    r["id"].(string),
// 	// 			Value: r["value"].(string),
// 	// 		}
// 	// 	}
// 	// // nolint: dupl
// 	// case db.PostgresType:
// 	// 	for rows.(*sqlx.Rows).Next() {
// 	// 		var setting Setting
// 	// 		err := rows.(*sqlx.Rows).StructScan(&setting)
// 	// 		if err != nil {
// 	// 			return nil, errors.Wrap(err, "Couldn't scan to settings")
// 	// 		}
// 	// 		settings[setting.ID] = &setting
// 	// 	}
// 	// }

// 	if _, ok := settingByIds[id]; !ok {
// 		return nil, &SettingUnsetError{ID: id}
// 	}

// 	return &settingByIds[id].(*Setting).Value, nil
// }

// // SettingSet todo
// func SettingSet(ctx context.Context, tx *libdata.Transaction, setting *Setting) error {

// 	_, err := SettingGet(ctx, tx, setting.ID)
// 	if err != nil {
// 		switch errors.Cause(err).(type) {
// 		case *SettingUnsetError:
// 			_, _, errC := settingDefinition.Create(ctx, tx, []interface{}{setting})
// 			// fields := []string{"id", "tenant_id", "value"}
// 			// _, err = tx.Insert(ctx, &settingModel, []interface{}{setting}, fields)
// 			if errC != nil {
// 				return errors.Wrap(errC, "Couldn't set settings")
// 			}
// 		default:
// 			return errors.Wrap(err, "Couldn't get setting")
// 		}

// 	} else {
// 		_, err := settingDefinition.Update(ctx, tx, []*libdata.Filter{
// 			{Field: "id", Operator: "=", Operande: setting.ID},
// 		}, setting, []string{"value"}, []string{})
// 		if err != nil {
// 			return errors.Wrap(err, "Couldn't update setting")
// 		}

// 		// switch os.Getenv("DATABASE_TYPE") {
// 		// case db.CassandraType:
// 		// case db.PostgresType:
// 		// 	err = row.(*sqlx.Row).StructScan(setting)
// 		// 	if err != nil {
// 		// 		return errors.Wrap(err, "Couldn't scan to setting")
// 		// 	}
// 		// default:
// 		// 	return &db.TypeError{}
// 		// }
// 	}

// 	return nil

// }
