package liborm

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"

	// "strings"
	"text/template"
	// "go/format"

	// "github.com/dave/jennifer/jen"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
	// "gitlab.com/empowerlab/stack/lib-go/libdata/fields"
)

// GenProtos will generate the protobuf files for the models.
func (defs *Definitions) GenProtos() {

	d := struct {
		Repository string
		Models     []*libdata.ModelDefinition
	}{
		Repository: defs.Repository,
	}

	for _, definition := range defs.Slice() {

		d.Models = append(d.Models, definition.Model)

		// for _, model := range d.Models {
		// 	for _, field := range model.Fields {
		// 		if field.Type() == fields.DateFieldType {
		// 			field.Type = "Datetime"
		// 		}
		// 		if field.Type() == fields.DatetimeFieldType {
		// 			field.Type = "Datetime"
		// 		}
		// 	}
		// }
	}

	err := os.MkdirAll("gen/orm/pb", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	buf := &bytes.Buffer{}
	err = protoTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println(buf)
		fmt.Println("model proto", err)
	}
	err = ioutil.WriteFile(
		"gen/orm/pb/main.proto",
		buf.Bytes(), 0644)
	if err != nil {
		fmt.Println(err)
	}

}

// nolint:lll
var protoTemplate = template.Must(template.New("").Funcs(template.FuncMap{
	"add": func(i int, j int) int {
		return i + j
	},
	"position": func(fields []libdata.Field, j int) int {
		i := len(fields)
		return i + j
	},
	"getField": func(model *libdata.ModelDefinition, field libdata.Field, i int, j int) string {

		var content string

		if field.GetRequired() {
			content = `{{.Field.ProtoType}} {{.Field.Name}} = {{.Position}} [(gogoproto.moretags) = "db:\"{{.Field.Snake}}\""];`
		} else {
			if model.UseOneOf {
				content = `{{oneof has{{.Field.Title}} { {{.Field.ProtoType}} {{.Field.Name}} = {{.Position}};}`
			} else {
				content = `{{.Field.ProtoType}} {{.Field.Name}} = {{.Position}} [(gogoproto.nullable) = true, (gogoproto.moretags) = "db:\"{{.Field.Snake}}\""];`
			}
		}

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Field    libdata.Field
			Position int
		}{
			Field:    field,
			Position: i + j,
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
}).Parse(`syntax = "proto3";
package pb;

import "gitlab.com/empowerlab/stack/lib-go/ptypes/timestamp/timestamp.proto";
import "gitlab.com/empowerlab/stack/lib-go/ptypes/fieldmask/fieldmask.proto";
import "github.com/gogo/protobuf/gogoproto/gogo.proto";

message Where {
	string field = 1;
	string operator = 2;
	string operande = 3;
}

message ListRequest {
	repeated Where where = 1;
	string orderBy = 2;
	int32 skip = 3;
	string after = 4;
	string before = 5;
	int32 first = 6;
	int32 last = 7;
}  
  
message GetRequest {
	string uuid = 1 [(gogoproto.customname) = "UUID"];
  }

{{- range .Models }}
message Create{{.Title}}Request {
	{{.Title}} {{.Name}} = 1;
	{{- range $i, $field := .NestedFields }}
	Create{{$field.GetReference.Title}}ManyRequest {{$field.Name}} = {{add $i 2}};
	{{- end }}
}

message Update{{.Title}}Request {
	{{.Title}} {{.Name}} = 1;
	google.protobuf.FieldMask updateMask = 2;
	{{- range $i, $field := .NestedFields }}
	Update{{$field.GetReference.Title}}ManyRequest {{$field.Name}} = {{add $i 3}};
	{{- end }}
}

message Create{{.Title}}ManyRequest {
	repeated Create{{.Title}}Request create = 1;
	repeated GetRequest connect = 2;
}

message Update{{.Title}}ManyRequest {
	repeated Create{{.Title}}Request create = 1;
	repeated GetRequest connect = 2;
	repeated GetRequest set = 3;
 	repeated GetRequest disconnect = 4;
	repeated GetRequest delete = 5;
	repeated Update{{.Title}}Request update = 6;
	// UpdateMany []{{.Title}}UpdateManyWithWhereNestedInput
	// DeleteMany []{{.Title}}WhereInput
	// Upsert []{{.Title}}UpsertWithWhereUniqueNestedInput
}
{{- end }}	

{{- range .Models }}
{{ $model := . }}
message {{.Title}} {
	string uuid = 1 [(gogoproto.customname) = "UUID"];
	{{- range $i, $field := .StoredFields }}
	{{getField $model $field $i 2}}
	{{- end }}
	google.protobuf.Timestamp createdAt = {{position .Fields 2}} [(gogoproto.moretags) = "db:\"created_at\""];
	google.protobuf.Timestamp updatedAt = {{position .Fields 3}} [(gogoproto.moretags) = "db:\"updated_at\""];
	string externalModule = {{position .Fields 4}} [(gogoproto.moretags) = "db:\"external_module\""];
	string externalID = {{position .Fields 5}} [(gogoproto.customname) = "ExternalID", (gogoproto.moretags) = "db:\"external_id\""];

}
message {{.Title}}s {
	repeated {{.Title}} {{.Name}}s = 1;
}

{{- end }}		

`))
