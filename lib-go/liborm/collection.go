package liborm

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"github.com/pkg/errors"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
	fieldmask "gitlab.com/empowerlab/stack/lib-go/ptypes/fieldmask"
)

// Collection contains several objects of a same model.
type Collection struct {
	Definition *Definition
	Env        *Env
	slice      []libdata.ModelInterface
	byUUIDs    map[string]libdata.ModelInterface
	Update     func(interface{}, *fieldmask.FieldMask) error
	Delete     func() error
	NewPool    func() *Pool
}

// Init will initialize the collection with the specified records.
func (c *Collection) Init(records []libdata.ModelInterface) {
	c.byUUIDs = map[string]libdata.ModelInterface{}
	for _, record := range records {
		c.slice = append(c.slice, record)

		c.byUUIDs[record.(libdata.ModelInterface).GetUUID()] = record
	}

	c.Update = c.update
	c.Delete = c.delete

}

// Slice will return the records in the collection, as a slice.
func (c *Collection) Slice() []libdata.ModelInterface {
	return c.slice
}

// GetByUUID will return the specified record in the collection, by UUID.
func (c *Collection) GetByUUID(uuid string) interface{} {
	return c.byUUIDs[uuid]
}

func (c *Collection) update(
	record interface{}, updateMask *fieldmask.FieldMask,
) error {
	// for _, filter := range filters {
	// 	fmt.Printf("filters %+v\n", *filter)
	// }

	// if 1 == 0 {
	// 	err := Validate.Struct(record)
	// 	if err != nil {
	// 		return nil, errors.Wrap(err, "Couldn't validate template")
	// 	}
	// }

	// _, previousByIds, err := d.Selecter(ctx, tx, filters, []string{}, nil, nil, nil)
	// if err != nil {
	// 	return nil, errors.Wrap(err, "Couldn't get previous requirement")
	// }

	// dbFields := libutils.GetDBFields(record)
	// if d.TranslamodelFields != nil {
	// 	dbFields = libutils.RemoveStringsInSlice(dbFields, d.TranslamodelFields)
	// }
	// dbFields = libutils.RemoveStringsInSlice(dbFields, []string{"id", "created_at", "updated_at"})
	// rFields := libutils.FilterStringsInSlice(dbFields, fields)

	// fmt.Printf("rFields %+v\n", rFields)

	var uuids []string
	for _, record := range c.Slice() {
		uuids = append(uuids, record.GetUUID())
	}
	if len(updateMask.FieldMask.Paths) > 0 && len(uuids) > 0 {
		rows, errU := c.Env.Transaction.Update(c.Env.Context, c.Definition.Model, []*libdata.Filter{{
			Field: "uuid", Operator: "IN", Operande: uuids,
		}}, record, updateMask)
		if errU != nil {
			return errors.Wrap(errU, "Couldn't update template")
		}

		fmt.Println("rows", rows)

		records, err := c.NewPool().Scan(rows.(*sqlx.Rows), c)
		if err != nil {
			return err
		}

		for _, record := range records {
			c.Env.Transaction.Cluster.CacheDriver.InvalidateRecordCache(
				c.Env.Transaction.Tenant.ID, c.Definition.Model.Name, record.GetUUID())
		}

	}

	// 	switch tx.Type {
	// 	case libdata.CassandraType:
	// 	case libdata.PostgresType:
	// 		for rows.(*sqlx.Rows).Next() {
	// 			err = rows.(*sqlx.Rows).StructScan(record)
	// 			if err != nil {
	// 				return nil, errors.Wrap(err, "Couldn't scan to template")
	// 			}
	// 			// recordIds = append(recordIds, records[i].(modelInterface).GetID())
	// 			// i = i + 1
	// 		}
	// 		// err := row.(*sqlx.Row).StructScan(record)
	// 		// if err != nil {
	// 		// 	return nil, errors.Wrap(err, "Couldn't scan to template")
	// 		// }
	// 	default:
	// 		return nil, &libdata.TypeError{}
	// 	}
	// }

	// if d.TranslamodelFields != nil {
	// 	// requirement := record.(*Requirement)

	// 	if len(previousByIds) > 0 {
	// 		lang := ctx.Value(LangKey).(string)

	// 		recordTrs, _, errS := d.TranslateModel.Selecter(ctx, tx, []*libdata.Filter{
	// 			{Field: "id", Operator: "IN", Operande: libutils.MapKeys(previousByIds)},
	// 			{Field: "lang", Operator: "=", Operande: lang},
	// 		}, []string{}, nil, nil, nil)
	// 		if errS != nil {
	// 			return nil, errors.Wrap(errS, "Couldn't get requirement translations")
	// 		}

	// 		recordTrByRL := map[string]map[string]interface{}{}
	// 		for _, recordTr := range recordTrs {
	// 			rtr := recordTr.(ModelInterface)
	// 			if _, ok := recordTrByRL[rtr.GetID()]; !ok {
	// 				recordTrByRL[rtr.GetID()] = map[string]interface{}{}
	// 			}
	// 			recordTrByRL[rtr.GetID()][recordTr.(modelTrInterface).GetLang()] = recordTr
	// 		}

	// 		// fmt.Printf("requirementTrs test %s\n", requirementTrs)
	// 		// fmt.Printf("requirementTrs test %s\n", requirementTrs[requirement.ID][requirement.Lang])
	// 		// nolint: dupl
	// 		if recordTrByRL[record.(ModelInterface).GetID()][lang] != nil {
	// 			trFields := libutils.FilterStringsInSlice(d.TranslamodelFields, fields)
	// 			fmt.Printf("trFields %s\n", trFields)

	// 			if len(trFields) > 0 {
	// 				_, err = d.TranslateModel.Updater(ctx, tx, []*libdata.Filter{
	// 					{Field: "id", Operator: "IN", Operande: libutils.MapKeys(previousByIds)},
	// 					{Field: "lang", Operator: "=", Operande: lang},
	// 				}, d.UpdateTrFielder(record, lang), trFields, []string{})
	// 				if err != nil {
	// 					return nil, errors.Wrap(err, "Couldn't update translations")
	// 				}
	// 			}
	// 		} else {
	// 			_, _, err = d.TranslateModel.Creater(ctx, tx, []interface{}{d.UpdateTrFielder(record, lang)})
	// 			if err != nil {
	// 				return nil, errors.Wrap(err, "Couldn't create translations")
	// 			}
	// 		}
	// 	}
	// }

	// err = d.PostUpdater(ctx, tx, previousByIds, record, fields)
	// if err != nil {
	// 	return nil, errors.Wrap(err, "Couldn't execute post update")
	// }

	return nil
}

func (c *Collection) delete() error {

	// records, err := d.Select(ctx, tx, filters, with, nil, nil, nil)
	// if err != nil {
	// 	return nil, errors.Wrap(err, "Couldn't get template")
	// }

	// if d.TranslamodelFields != nil {
	// 	// defaultLang, errT := SettingGet(ctx, tx, "languageDefault")
	// 	// if errT != nil {
	// 	// 	return nil, errors.Wrap(errT, "Couldn't get language default")
	// 	// }
	// 	// ctx = context.WithValue(ctx, LangKey, *defaultLang)
	// 	// fmt.Println(*defaultLang)

	// 	_, err = d.TranslateModel.Deleter(ctx, tx, []*libdata.Filter{
	// 		{Field: "id", Operator: "IN", Operande: libutils.MapKeys(recordByIds)}}, []string{})
	// 	if err != nil {
	// 		return nil, errors.Wrap(err, "Couldn't delete requirement translations")
	// 	}
	// }

	// err = d.PreDeleter(ctx, tx, recordByIds)
	// if err != nil {
	// 	return nil, errors.Wrap(err, "Couldn't execute pre deletion")
	// }

	var uuids []string
	for _, record := range c.Slice() {
		uuids = append(uuids, record.(modelInterface).GetUUID())
	}

	fmt.Println(c.Env.Transaction)
	fmt.Println(c.Env.Context)
	err := c.Env.Transaction.Delete(c.Env.Context, c.Definition.Model, []*libdata.Filter{{
		Field: "uuid", Operator: "IN", Operande: uuids,
	}})
	if err != nil {
		return errors.Wrap(err, "Couldn't delete template")
	}

	for _, uuid := range uuids {
		c.Env.Transaction.Cluster.CacheDriver.InvalidateRecordCache(
			c.Env.Transaction.Tenant.ID, c.Definition.Model.Name, uuid)
	}

	return nil
}
