package liborm

import (
	"context"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
)

type modelInterface interface {
	GetUUID() string
}

// Model represent a single record.
type Model struct{}

// ModelInterface force to define the function relative to Model.
type ModelInterface interface {
	Select(
		context.Context, *libdata.Transaction, []*libdata.Filter,
		[]string, *uint, *string, *string,
	) (*Collection, error)
	Create(context.Context, *libdata.Transaction, []interface{}) (*Collection, error)
	Delete(
		context.Context, *libdata.Transaction, []*libdata.Filter, []string,
	) (*Collection, error)
	SetStore(interface{}) error
	SetNew(func() interface{}) error
}

// Env contain the context in which the current transaction is occurring.
type Env struct {
	Context     context.Context
	Transaction *libdata.Transaction
}
