package liborm

import (
	"bytes"
	"fmt"
	"go/format"
	"io/ioutil"
	"os"
	"strings"
	"text/template"
	"time"

	"github.com/iancoleman/strcase"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libdata/fields"
)

// Gen will generate the models files.
func (defs *Definitions) Gen() {

	var models []*libdata.ModelDefinition
	for _, definition := range defs.Slice() {

		// customFuncsPool, customFuncsCollection := definition.GetCustomFuncsData()

		d := struct {
			Repository            string
			Model                 *libdata.ModelDefinition
			CustomFuncsPool       []*CustomFunc
			CustomFuncsCollection []*CustomFunc
		}{
			Repository:            defs.Repository,
			Model:                 definition.Model,
			CustomFuncsPool:       definition.CustomFuncsPool,
			CustomFuncsCollection: definition.CustomFuncsCollection,
		}

		models = append(models, d.Model)

		buf := &bytes.Buffer{}
		err := modelTemplate.Execute(buf, d)
		if err != nil {
			fmt.Println("execute ", err)
		}
		// content, err := format.Source(buf.Bytes())
		// if err != nil {
		// 	fmt.Println(buf)
		// 	fmt.Println("model ", err)
		// }
		err = ioutil.WriteFile(
			fmt.Sprintf("gen/orm/%s.gen.go", strcase.ToSnake(d.Model.Name)),
			buf.Bytes(), 0644)
		if err != nil {
			fmt.Println(err)
		}

	}

	d := struct {
		Repository string
		Timestamp  time.Time
		Models     []*libdata.ModelDefinition
	}{
		Repository: defs.Repository,
		Timestamp:  time.Now(),
		Models:     models,
	}

	err := os.MkdirAll("gen/orm", os.ModePerm)
	if err != nil {
		fmt.Println(err)
	}

	buf := &bytes.Buffer{}
	err = dataTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println("execute ", err)
	}
	// content, err := format.Source(buf.Bytes())
	// if err != nil {
	// 	fmt.Println(buf)
	// 	fmt.Println("data model ", err)
	// }
	err = ioutil.WriteFile(
		"gen/orm/data.gen.go",
		buf.Bytes(), 0644)
	if err != nil {
		fmt.Println(err)
	}

	buf = &bytes.Buffer{}
	err = initTemplate.Execute(buf, d)
	if err != nil {
		fmt.Println("execute ", err)
	}
	content, err := format.Source(buf.Bytes())
	if err != nil {
		fmt.Println(buf)
		fmt.Println("init model ", err)
	}
	err = ioutil.WriteFile(
		"gen/orm/init.gen.go",
		content, 0644)
	if err != nil {
		fmt.Println(err)
	}
}

// nolint:lll
var modelTemplate = template.Must(template.New("").Funcs(template.FuncMap{
	"getCustomFuncSingleton": func(model *libdata.ModelDefinition, customFunc *CustomFunc) string {

		content := `/* {{.Title}} todo
		*/
	   	func (t *{{.ModelTitle}}) {{.Title}}({{.ArgsPrototype}}) ({{.ResultsPrototype}}) {
	   
		   collection := &{{.ModelTitle}}Collection{}
		   collection.Init([]libdata.ModelInterface{t})
		   
		   {{.Results}} := collection.{{.Title}}({{.Args}})
		   if err != nil {
			   return {{.ReturnKO}}
		   }
		   return {{.ReturnOK}}
	   
	   	}`

		var argsPrototype []string
		var args []string
		for _, arg := range customFunc.Args {
			argsPrototype = append(argsPrototype, arg.GetName()+" "+string(arg.Type()))
			args = append(args, arg.GetName())
		}

		var resultsPrototype []string
		var results []string
		var returnKO []string
		var returnOK []string
		for _, result := range customFunc.Results {
			resultsPrototype = append(resultsPrototype, string(result.Type()))
			results = append(results, result.GetName())
			returnKO = append(returnKO, "nil")
			returnOK = append(returnOK, result.GetName())
		}
		resultsPrototype = append(resultsPrototype, "error")
		results = append(results, "err")
		returnKO = append(returnKO, "err")
		returnOK = append(returnOK, "nil")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			ModelTitle       string
			Title            string
			ArgsPrototype    string
			Args             string
			ResultsPrototype string
			Results          string
			ReturnKO         string
			ReturnOK         string
		}{
			ModelTitle:       model.Title(),
			Title:            customFunc.Title(),
			ArgsPrototype:    strings.Join(argsPrototype, ","),
			Args:             strings.Join(args, ","),
			ResultsPrototype: strings.Join(resultsPrototype, ","),
			Results:          strings.Join(results, ","),
			ReturnKO:         strings.Join(returnKO, ","),
			ReturnOK:         strings.Join(returnOK, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"getCustomFuncPrototype": func(customFunc *CustomFunc) string {

		content := "{{.Title}}({{.Args}}) ({{.Results}})"

		var args []string
		for _, arg := range customFunc.Args {
			args = append(args, string(arg.GoType()))
		}

		var results []string
		for _, result := range customFunc.Results {
			results = append(results, string(result.GoType()))
		}
		results = append(results, "error")

		buf := &bytes.Buffer{}
		err := template.Must(template.New("").Parse(content)).Execute(buf, struct {
			Title   string
			Args    string
			Results string
		}{
			Title:   customFunc.Title(),
			Args:    strings.Join(args, ","),
			Results: strings.Join(results, ","),
		})
		if err != nil {
			fmt.Println(err)
		}
		return buf.String()
	},
	"convertJson": func(field libdata.Field) string {
		if field.Type() == fields.FloatFieldType {
			return fmt.Sprintf("%vField := data[\"%v\"].(float64)", field.GetName(), field.GetName())
		}
		return fmt.Sprintf("%vField := data[\"%v\"].(string)", field.GetName(), field.GetName())
	},
	"getImportExternal": func(model *libdata.ModelDefinition) string {
		importsMap := map[string]string{}
		for _, field := range model.Fields {
			if field.GetReference() != nil {
				if field.GetReference().ExternalDriver != nil {
					importsMap[field.GetReference().ExternalDriver.GetImportName()] = field.GetReference().ExternalDriver.GetImport()
				}
			}
		}
		var imports []string
		for name, path := range importsMap {
			imports = append(imports, fmt.Sprintf("%v \"%s\"", name, path))
		}
		return strings.Join(imports, "\n")
	},
	"getScanFunc": func(model *libdata.ModelDefinition) string {
		if model.Cluster != nil {
			return model.Cluster.Driver.GenerateScanFunc(model)
		}
		return fmt.Sprintf(`
		func (d *%sPool) scan(
			rows interface{}, collection *liborm.Collection,
		) ([]libdata.ModelInterface, error) {
			var records []libdata.ModelInterface
			return records, nil
		}`, model.Title())
	},
	"getImportName": func(field libdata.Field) string {
		return field.GetReference().ExternalDriver.GetImportName()
	},
	"timestamp": func() time.Time {
		return time.Now()
	},
}).Parse(`
// Code generated by go generate; DO NOT EDIT.
// This file was generated by robots at
// {{ timestamp }}

package orm

import (
	"context"
	"fmt"
	"database/sql"
	"strings"
	"encoding/json"
	"strconv"

	{{if not .Model.CanAssignID}}"github.com/google/uuid"{{end}}
	opentracing "github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	"github.com/jmoiron/sqlx"
	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/liborm"
	"gitlab.com/empowerlab/stack/lib-go/libutils"
	fieldmask "gitlab.com/empowerlab/stack/lib-go/ptypes/fieldmask"
	nats "github.com/nats-io/nats.go"
	"gitlab.com/empowerlab/stack/lib-go/ptypes/timestamp"

	"{{.Repository}}/gen/orm/pb"
	"{{.Repository}}/orm"
	{{getImportExternal .Model}}
)

/*{{.Model.Title}} is the main struct for operating a {{.Model.Name}} object, which shall
 *be used in custom code because it contains the environnement informations and
 *the easy-to-use functions.
 */
type {{.Model.Title}} struct {
	*pb.{{.Model.Title}}				
	Env *liborm.Env
}

// SetUUID is a function to set the UUID of the {{.Model.Name}}.
func (t *{{.Model.Title}}) SetUUID(uuid string) {
	t.{{.Model.Title}}.UUID = uuid
	return
}

// JSON is a function to return the {{.Model.Name}} data in JSON format.
func (t *{{.Model.Title}}) JSON() ([]byte, error) {

	data := map[string]interface{}{}
	data["uuid"] = t.{{.Model.Title}}.UUID
	{{- range .Model.StoredFields }}
	data["{{.Name}}"] = t.{{$.Model.Title}}.{{.Title}}
	{{- end}}

	return json.Marshal(data)
}


{{- range .Model.Fields }}
{{if .GetReference}}
{{if not .IsNested}}
// {{.TitleWithoutUUID}} return the {{.GetReference.Name}} behind the {{.TitleWithoutUUID}} field.
func (t *{{$.Model.Title}}) {{.TitleWithoutUUID}}() (*{{.GetReference.Title}}, error) {
	span, _ := opentracing.StartSpanFromContext(t.Env.Context, "orm-{{$.Model.Name}}-get-{{.NameWithoutUUID}}")
	defer span.Finish()
	t.Env.Context = opentracing.ContextWithSpan(t.Env.Context, span)

	collection := &{{$.Model.Title}}Collection{
		Collection: &liborm.Collection{
			Definition: {{$.Model.Title}}Definition.Definition,
			Env:        t.Env,
		},
	}
	collection.Init([]libdata.ModelInterface{t})
	
	{{.NameWithoutUUID}}sBy{{$.Model.Title}}UUID, err := collection.{{.TitleWithoutUUID}}sBy{{$.Model.Title}}UUID()
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't get {{$.Model.Name}}")
	}

	if _, ok := {{.NameWithoutUUID}}sBy{{$.Model.Title}}UUID[t.UUID]; !ok {
		return nil, errors.Wrap(err, "UUID missing")
	}
	return {{.NameWithoutUUID}}sBy{{$.Model.Title}}UUID[t.UUID], nil
}
{{else}}
// {{.Title}} return the {{.GetReference.Name}}s behind the {{.Title}} field.
func (t *{{$.Model.Title}}) {{.Title}}() (*{{.GetReference.Title}}Collection, error) {
	span, _ := opentracing.StartSpanFromContext(t.Env.Context, "orm-{{$.Model.Name}}-get-{{.Name}}")
	defer span.Finish()
	t.Env.Context = opentracing.ContextWithSpan(t.Env.Context, span)

	collection := &{{$.Model.Title}}Collection{
		Collection: &liborm.Collection{
			Definition: {{$.Model.Title}}Definition.Definition,
			Env:        t.Env,
		},
	}
	collection.Init([]libdata.ModelInterface{t})
	
	{{.Name}}By{{$.Model.Title}}UUID, err := collection.{{.Title}}By{{$.Model.Title}}UUID()
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't get {{$.Model.Name}}")
	}

	if _, ok := {{.Name}}By{{$.Model.Title}}UUID[t.UUID]; !ok {
		return nil, errors.Wrap(err, "UUID missing")
	}
	return {{.Name}}By{{$.Model.Title}}UUID[t.UUID], nil
}
{{end}}
{{end}}
{{- end }}		

{{- range .CustomFuncsCollection }}
	{{ getCustomFuncSingleton .}}
{{- end }}



/* Delete will delete the {{.Model.Name}} from the database.
 */
func (t *{{.Model.Title}}) Delete() error {
	span, _ := opentracing.StartSpanFromContext(t.Env.Context, "orm-{{.Model.Name}}-delete")
	defer span.Finish()
	t.Env.Context = opentracing.ContextWithSpan(t.Env.Context, span)

	collection := &{{.Model.Title}}Collection{
		Collection: &liborm.Collection{
			Definition: {{.Model.Title}}Definition.Definition,
			Env: t.Env,
		},
	}
	collection.Init([]libdata.ModelInterface{t})
	
	err := collection.Delete()
	if err != nil {
		return errors.Wrap(err, "Couldn't get {{.Model.Name}}")
	}

	return nil

}	


/*{{.Model.Title}}Collection is the main struct for operating a list {{.Model.Name}} object, which shall
 *be used in custom code because it contains the environnement informations and
 *the easy-to-use functions.
 */
type {{.Model.Title}}Collection struct {
	*liborm.Collection
	{{.Model.Title}}CollectionInterface
}

// {{.Model.Title}}CollectionInterface force the implementation of custom functions 
// specified for the {{.Model.Name}}s objects.
type {{.Model.Title}}CollectionInterface interface {
	{{- range .CustomFuncsCollection }}
	{{ getCustomFuncPrototype .}}
	{{- end }}
}

//GetSlice return all {{.Model.Name}}s in this collection, as a slice.
func (c *{{.Model.Title}}Collection) Slice() []*{{.Model.Title}} {
	var {{.Model.Name}}s []*{{.Model.Title}}
	for _, record := range c.Collection.Slice() {
		{{.Model.Name}}s = append({{.Model.Name}}s, record.(*{{.Model.Title}}))
	}
	return {{.Model.Name}}s
}

//GetByUUID return the {{.Model.Name}} in this collection, by uuid.
func (c *{{.Model.Title}}Collection) GetByUUID(uuid string) *{{.Model.Title}} {
	return c.Collection.GetByUUID(uuid).(*{{.Model.Title}})
}

{{- range .Model.Fields }}
{{if .GetReference}}
{{if not .IsNested}}
// {{.TitleWithoutUUID}}sBy{{$.Model.Title}}UUID return the {{.GetReference.Name}} behind {{.Title}} field,
// for each {{$.Model.Name}} in the collection, by uuid.
func (t *{{$.Model.Title}}Collection) {{.TitleWithoutUUID}}sBy{{$.Model.Title}}UUID() (map[string]*{{.GetReference.Title}}, error) {
	span, _ := opentracing.StartSpanFromContext(t.Env.Context, "orm-{{$.Model.Name}}-{{.NameWithoutUUID}}sby{{$.Model.Name}}uuid")
	defer span.Finish()
	t.Env.Context = opentracing.ContextWithSpan(t.Env.Context, span)

	result := map[string]*{{.GetReference.Title}}{}
	var {{$.Model.Name}}UUIDs []string
	for _, {{$.Model.Name}} := range t.Slice() {

		var err error
		var {{.Name}}Byte []byte
		if t.Env.Transaction.UseCache {
			key := fmt.Sprintf(
				"%s_{{.Title}}By{{$.Model.Title}}UUID_%s",
				t.Env.Transaction.Tenant.ID, {{$.Model.Name}}.UUID,
			)
			{{.Name}}Byte, err = t.Env.Transaction.Cluster.CacheDriver.GetCache(key)
		} else {
			err = errors.New("Not using cache")
		}

		if err == nil {

			var {{.Name}} *{{.GetReference.Title}}
			{{.Name}} = &{{.GetReference.Title}}{}
			err = {{.Name}}.Unmarshal({{.Name}}Byte)
		
			result[{{$.Model.Name}}.UUID] = {{.Name}}
		}

		if err != nil {

			{{$.Model.Name}}UUIDs = append({{$.Model.Name}}UUIDs, {{$.Model.Name}}.{{.Title}}.UUID)
		}
	}

	if len({{$.Model.Name}}UUIDs) > 0 {
		{{.GetReference.Name}}s, err := {{.GetReference.Title}}Definition.NewPool(t.Env).Select(
			[]*libdata.Filter{ {
				Field: "uuid", Operator: "IN", Operande: {{$.Model.Name}}UUIDs,
			} }, nil, nil, nil)
		if err != nil {
			return nil, errors.Wrap(err, "Couldn't get {{$.Model.Name}}")
		}
		result = map[string]*{{.GetReference.Title}}{}
		for _, {{$.Model.Name}} := range t.Slice() {
			result[{{$.Model.Name}}.UUID] = {{.GetReference.Name}}s.GetByUUID({{$.Model.Name}}.{{.Title}}.UUID)

			key := fmt.Sprintf(
				"%s_{{.Title}}By{{$.Model.Title}}UUID_%s",
				t.Env.Transaction.Tenant.ID, {{$.Model.Name}}.UUID,
			)
			{{$.Model.Name}}Byte, err := result[{{$.Model.Name}}.UUID].Marshal()
			if err != nil {
				return nil, errors.Wrap(err, "Couldn't marshal {{$.Model.Name}}")
			}
			err = t.Env.Transaction.Cluster.CacheDriver.SetCache(key, {{$.Model.Name}}Byte)
			if err != nil {
				return nil, errors.Wrap(err, "Couldn't set {{$.Model.Name}} in cache")
			}
		}
	}
	return result, nil
}
{{else}}
// {{.TitleWithoutUUID}}sBy{{$.Model.Title}}UUID return the {{.GetReference.Name}} behind {{.Title}} field,
// for each {{$.Model.Name}} in the collection, by uuid.
func (t *{{$.Model.Title}}Collection) {{.Title}}By{{$.Model.Title}}UUID() (map[string]*{{.GetReference.Title}}Collection, error) {
	span, _ := opentracing.StartSpanFromContext(t.Env.Context, "orm-{{$.Model.Name}}-{{.Name}}sby{{$.Model.Name}}uuid")
	defer span.Finish()
	t.Env.Context = opentracing.ContextWithSpan(t.Env.Context, span)
	
	result := map[string]*{{.GetReference.Title}}Collection{}
	var {{$.Model.Name}}UUIDs []string
	for _, {{$.Model.Name}} := range t.Slice() {

		var err error
		var {{$.Model.Name}}sUUID []byte
		if t.Collection.Env.Transaction.UseCache {
			key := fmt.Sprintf(
				"%s_{{.Title}}By{{$.Model.Title}}UUID_%s",
				t.Collection.Env.Transaction.Tenant.ID, {{$.Model.Name}}.UUID,
			)
			{{$.Model.Name}}sUUID, err = t.Env.Transaction.Cluster.CacheDriver.GetCache(key)
		} else {
			err = errors.New("Not using cache")
		}

		if err == nil {

			var {{.Name}} *{{.GetReference.Title}}Collection
			{{.GetReference.Name}}Pool := {{.GetReference.Title}}Definition.NewPool(t.Collection.Env)
			var {{.Name}}UUIDSlice []string
			if string({{$.Model.Name}}sUUID) != "" {
				{{.Name}}UUIDSlice = strings.Split(string({{$.Model.Name}}sUUID), ",")
			}
			{{.Name}}, err = {{.GetReference.Name}}Pool.SelectByUUID({{.Name}}UUIDSlice)

			result[{{$.Model.Name}}.UUID] = {{.Name}}
		}

		if err != nil {

			{{$.Model.Name}}UUIDs = append({{$.Model.Name}}UUIDs, {{$.Model.Name}}.UUID)
		}
	}

	if len({{$.Model.Name}}UUIDs) > 0 {
		{{.GetReference.Name}}s, err := {{.GetReference.Title}}Definition.NewPool(t.Env).Select(
			[]*libdata.Filter{ {
				Field: "{{$.Model.Name}}_uuid", Operator: "IN", Operande: {{$.Model.Name}}UUIDs,
			} }, nil, nil, nil)
		if err != nil {
			return nil, errors.Wrap(err, "Couldn't get {{$.Model.Name}}")
		}
		by{{$.Model.Title}}UUID := map[string][]libdata.ModelInterface{}
		for _, {{.GetReference.Name}} := range {{.GetReference.Name}}s.Slice() {
			if _, ok := by{{$.Model.Title}}UUID[{{.GetReference.Name}}.{{.GetInverseField}}.UUID]; !ok {
				by{{$.Model.Title}}UUID[{{.GetReference.Name}}.{{.GetInverseField}}.UUID] = []libdata.ModelInterface{}
			}
			by{{$.Model.Title}}UUID[{{.GetReference.Name}}.{{.GetInverseField}}.UUID] = append(
				by{{$.Model.Title}}UUID[{{.GetReference.Name}}.{{.GetInverseField}}.UUID], {{.GetReference.Name}})
		}
		result = map[string]*{{.GetReference.Title}}Collection{}
		for _, {{$.Model.Name}} := range t.Slice() {
			result[{{$.Model.Name}}.UUID] = &{{.GetReference.Title}}Collection{
				Collection: &liborm.Collection{
					Definition: {{.GetReference.Title}}Definition.Definition,
					Env:        t.Collection.Env,
				},
			}
			result[{{$.Model.Name}}.UUID].Init(by{{$.Model.Title}}UUID[{{$.Model.Name}}.UUID])

			key := fmt.Sprintf(
				"%s_{{.Title}}By{{$.Model.Title}}UUID_%s",
				t.Env.Transaction.Tenant.ID, {{$.Model.Name}}.UUID,
			)
			var uuids []string
			for _, target := range result[{{$.Model.Name}}.UUID].Slice() {
				uuids = append(uuids, target.UUID)
			}
			err = t.Env.Transaction.Cluster.CacheDriver.SetCache(key, []byte(strings.Join(uuids, ",")))
			if err != nil {
				return nil, errors.Wrap(err, "Couldn't set {{$.Model.Name}} in cache")
			}

		}
	}
	return result, nil
}
{{end}}
{{end}}
{{- end }}	

/* Update will update all the {{.Model.Name}}s in the collection with the specified data.
 */
func (d *{{.Model.Title}}Collection) Update(r *pb.Update{{.Model.Title}}Request) error {
	span, _ := opentracing.StartSpanFromContext(d.Env.Context, "orm-{{.Model.Name}}-update")
	defer span.Finish()
	d.Env.Context = opentracing.ContextWithSpan(d.Env.Context, span)

	err := d.Collection.Update(r.{{.Model.Title}}, r.UpdateMask)
	if err != nil {
		return errors.Wrap(err, "Couldn't delete")
	}

	{{if not .Model.ExternalDriver}}
	for _, record := range d.Collection.Slice() {
		{{.Model.Name}} := record.(*{{.Model.Title}})
		json, errE := {{.Model.Name}}.JSON()
		if errE != nil {
			return errE
		}
		errE = d.Env.Transaction.RegisterEvent("{{.Model.Name}}", {{.Model.Name}}.{{.Model.Title}}.UUID, "updated", json)
		if errE != nil {
			return errE
		}
	}
	{{end}}

	return nil
}

/* Delete will delete all the {{.Model.Name}}s in the collection.
 */
func (d *{{.Model.Title}}Collection) Delete() error {
	span, _ := opentracing.StartSpanFromContext(d.Env.Context, "orm-{{.Model.Name}}-delete")
	defer span.Finish()
	d.Env.Context = opentracing.ContextWithSpan(d.Env.Context, span)

	err := d.Collection.Delete()
	if err != nil {
		return errors.Wrap(err, "Couldn't delete")
	}

	{{if not .Model.ExternalDriver}}
	for _, record := range d.Collection.Slice() {
		{{.Model.Name}} := record.(*{{.Model.Title}})
		json, errE := {{.Model.Name}}.JSON()
		if errE != nil {
			return errE
		}
		errE = d.Env.Transaction.RegisterEvent("{{.Model.Name}}", {{.Model.Name}}.{{.Model.Title}}.UUID, "updated", json)
		if errE != nil {
			return errE
		}
	}
	{{end}}

	return nil
}

func (d *{{.Model.Title}}Collection) newPool() *liborm.Pool {
	pool := {{.Model.Title}}Definition.NewPool(d.Collection.Env)
	return pool.Pool
}

// var New{{.Model.Title}}Collection func(
// context.Context, *libdata.Transaction) *{{.Model.Title}}Collection

// func new{{.Model.Title}}Collection (
// 	ctx context.Context,
// 	tx *libdata.Transaction,
// ) *{{.Model.Title}}Collection {
// 	{{.Model.Title}}Collection := &liborm.ModelCollection
// 	return &{{.Model.Title}}Collection{
// 		ModelCollectionInterface: {{.Model.Title}}Collection,
// 		{{.Model.Name}}CollectionInterface: {{.Model.Title}}Collection,
// 		Environnement: &liborm.Environnement{
// 			Context: ctx,
// 			Transaction: tx,
// 		},
// 	}
// }


func init() {
	{{.Model.Title}}Definition = &{{.Model.Name}}Definition{
		Definition: orm.Definitions.GetByID("{{.Model.Name}}"),
	}
	{{.Model.Title}}Definition.PostNewPool = {{.Model.Title}}Definition.postNewPool
	{{.Model.Title}}Definition.PostNewCollection = {{.Model.Title}}Definition.postNewCollection
	{{.Model.Title}}Definition.Model.Store = &{{.Model.Title}}Store{}
	// {{.Model.Title}}Definition.Definition.Init()
	// {{.Model.Title}}Definition.ModelInterface.SetStore(&{{.Model.Title}}Store{})
	// {{.Model.Title}}Definition.ModelInterface.SetNew({{.Model.Title}}Definition.new)
	// New{{.Model.Title}}Definition = new{{.Model.Title}}Definition

	_, err := libdata.NatsClientToRemoveInFavorOfDriver.Subscribe("{{.Model.Name}}Updated", func(m *nats.Msg) {
		span := opentracing.StartSpan("subscription-{{.Model.Name}}-updated")
		defer span.Finish()

		fmt.Printf("Received a message: %s\n", string(m.Data))


		data := map[string]string{}
		err := json.Unmarshal(m.Data, &data)
		if err != nil {
			fmt.Println("err", err)
		}

		liborm.Cluster.CacheDriver.InvalidateRecordCache(data["tenantUUID"], "{{.Model.Name}}", data["uuid"])
	})
	if err != nil {
		fmt.Printf("Impossible to subcribe: %v\n", err)
	}	

	_, err = libdata.NatsClientToRemoveInFavorOfDriver.Subscribe("{{.Model.Name}}Deleted", func(m *nats.Msg) {
		span := opentracing.StartSpan("subscription-{{.Model.Name}}-deleted")
		defer span.Finish()

		fmt.Printf("Received a message: %s\n", string(m.Data))

		fmt.Println(context.Background())
		libutils.MapKeys(map[string]string{})


		data := map[string]string{}
		err := json.Unmarshal(m.Data, &data)
		if err != nil {
			fmt.Println("err", err)
		}

		liborm.Cluster.CacheDriver.InvalidateRecordCache(data["tenantUUID"], "{{.Model.Name}}", data["uuid"])

	})
	if err != nil {
		fmt.Printf("Impossible to subcribe: %v\n", err)
	}	

	{{- range .Model.StoredFields }}
	{{if .GetReference}}
	{{if .GetReference.ExternalDriver}}
	_, err = libdata.NatsClientToRemoveInFavorOfDriver.Subscribe("{{.GetReference.Name}}Created", func(m *nats.Msg) {
		span := opentracing.StartSpan("subscription-{{.GetReference.Name}}-created")
		defer span.Finish()

		fmt.Printf("Received a message: %s\n", string(m.Data))

		data := map[string]interface{}{}
		err := json.Unmarshal(m.Data, &data)
		if err != nil {
			fmt.Println("err", err)
		}

		ctx := context.Background()
		tx, err := liborm.Cluster.BeginTransaction(ctx, &libdata.Tenant{}, true)
		if err != nil {
			fmt.Println("Couldn't begin transaction")
		}
		defer func() {
			err = tx.CommitTransaction(ctx, err)
		}()
		ctx = opentracing.ContextWithSpan(ctx, span)

		{{- range .GetReference.Fields }}
		{{convertJson .}}
		{{end}}
		
		_, err = {{.GetReference.Title}}Definition.NewPool(&liborm.Env{
			Context: ctx,
			Transaction: tx,
		}).Create([]*pb.Create{{.GetReference.Title}}Request{ {
			{{.GetReference.Title}}: &pb.{{.GetReference.Title}}{
				UUID: data["uuid"].(string),
				{{- range .GetReference.Fields }}
				{{.Title}}: {{.Name}}Field,
				{{- end}}
			},
		} })
		
		if err != nil {
			fmt.Println("A message {{.GetReference.Title}}created was received but we failed the insert: %v", err)
		}
	})
	if err != nil {
		fmt.Printf("Impossible to subcribe: %v\n", err)
	}	

	_, err = libdata.NatsClientToRemoveInFavorOfDriver.Subscribe("{{.GetReference.Name}}Deleted", func(m *nats.Msg) {
		span := opentracing.StartSpan("subscription-{{.GetReference.Name}}-deleted")
		defer span.Finish()

		fmt.Printf("Received a message: %s\n", string(m.Data))

		data := map[string]interface{}{}
		err := json.Unmarshal(m.Data, &data)
		if err != nil {
			fmt.Println("err", err)
		}

		ctx := context.Background()
		tx, err := liborm.Cluster.BeginTransaction(ctx, &libdata.Tenant{}, true)
		if err != nil {
			fmt.Println("Couldn't begin transaction")
		}
		defer func() {
			err = tx.CommitTransaction(ctx, err)
		}()
		ctx = opentracing.ContextWithSpan(ctx, span)
		
		{{.GetReference.Name}}, err := {{.GetReference.Title}}Definition.NewPool(&liborm.Env{
			Context: ctx,
			Transaction: tx,
		}).GetByUUID(data["uuid"].(string))
		if err != nil {
			fmt.Println("Couldn't get {{.GetReference.Name}}: %v", err)
		}
		err = {{.GetReference.Name}}.Delete()
		if err != nil {
			fmt.Println("A message {{.GetReference.Title}}deleted was received but we failed the delete: %v", err)
		}
	})	
	if err != nil {
		fmt.Printf("Impossible to subcribe: %v\n", err)
	}	
	{{end}}
	{{end}}
	{{- end}}

	_, _ = strconv.ParseFloat("1.1", 64)

}

/*{{.Model.Title}}Store is used in the libdata module for performing manipulation on data.
 */
type {{.Model.Title}}Store struct{}

// InsertBuildArgs return the fields and arguments to build the SQL insert request.
func (s *{{.Model.Title}}Store) InsertBuildArgs(
	records []libdata.ModelInterface) ([]string, map[string]interface{}) {
	
	args := map[string]interface{}{}
	for i, record := range records {
		storage := record.(*pb.{{.Model.Title}})

		args[fmt.Sprintf("%v_uuid", i)] = storage.UUID
		{{- range .Model.StoredFields }}
		
		{{if eq .Type "Many2oneType"}}
		{{.Name}} := storage.{{.Title}}.UUID
		{{else}}
			{{.Name}} := &storage.{{.Title}}
			{{if eq .GoType "string"}}
			if *{{.Name}} == "" {
				{{.Name}} = nil
			} 
			{{end}}
		{{end}}
		args[fmt.Sprintf("%v_{{.Snake}}", i)] = {{.Name}}
		{{- end }}	
		args[fmt.Sprintf("%v_external_module", i)] = storage.ExternalModule
		args[fmt.Sprintf("%v_external_id", i)] = storage.ExternalID	
	}

	return []string{
		"uuid",
		{{- range .Model.StoredFields }}
		"{{.Snake}}",
		{{- end }}
		"external_module",
		"external_id",
	}, args
}


// UpdateBuildArgs return the fields and arguments to build the SQL update request.
func (s *{{.Model.Title}}Store) UpdateBuildArgs(
	record interface{}, updateMask *fieldmask.FieldMask) ([]string, map[string]interface{}) {
	
	var set []string
	args := map[string]interface{}{}
	storage := record.(*pb.{{.Model.Title}})
	for _, f := range updateMask.FieldMask.Paths {
		// if set != "" {
		// 	set = fmt.Sprintf("%s, ", set)
		// }
		set = append(set, fmt.Sprintf("%s=:%s", f, f))
		//nolint: goconst
		switch f {
		{{- range .Model.StoredFields }}
		case "{{.Name}}":
			args["{{.Snake}}"] = storage.{{.Title}}
		{{- end }}	
		}
	}

	return set, args
}

{{.Model.StoreCustomFunctions}}

type {{.Model.Name}}Interface interface {
	{{- range .CustomFuncsCollection }}
	{{ getCustomFuncPrototype .}}
	{{- end }}
}

/*{{.Model.Title}}Definition contain the definition of the {{.Model.Name}} object.
 *It is also used to generate the pool containing the main functions with the environnement.
 */
var {{.Model.Title}}Definition *{{.Model.Name}}Definition

type {{.Model.Name}}Definition struct {
	*liborm.Definition
	PostNewPool func(*{{.Model.Title}}Pool) 
	PostNewCollection func(*{{.Model.Title}}Collection) 
}

// NewPool generate a new pool for {{.Model.Name}} from the definition, containing
// the main functions and the environnement.
func (d *{{.Model.Name}}Definition) NewPool(
	env *liborm.Env,
) *{{.Model.Title}}Pool {
	result := &{{.Model.Title}}Pool{
		Pool: &liborm.Pool{
			Definition: d.Definition,
			Env: env,
		},
	}
	result.Init()
	result.Pool.New = result.new
	result.Pool.Scan = result.scan
	result.Pool.CheckExternalForeignKeys = result.checkExternalForeignKeys
	d.PostNewPool(result)
	return result
}

func (d *{{.Model.Name}}Definition) postNewPool(*{{.Model.Title}}Pool) {}

func (d *{{.Model.Name}}Definition) postNewCollection(*{{.Model.Title}}Collection) {}

type {{.Model.Name}}PoolInterface interface {
	{{- range .CustomFuncsPool }}
	{{ getCustomFuncPrototype .}}
	{{- end }}
}

/*{{.Model.Title}}Pool is the main struct for operating {{.Model.Name}} objects, which shall
 *be used in custom code because it contains the environnement informations and
 *the easy-to-use functions.
 */
type {{.Model.Title}}Pool struct {
	*liborm.Pool
	{{.Model.Name}}PoolInterface
	PreCreate func([]*pb.Create{{.Model.Title}}Request) error
}


// New return a new instance of a {{.Model.Name}} object.
func (d *{{.Model.Title}}Pool) New() *{{.Model.Title}} {
	return d.new().(*{{.Model.Title}})
}
func (d *{{.Model.Title}}Pool) new() libdata.ModelInterface {
	return &{{.Model.Title}}{
		{{.Model.Title}}: &pb.{{.Model.Title}}{},
		Env: d.Pool.Env,
	}
}

{{ getScanFunc .Model }}

func (d *{{.Model.Title}}Pool) checkExternalForeignKeys(
	records []libdata.ModelInterface,
) error {

	{{- range .Model.StoredFields }}
	{{if .GetReference}}
	{{if .GetReference.ExternalDriver}}
	{{$.Model.Name}}sMap := map[string]string{}
	for _, record := range records {
		{{$.Model.Name}} := record.(*pb.{{$.Model.Title}})
		{{ if eq .Type "Many2oneType" }}
			{{$.Model.Name}}sMap[{{$.Model.Name}}.{{.Title}}.UUID] = {{$.Model.Name}}.{{.Title}}.UUID
		{{else}}
			{{$.Model.Name}}sMap[{{$.Model.Name}}.{{.Title}}] = {{$.Model.Name}}.{{.Title}}
		{{ end }}
	}
	{{.Name}}s := libutils.MapKeys({{$.Model.Name}}sMap)

	{{if not .GetReference.DisableDatabaseStore}}
	{{.GetReference.Name}}Pool := {{.GetReference.Title}}Definition.NewPool(d.Env)
	referenced{{.GetReference.Title}}s, err := {{.GetReference.Name}}Pool.SelectByUUID({{.Name}}s)
	if err != nil {
		return errors.Wrap(err, "Couldn't get {{.GetReference.Name}}s")
	}
	for _, {{.GetReference.Name}} := range referenced{{.GetReference.Title}}s.Slice() {
		{{.Name}}s = libutils.RemoveStringsInSlice({{.Name}}s, []string{ {{.GetReference.Name}}.UUID})
	}
	{{end}}

	if len({{.Name}}s) > 0 {
		referencedRecords, err := orm.Definitions.GetByID("{{.GetReference.Name}}").Model.ExternalDriver.Select(d.Env.Context, "{{.GetReference.Name}}", []*libdata.Filter{ {Field: "uuid", Operator: "IN", Operande: {{.Name}}s} })
		if err != nil {
			return errors.Wrap(err, "Couldn't get {{.GetReference.Name}}s")
		}

		for _, referenceRecord := range referencedRecords {
			{{.Name}}s = libutils.RemoveStringsInSlice({{.Name}}s, []string{referenceRecord.(*{{getImportName .}}.{{.GetReference.Title}}).UUID})
		}

		{{if not .GetReference.DisableDatabaseStore}}if len(referencedRecords) > 0 {
			var {{.GetReference.Name}}s []*pb.Create{{.GetReference.Title}}Request
			for _, record := range referencedRecords {
				importmodel := record.(*{{getImportName .}}.{{.GetReference.Title}})
				{{.GetReference.Name}}s = append({{.GetReference.Name}}s, &pb.Create{{.GetReference.Title}}Request{
					{{.GetReference.Title}}: &pb.{{.GetReference.Title}}{
						UUID: importmodel.UUID,
						{{- range .GetReference.Fields }}
						{{.Title}}: importmodel.{{.Title}},
						{{- end}}
					},
				})
			}
			_, err = {{.GetReference.Name}}Pool.Create({{.GetReference.Name}}s)
			if err != nil {
				return errors.Wrap(err, "Couldn't create {{.GetReference.Name}}s")
			}
		}
		{{end}}

	}

	for _, uuid := range {{.Name}}s {
		return errors.New(fmt.Sprintf("The {{.GetReference.Name}} with uuid  %s doesn't exist", uuid))
	}


	{{end}}
	{{end}}
	{{- end}}

	return nil

}

// NewCollection return a new instance of a {{.Model.Name}} collection object.
func (d *{{.Model.Title}}Pool) NewCollection(records *liborm.Collection) *{{.Model.Title}}Collection {

	result := &{{.Model.Title}}Collection{
		Collection: records,
	}
	result.Collection.NewPool = result.newPool
	{{.Model.Title}}Definition.PostNewCollection(result)
	return result
}

// GetByUUID return the {{.Model.Name}} object from the pool, by its uuid.
func (d *{{.Model.Title}}Pool) GetByUUID(
	uuid string,
) (*{{.Model.Title}}, error) {
	span, _ := opentracing.StartSpanFromContext(d.Env.Context, "orm-{{.Model.Name}}-getbyuuid")
	defer span.Finish()
	d.Env.Context = opentracing.ContextWithSpan(d.Env.Context, span)

	var err error
	var {{.Model.Name}}Byte []byte
	if d.Pool.Env.Transaction.UseCache {
		{{.Model.Name}}Byte, err = d.Env.Transaction.Cluster.CacheDriver.GetRecordCache(d.Env.Transaction.Tenant.ID, "{{.Model.Name}}", uuid)
	} else {
		err = errors.New("Not using cache")
	}
	
	var {{.Model.Name}} *{{.Model.Title}}
	if err == nil {

		{{.Model.Name}} = &{{.Model.Title}}{
			{{.Model.Title}}: &pb.{{.Model.Title}}{},
			Env: d.Pool.Env,
		}
		err = {{.Model.Name}}.Unmarshal({{.Model.Name}}Byte)
	
		// result[{{.Model.Name}}.UUID] = {{.Model.Name}}
	}

	if err != nil {
		
		{{.Model.Name}}s, err := d.Select(
			[]*libdata.Filter{ {
				Field: "uuid", Operator: "=", Operande: uuid,
			} }, nil, nil, nil)
		if err != nil {
			return nil, errors.Wrap(err, "Couldn't get {{.Model.Name}}s")
		}

		if len({{.Model.Name}}s.Slice()) < 1 {
			return nil, errors.New("No {{.Model.Name}} exist with this uuid")
		} else if len({{.Model.Name}}s.Slice()) > 1 {
			return nil, errors.New("Several {{.Model.Name}}s exist with this uuid")
		}
		{{.Model.Name}} = {{.Model.Name}}s.Slice()[0]
	}

	return {{.Model.Name}}, nil
}


// SelectByUUID return the {{.Model.Name}} objects from the pool, by their uuids.
func (d *{{.Model.Title}}Pool) SelectByUUID(uuids []string) (*{{.Model.Title}}Collection, error) {
	span, _ := opentracing.StartSpanFromContext(d.Env.Context, "orm-{{.Model.Name}}-selectbyuuid")
	defer span.Finish()
	d.Env.Context = opentracing.ContextWithSpan(d.Env.Context, span)

	{{.Model.Name}}s := map[string]*{{.Model.Title}}{}
	var uuidsToSelect []string
	for _, uuid := range uuids {
		var err error
		var {{.Model.Name}}Byte []byte
		if d.Pool.Env.Transaction.UseCache {
			{{.Model.Name}}Byte, err = d.Env.Transaction.Cluster.CacheDriver.GetRecordCache(d.Env.Transaction.Tenant.ID, "{{.Model.Name}}", uuid)
		} else {
			err = errors.New("Not using cache")
		}
		
		var {{.Model.Name}} *{{.Model.Title}}
		if err == nil {
	
			{{.Model.Name}} = &{{.Model.Title}}{
				{{.Model.Title}}: &pb.{{.Model.Title}}{},
			}
			err = {{.Model.Name}}.Unmarshal({{.Model.Name}}Byte)
		
			{{.Model.Name}}s[{{.Model.Name}}.UUID] = {{.Model.Name}}
		}
	
		if err != nil {

			uuidsToSelect = append(uuidsToSelect, uuid)
		}
	}

	

	if len(uuidsToSelect) > 0 {
		{{.Model.Name}}sSelected, err :=  d.Select(
			[]*libdata.Filter{ {
				Field: "uuid", Operator: "IN", Operande: uuidsToSelect,
			} }, nil, nil, nil)
		if err != nil {
			return nil, err
		}

		for _, {{.Model.Name}} := range {{.Model.Name}}sSelected.Slice() {
			{{.Model.Name}}s[{{.Model.Name}}.UUID] = {{.Model.Name}}
		}
	}

	var {{.Model.Name}}sSlice []libdata.ModelInterface
	for _, uuid := range uuids {
		if {{.Model.Name}}s[uuid] != nil {
			{{.Model.Name}}sSlice = append({{.Model.Name}}sSlice, {{.Model.Name}}s[uuid])
		}
	}

	result := d.NewCollection(&liborm.Collection{})
	result.Init({{.Model.Name}}sSlice)

	return result, nil
}

// Select return the {{.Model.Name}} objects from the pool, by the filters.
func (d *{{.Model.Title}}Pool) Select(
	filters []*libdata.Filter,
	limit *uint, after *string, orderByArg *string,
) (*{{.Model.Title}}Collection, error) {
	span, _ := opentracing.StartSpanFromContext(d.Env.Context, "orm-{{.Model.Name}}-select")
	defer span.Finish()
	d.Env.Context = opentracing.ContextWithSpan(d.Env.Context, span)

	var records *liborm.Collection
	if len(filters) == 0 && limit == nil && after == nil && orderByArg == nil {
		var err error
		var {{.Model.Name}}sByte []byte
		key := fmt.Sprintf(
			"%s_{{.Model.Title}}sUUID_all",
			d.Pool.Env.Transaction.Tenant.ID,
		)
		if d.Pool.Env.Transaction.UseCache {
			
			{{.Model.Name}}sByte, err = d.Env.Transaction.Cluster.CacheDriver.GetCache(key)
		} else {
			err = errors.New("Not using cache")
		}

		if err == nil {
			{{.Model.Name}}s, errS := d.SelectByUUID(strings.Split(string({{.Model.Name}}sByte), ","))
			if errS != nil {
				return nil, errS
			}

			return {{.Model.Name}}s, nil
		}

		if err != nil {
			var err error
			records, err = d.Pool.Select(filters, []string{}, limit, after, orderByArg)
			if err != nil {
				return nil, errors.Wrap(err, "Couldn't get {{.Model.Name}}s")
			}

			var uuids []string
			for _, record := range records.Slice() {
				uuids = append(uuids, record.(*{{.Model.Title}}).UUID)
			}
			err = d.Env.Transaction.Cluster.CacheDriver.SetCache(key, []byte(strings.Join(uuids, ",")))
			if err != nil {
				return nil, errors.Wrap(err, "Couldn't set {{.Model.Name}} in cache")
			}

		}
		
	} else {
		var err error
		records, err = d.Pool.Select(filters, []string{}, limit, after, orderByArg)
		if err != nil {
			return nil, errors.Wrap(err, "Couldn't get {{.Model.Name}}s")
		}
	}

	return d.NewCollection(records), nil
}

// Create create a new {{.Model.Name}} from the pool, and return it.
func (d *{{.Model.Title}}Pool) Create(
	{{.Model.Name}}s []*pb.Create{{.Model.Title}}Request,
) (*{{.Model.Title}}Collection, error) {
	span, _ := opentracing.StartSpanFromContext(d.Env.Context, "orm-{{.Model.Name}}-create")
	defer span.Finish()
	d.Env.Context = opentracing.ContextWithSpan(d.Env.Context, span)

	if d.PreCreate != nil {
		err := d.PreCreate({{.Model.Name}}s)
		if err != nil {
			return nil, errors.Wrap(err, "Couldn't execute precreate")
		}
	}

	var records []libdata.ModelInterface
	for _, {{.Model.Name}} := range {{.Model.Name}}s {
		{{if not .Model.CanAssignID}}
		{{.Model.Name}}.{{.Model.Title}}.UUID = uuid.New().String()
		{{end}}
		records = append(records, {{.Model.Name}}.{{.Model.Title}})
	}

	recordsCollection, err := d.Pool.Create(records)
	if err != nil {
		return nil, errors.Wrap(err, "Couldn't create records")
	}

	{{- range .Model.NestedFields}}

	var {{.Name}} []*pb.Create{{.GetReference.Title}}Request
	for _, {{$.Model.Name}} := range {{$.Model.Name}}s {
		fmt.Println("UUID: ", {{$.Model.Name}}.{{$.Model.Title}}.UUID)
		if {{$.Model.Name}}.{{.Title}} != nil {
			for _, nested := range {{$.Model.Name}}.{{.Title}}.Create {
				{{ if eq .Type "One2manyType" }}
				nested.{{.GetReference.Title}}.{{.GetInverseField}} = &pb.{{$.Model.Title}}{UUID: {{$.Model.Name}}.{{$.Model.Title}}.UUID}
				{{else}}
				nested.{{.GetReference.Title}}.{{.GetInverseField}} = {{$.Model.Name}}.{{$.Model.Title}}.UUID
				{{end}}

				{{.Name}} = append({{.Name}}, nested)
			}
		}
	}
	if len({{.Name}}) > 0{
		_, err := {{.GetReference.Title}}Definition.NewPool(d.Pool.Env).Create({{.Name}})
		if err != nil {
			return nil, errors.Wrap(err, "Couldn't create records")
		}
	}
	{{- end}}

	{{.Model.Name}}sCollection := d.NewCollection(recordsCollection)

	{{if not .Model.ExternalDriver}}
	for _, {{.Model.Name}} := range {{.Model.Name}}sCollection.Slice() {
		json, errE := {{.Model.Name}}.JSON()
		if errE != nil {
			return nil, errE
		}
		errE = d.Env.Transaction.RegisterEvent("{{.Model.Name}}", {{.Model.Name}}.{{.Model.Title}}.UUID, "created", json)
		if errE != nil {
			return nil, errE
		}
	}
	{{end}}

	return {{.Model.Name}}sCollection, nil
}

// Upsert create or update a {{.Model.Name}} from the pool, and return it.
func (d *{{.Model.Title}}Pool) Upsert(
	where []*libdata.Filter,
	{{.Model.Name}} *{{.Model.Title}},
	updateMask bool,
) (*{{.Model.Title}}, bool, error) {
	span, _ := opentracing.StartSpanFromContext(d.Env.Context, "orm-{{.Model.Name}}-upsert")
	defer span.Finish()
	d.Env.Context = opentracing.ContextWithSpan(d.Env.Context, span)

	record, wasCreated, err := d.Pool.Upsert(where, {{.Model.Name}}, updateMask)
	if err != nil {
		return nil, false, errors.Wrap(err, "Couldn't create records")
	}

	return record.(*{{.Model.Title}}), wasCreated, nil
}

var dummy sql.DB
var dummy2 sqlx.DB
var dummy3 ptypes.Timestamp


`))

// nolint:lll
var dataTemplate = template.Must(template.New("").Parse(`// Code generated by go generate; DO NOT EDIT.
// This file was generated by robots at
// {{ .Timestamp }}

package orm

import (
	"fmt"
	"encoding/json"
	"strings"

	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/liborm"
	fieldmask "gitlab.com/empowerlab/stack/lib-go/ptypes/fieldmask"

	"{{.Repository}}/gen/orm/pb"
)

/*JsonBlob contain the detail of the module to import.
 */
type JsonBlob struct {
	Module     string      ` + "`" + `json:"module"` + "`" + `
	Data  []*JsonData ` + "`" + `json:"data"` + "`" + `
}

// JsonData contains the data to import.
type JsonData struct {
	ID     string      ` + "`" + `json:"id"` + "`" + `
	Model  string      ` + "`" + `json:"model"` + "`" + `
	Fields map[string]interface{} ` + "`" + `json:"fields"` + "`" + `
}

// UnmarshalJSON will unmarshal the data into the JsonData struct, 
// in order to use it later.
func (j *JsonData) UnmarshalJSON(data []byte) error {
	dataMap := map[string]interface{}{}
	err := json.Unmarshal(data, &dataMap)
	if err != nil {
		fmt.Println("err", err)
	}
	fmt.Println(dataMap)
	j.ID = dataMap["id"].(string)
	j.Model = dataMap["model"].(string)

	switch j.Model {
	{{- range .Models }}
	case "{{.Name}}":
		fmt.Println("{{.Name}}")
		{{.Name}} := &pb.{{.Title}}{}
		err = mapstructure.Decode(dataMap["fields"], {{.Name}})
		if err != nil {
			fmt.Println("err", err)
		}
		{{- range .NestedFields }}
		var {{.Name}} []*pb.{{.GetReference.Title}}
		if _, ok := dataMap["fields"].(map[string]interface{})["{{.Name}}"]; ok {
			//nolint: lll
			for _, subrecordData := range dataMap["fields"].(map[string]interface{})["{{.Name}}"].([]interface{}) {
				subrecord := &pb.{{.GetReference.Title}}{
					ExternalID: subrecordData.(map[string]interface{})["id"].(string),
				}
				fmt.Printf("subrecordData %+v\n", subrecordData)
				err = mapstructure.Decode(subrecordData.(map[string]interface{})["fields"], subrecord)
				if err != nil {
					fmt.Println("err", err)
				}
				{{.Name}} = append({{.Name}}, subrecord)
			}
		}
		{{- end }}
		j.Fields = map[string]interface{}{
			"{{.Name}}": &{{.Title}}{
				{{.Title}}: {{.Name}},
			},
			{{- range .NestedFields }}
			"{{.Name}}": {{.Name}},
			{{- end }}
		}
	{{- end }}
	
	}

	return nil
  }

type dataDefinition struct{}

/*LoadData is a function which can be called to import data.
 *It take a json as argument, and must be in a specific format.
 */
func (d *dataDefinition) LoadData(env *liborm.Env, jsonBlob []byte) error {

	fmt.Println("LoadDataTest!!")
	_ = strings.Split("test", ",")

	jsonBlob = []byte(` + "`" + `
		{
			"module": "init",
			"data": [
				{
					"id": "customer_test",
					"model": "customer", 
					"fields": {
						"name": "test",
						"street": "1 lol street",
						"zip": "1337",
						"city": "nowhere",
					}
				},


				{
					"id": "invoice_test",
					"model": "invoice", 
					"fields": {
						"number": "123",
						"customerUUID": "init.customer_test",
						"lines": [
							{
								"id": "invoice_line_test",
								"fields": {
									"name": "Test",
									"quantity": "1",
									"price_unit": "10",
								}
							}
						],
					}
				},
	
			]
		}
	` + "`" + `)



	var datas JsonBlob
	err := json.Unmarshal(jsonBlob, &datas)
	if err != nil {
		fmt.Printf("err %s", err)
		return err
	}
	fmt.Printf("datas %+v\n", datas)
	for _, data := range datas.Data {
		fmt.Printf("datas %+v\n", data)
	}

	{{- range .Models }}
	{{.Name}}Pool := {{.Title}}Definition.NewPool(env)
	{{.Name}}CurrentDatas, err := {{.Name}}Pool.Select([]*libdata.Filter{
		{Field: "external_module", Operator: "=", Operande: datas.Module},
	}, nil, nil, nil)
	if err != nil {
		return errors.Wrap(err, "Can't")
	}

	{{.Name}}ToDelete := map[string]*{{.Title}}{}
	{{.Name}}CurrentDatasByID := map[string]*{{.Title}}{}
	for _, data := range {{.Name}}CurrentDatas.Slice() {
		{{.Name}}CurrentDatasByID[data.ExternalID] = data
		{{.Name}}ToDelete[data.UUID] = data
	}
	{{- end }}

	for _, data := range datas.Data {

		switch data.Model {
		{{- range .Models }}
		{{if ne .Name "data"}}
		case "{{.Name}}":

			fmt.Println(data.Model)
			{{.Name}} := data.Fields["{{.Name}}"].(*{{.Title}})
			{{.Name}}.Env = env
			{{.Name}}.{{.Title}}.ExternalModule = datas.Module
			{{.Name}}.{{.Title}}.ExternalID = data.ID
			{{.Name}}.convertMany2one()

			{{- range .NestedFields }}
			fmt.Printf("{{.Name}} %+v\n", data.Fields["{{.Name}}"])
			{{.Name}} := data.Fields["{{.Name}}"].([]*pb.{{.GetReference.Title}})
			for _, nested := range {{.Name}} {
				nested.ExternalModule = datas.Module
				wrapper := {{.GetReference.Title}}{
					{{.GetReference.Title}}: nested,
					Env: env,
				}
				wrapper.convertMany2one()
			}
			{{- end }}


			{{.Name}}s := &{{.Title}}Collection{
				Collection: &liborm.Collection{},
			}
			// {{.Name}}s.Init([]interface{}{})
			if _, ok := {{.Name}}CurrentDatasByID[data.ID]; ok {
				// {{.Name}}s, err = {{.Name}}Pool.Select(
				// 	[]*libdata.Filter{
				// 		{Field: "externalModule", Operator: "=", Operande: datas.Module},
				// 		{Field: "externalID", Operator: "=", Operande: data.ID},
				// 	}, nil, nil, nil)
				// if err != nil {
				// 	return err
				// }
				{{.Name}}s.Init([]libdata.ModelInterface{ {{.Name}}CurrentDatasByID[data.ID]})
				err = {{.Name}}s.Update(&pb.Update{{.Title}}Request{
					{{.Title}}: {{.Name}}.{{.Title}}, 
					UpdateMask: &fieldmask.FieldMask{},
				})
				if err != nil {
					return err
				}	
			// }
			// if len({{.Name}}s.Slice()) > 0 {
			// 	err = {{.Name}}s.Update({{.Name}}, true)
			// 	if err != nil {
			// 		return err
			// 	}	
			} else {
				fmt.Println("create")
				{{- range .NestedFields }}
				var {{.Name}}Create []*pb.Create{{.GetReference.Title}}Request
				for _, nested := range {{.Name}} {
					{{.Name}}Create = append({{.Name}}Create, &pb.Create{{.GetReference.Title}}Request{
						{{.GetReference.Title}}: nested,
					})
				}
				{{- end }}
				{{.Name}}s, err = {{.Name}}Pool.Create([]*pb.Create{{.Title}}Request{ {
					{{.Title}}: {{.Name}}.{{.Title}},
					{{- range .NestedFields }}
					{{.Title}}: &pb.Create{{.GetReference.Title}}ManyRequest{
						Create: {{.Name}}Create,
					},
					{{- end }}
				 } })
				if err != nil {
					fmt.Println(err)
					return err
				}
			}

			for _, {{.Name}} := range {{.Name}}s.Slice() {
				if _, ok := {{.Name}}ToDelete[{{.Name}}.UUID]; ok {
					delete({{.Name}}ToDelete, {{.Name}}.UUID)
				}
			}
		{{end}}
		{{- end }}
		}

	}

	{{- range .Models }}
	for _, {{.Name}} := range  {{.Name}}ToDelete{
		fmt.Printf("%+v\n", {{.Name}}.Env)
		err = {{.Name}}.Delete()
		if err != nil {
			return err
		}
	}
	{{- end }}

	return nil
}

{{- range .Models }}
func (record *{{.Title}}) convertMany2one() error {
	{{- range .Fields }}
	{{ if eq .Type "Many2oneType"}}
	if record.{{.Title}}.UUID != "" {
		{{.NameWithoutUUID}}Ref := strings.Split(record.{{.Title}}.UUID, ".")
		fmt.Println({{.NameWithoutUUID}}Ref)
		{{.NameWithoutUUID}}s, err := {{.GetReference.Title}}Definition.NewPool(record.Env).Select(
			[]*libdata.Filter{
				{Field: "external_module", Operator: "=", Operande: {{.NameWithoutUUID}}Ref[0]},
				{Field: "external_id", Operator: "=", Operande: {{.NameWithoutUUID}}Ref[1] },
			}, nil, nil, nil)
		if err != nil {
			return err
		}
		if len({{.NameWithoutUUID}}s.Slice()) > 0 {
			for _, {{.NameWithoutUUID}} := range {{.NameWithoutUUID}}s.Slice() {
				record.{{.Title}} = &pb.{{.GetReference.Title}}{UUID: {{.NameWithoutUUID}}.UUID}
			}
		} else {
			fmt.Println(record.{{.Title}}, "doesn't exist")
			return errors.New("UUID doesn't exist")
		}
	}
	{{end}}
	{{- end }}
	return nil
}
{{- end }}



`))

// nolint:lll
var initTemplate = template.Must(template.New("").Parse(`// Code generated by go generate; DO NOT EDIT.
// This file was generated by robots at
// {{ .Timestamp }}

package orm

import (
	// keto "github.com/ory/keto/sdk/go/keto/client/engines"
)


// var KetoClient *keto.Client


`))
