package liborm

import (
	"gitlab.com/empowerlab/stack/lib-go/libdata"
	"gitlab.com/empowerlab/stack/lib-go/libdata/fields"
)

// DataModel is a special definition to import data into the service.
var DataModel *Definition

func init() {
	DataModel = &Definition{
		Model: &libdata.ModelDefinition{
			Name: "data",
			Fields: []libdata.Field{
				&fields.Text{Name: "model", String: "Model", Required: true},
				&fields.ID{Name: "recordUUID", String: "Record UUID", Required: true},
				&fields.Text{Name: "xmlid", String: "ID", Required: true},
				&fields.Text{Name: "module", String: "Module", Required: true},
			},
			Datetime: true,
		},
		CustomFuncsCollection: []*CustomFunc{{
			Name: "loadData",
			Args: []libdata.Field{&fields.JSON{
				Name:     "json",
				Required: true,
			}},
		}},
	}

}
