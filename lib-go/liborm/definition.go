package liborm

import (
	"fmt"
	"strings"

	"gitlab.com/empowerlab/stack/lib-go/libdata"
)

var Cluster *libdata.Cluster

type Definitions struct {
	Repository string
	slice      []*Definition
	byIds      map[string]*Definition
}

// Register is used to register a new definition into the service.
func (ds *Definitions) Register(d *Definition) {

	if ds.byIds == nil {
		ds.byIds = map[string]*Definition{}
	}

	for _, f := range d.Model.Fields {
		for _, old := range ds.Slice() {
			if f.GetReferenceName() == old.Model.Name {
				f.SetReference(old.Model)
			}
		}
	}

	ds.slice = append(ds.slice, d)
	ds.byIds[d.Model.Name] = d

	for _, old := range ds.Slice() {
		for _, f := range old.Model.Fields {
			if f.GetReferenceName() == d.Model.Name {
				f.SetReference(ds.GetByID(d.Model.Name).Model)
			}

		}
	}
}

// Slice return the definitions as a slice.
func (ds *Definitions) Slice() []*Definition {
	return ds.slice
}

// GetByID return the specified definition by its ID.
func (ds *Definitions) GetByID(id string) *Definition {
	d := ds.byIds[id]
	if d == nil {
		panic(fmt.Sprintf("The model definition %s doesn't exist", id))
	}
	return d
}

// Definition is used to declare the information of a model, so it can generate its code.
type Definition struct {
	Model                 *libdata.ModelDefinition
	Tree                  bool
	TranslamodelFields    []string
	TranslateModel        *Definition
	CustomFuncsPool       []*CustomFunc
	CustomFuncsCollection []*CustomFunc
	new                   func() interface{}
}

// GetCustomFuncsData return the custom function information for collections and pools,
// in a format usable by templates.
// func (d *Definition) GetCustomFuncsData() ([]*CustomFuncData, []*CustomFuncData) {
// 	var resultPool []*CustomFuncData
// 	for _, c := range d.CustomFuncsPool {
// 		poolData := c.GetCustomFuncData()
// 		poolData.Model = strings.Title(d.Model.Name)
// 		resultPool = append(resultPool, poolData)
// 	}
// 	var resultCollection []*CustomFuncData
// 	for _, c := range d.CustomFuncsCollection {
// 		collectionData := c.GetCustomFuncData()
// 		collectionData.Model = strings.Title(d.Model.Name)
// 		resultCollection = append(resultCollection, collectionData)
// 	}
// 	return resultPool, resultCollection
// }

// Arg represent the argument of a custom function.
// type Arg struct {
// 	Name     string
// 	Type     libdata.FieldType
// 	Required bool
// }

// func (a *Arg) Title() string {
// 	return strings.Title(a.Name)
// }

// CustomFunc represent a custom function.
type CustomFunc struct {
	Name    string
	Args    []libdata.Field
	Results []libdata.Field
}

func (f *CustomFunc) Title() string {
	return strings.Title(f.Name)
}

// // GetCustomFuncData return the custom function information, in a format usable by templates.
// func (c *CustomFunc) GetCustomFuncData() *CustomFuncData {

// 	var args []*ArgData
// 	for _, arg := range c.Args {
// 		args = append(args, &ArgData{
// 			Name:  arg.Name,
// 			Title: strings.Title(arg.Name),
// 			// Type:     arg.Type.GoType(),
// 			Required: arg.Required,
// 			Arg:      arg,
// 		})
// 	}

// 	var results []*ArgData
// 	for _, result := range c.Results {
// 		results = append(results, &ArgData{
// 			Name:  result.Name,
// 			Title: strings.Title(result.Name),
// 			// Type:     result.Type.GoType(),
// 			Required: result.Required,
// 			Arg:      result,
// 		})
// 	}

// 	return &CustomFuncData{
// 		Name:    c.Name,
// 		Title:   strings.Title(c.Name),
// 		Args:    args,
// 		Results: results,
// 	}
// }

// ArgData represent the argument of a custom function, in a format usable by templates.
// type ArgData struct {
// 	Name     string
// 	Title    string
// 	Type     string
// 	Required bool
// 	Arg      *Arg
// }

// // CustomFuncData represent a custom function, in a format usable by templates.
// type CustomFuncData struct {
// 	Name    string
// 	Title   string
// 	Model   string
// 	Args    []*ArgData
// 	Results []*ArgData
// }

// SetStore is used to set the store of the model.
func (d *Definition) SetStore(store interface{}) error {
	d.Model.Store = store
	return nil
}

// SetNew is used to set the new function.
func (d *Definition) SetNew(new func() interface{}) error {
	d.new = new
	return nil
}

// GetIdsFromRecordList will return the uuid from a list of records.
func GetIdsFromRecordList(records []libdata.ModelInterface) []string {
	var recordIds []string
	for _, record := range records {
		recordIds = append(recordIds, record.(libdata.ModelInterface).GetUUID())
	}
	return recordIds
}
