/**
 * Copyright (c) 2017-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

const React = require('react');

const CompLibrary = require('../../core/CompLibrary.js');

class HomeSplash extends React.Component {
  render() {
    const {siteConfig, language = ''} = this.props;
    const {baseUrl, docsUrl} = siteConfig;
    const docsPart = `${docsUrl ? `${docsUrl}/` : ''}`;
    const langPart = `${language ? `${language}/` : ''}`;
    const docUrl = doc => `${baseUrl}${docsPart}${langPart}${doc}`;

    const heroStyle = {
      backgroundColor: '#1d144d',
      height: '100%',
      width: '100%',
      minHeight: '480px',

    }

    const SplashContainer = props => (
      <div className="homeContainer">
        <div className="homeSplashFade" style={heroStyle}>
          <div className="wrapper homeWrapper" >{props.children}</div>
        </div>
      </div>
    );

    const Logo = props => (
      <div className="projectLogo">
        <img src={props.img_src} alt="Project Logo" />
      </div>
    );


    const titleStyle = {
      color: 'white',
      textAlign: "left",
      maxWidth: "600px",
    }

    const ProjectTitle = () => (
      <h2 className="projectTitle" style={titleStyle}>
        {siteConfig.tagline}
        <small>{siteConfig.tagline2}</small>
      </h2>
    );

    const pluginRowStyle = {
      justifyContent: "left",
    }

    const PromoSection = props => (
      <div className="section promoSection">
        <div className="promoRow">
          <div className="pluginRowBlock" style={pluginRowStyle}>{props.children}</div>
        </div>
      </div>
    );

    const Button = props => (
      <div className={props.addClass}>
        <a className="button" href={props.href} target={props.target}>
          {props.children}
        </a>
      </div>
    );

    // use svg for the logo
    return (
      <SplashContainer>
        <Logo img_src={`${baseUrl}img/hero-logo.jpg`} />
        <div className="inner">
          <ProjectTitle siteConfig={siteConfig} />
          <PromoSection>
            <Button href="https://www.freelists.org/list/empower-stack" target="_blank" addClass="pluginWrapper buttonWrapper subscribe">Join the community</Button>
            <Button href="/docs/architecture" addClass="pluginWrapper buttonWrapper">Docs</Button>
          </PromoSection>
        </div>
      </SplashContainer>
    );
  }
}

class Index extends React.Component {
  render() {
    const {config: siteConfig, language = ''} = this.props;
    const {baseUrl} = siteConfig;

    const descriptionSection = {
      backgroundColor: "#f2f2f2",
    }

    const descriptionStyle = {
      textAlign: "center",
      fontFamily: "Lato",
      fontWeight: "400",
      fontSize: "20",
      margin: "0 auto 60px",
      lineHeight: "1.5em",
      maxWidth: "640px",
    }

    const Description = () => {
      return (
        <div style={descriptionSection}>
          <div style={descriptionStyle}>
            <h3 className="heading-2">Microservices</h3>
            <p>
            Centralizing all the processes of your organization into a single software considerably hamper the flexibility of the said organization.
            <br/><br/>
            To scale, you need to break this monolith into smaller services. But it's difficult, and many organizations are struggling to make the transition.
            <br/><br/>
            We aim to build a framework which natively support <a href="https://microservices.io/" target="_blank">all microservices patterns</a>, also capable of supporting the usual web and ERP usecases (currently supported by frameworks such as Ruby on Rails or Odoo). We chose Golang, React and Kubernetes as the core of this stack.
            </p>
          </div>
        </div>
      );
    };

    const keyPointsSection = {
      backgroundColor: "#f2f2f2"
    }
    const clear = {
      clear: "both",
      paddingTop: "40px",
      color: "#646464",
    }
    const headerWithIconMentee = {
      backgroundImage: `url("${baseUrl}img/icon-find-mentee.svg")`
    }
    const headerWithIconFlexibility = {
      backgroundImage: `url("${baseUrl}img/icon-flexibility.svg")`
    }
    const headerWithIconWork = {
      backgroundImage: `url("${baseUrl}img/icon-work.svg")`
    }
    const keyPointsPStyle = {
      fontFamily: "Lato",
      fontWeight: "400",
      fontSize: "16",
      margin: "0 auto",
      lineHeight: "1.5em",
      color: "#646464",
      display: "block"
    }
    const dividerStyle = {
      maxWidth: "960px",
      margin: "0 auto 60px",
      height: "1px",
      backgroundColor: "#DDD",
    }


    const KeyPoints = () => {
      return (
        <div className="section cc-store-home-wrap" style={keyPointsSection}>
          <div style={dividerStyle}/>
          <div className="section-3">
            <div className="w-row">
              <div className="w-col w-col-4">
                <h3 className="heading-2 text-icon" style={headerWithIconFlexibility}>Slow transition</h3>
                <p style={keyPointsPStyle}>
                  We provide all the needed libraries to build services, to make the most of your microservices architecture.
                  <br/><br/>
                  They can not only easily communicate with each other, theses libraries are also designed to get data from your legacy systems so you can take your time moving your process out of them.‍
                </p>
              </div>
              <div className="w-col w-col-4">
                <h3 className="heading-3 text-icon" style={headerWithIconWork}>Opinionated</h3>
                <p style={keyPointsPStyle}>
                  We rigorously studied the available technologies to select for each domain, not only those who are powerful but also are compatible with any architecture choice you may have.
                  <br/><br/>
                  We provide an evolving stack. The components will change depending on the trends and the decisions of our community.
                </p>
              </div>
              <div className="w-col w-col-4">
                <h3 className="heading-4 text-icon" style={headerWithIconMentee}>From 1 to 1000s</h3>
                <p style={keyPointsPStyle}>
                  Being an accessible, turnkey solution, the stack allows you to start using microservices at the early stage of your project, even if you're alone.
                  <br/><br/>
                  But it also provides all the tools to divide your services into several teams, and face the challenges big organizations may have to manage them.
                </p>
              </div>
            </div>
          </div>
          <div style={descriptionSection} style={clear}>
            <div style={descriptionStyle}>
              <p>
                Last but not least, the Empower Stack is free to use, being entirely open-source.
                <br/>
                <small>And so are the technologies we use under the hood.</small>
              </p>
            </div>
          </div>
        </div>
      );
    };

    const localStyle = {
      paddingLeft: "40px",
      paddingRight: "40px",
      backgroundColor: '#282828',
      minHeight: "400px",
      
      color: "#fff",
    }

    const white = {
      color: "#fff",
    }


    const kubernetesStyle = {
      paddingLeft: "40px",
      paddingRight: "40px",
      backgroundColor: "#2A1D4C",
      color: "#fff",
      minHeight: "400px",
    }

    const codeStyle = {
      backgroundColor: "#eff0f1",
      color: "#393318",
      padding: "12px 8px",
      fontSize: "12px",
    }

    const gitlabStyle = {
      width: "20px",
    }

    const Installation = () => {
      return (
        <div>
          <div className="column-3 w-col w-col-6" style={localStyle}>
            <h3>Local installation</h3>
            <p>
              <u><a style={white} href="/docs/local">Get started in a couple of minutes with Docker compose</a></u>
              <br/>
              <div style={codeStyle}>
                wget https://gitlab.com/empowerlab/example/raw/master/docker-compose.yml
                <br/>
                docker-compose up
              </div>
              <br/>
              <table className="localTable">
                <tr>
                  <td>Admin GUI:</td>
                  <td>http://localhost:5000</td>
                </tr>
                <tr>
                  <td>API:</td>
                  <td>http://localhost:5001</td>
                </tr>
                <tr>
                  <td>Login:</td>
                  <td>http://localhost:5003</td>
                </tr>
                <tr>
                  <td>Jaeger:</td>
                  <td>http://localhost:5004</td>
                </tr>
              </table>
            </p>
          </div>
          <div className="column-2 w-col w-col-6" style={kubernetesStyle}>
            <h3>Kubernetes</h3>
            <p>
              Start your own architecture by forking our exemplary project on Gitlab.
              <br/>
              <br/>
              <a className="button gitlabButton" href="https://gitlab.com/empowerlab/example/" target="_blank"><img style={gitlabStyle} src={`${baseUrl}img/gitlab-icon.png`}/>  Fork me on Gitlab</a>
              <br/>
              <br/>
              <u><a style={white} href="docs/kubernetes">Follow the documentation</a></u> to get your DevOps process ready and deploy your Kubernetes cluster.
            </p>
          </div>
        </div>
      );
    };

    const containerStackStyle = {
      width: "100%",
      paddingRight: "15px",
      paddingLeft: "15px",
      marginRight: "auto",
      marginLeft: "auto",
      textAlign: "center",
    }

    const rowStackStyle = {
      display: "flex",
      flexWrap: "wrap",
    }


    const Stack = () => {
      return (
        <section id="stack" className="g-py-100" style={clear}>
          <div className="container" style={containerStackStyle}>
            <div className="text-center g-color-black g-mb-50">
              <h2 className="h4">Built on the shoulders of giants</h2>
            </div>
        
            <div className="g-overflow-hidden">
              <div className="row text-center mx-0 g-ml-minus-1 g-mb-minus-1" style={rowStackStyle}>
                <div className="col-md-3 px-0">
                  <div className="img-stack-div">
                    <img className="img-stack" src="./img/stack/golang.png" alt="Image Description"/>
                  </div>
                </div>
        
                <div className="col-md-3 px-0">
                  <div className="img-stack-div">
                    <img className="img-stack" src="./img/stack/react.png" alt="Image Description"/>
                  </div>
                </div>
        
                <div className="col-md-3 px-0">
                  <div className="img-stack-div">
                    <img className="img-stack" src="./img/stack/graphql.png" alt="Image Description"/>
                  </div>
                </div>
        
                <div className="col-md-3 px-0">
                  <div className="img-stack-div">
                    <img className="img-stack" src="./img/stack/kubernetes.png" alt="Image Description"/>
                  </div>
                </div>
        
                <div className="col-md-3 px-0">
                  <div className="img-stack-div">
                    <img className="img-stack" src="./img/stack/cockroachdb.png" alt="Image Description"/>
                  </div>
                </div>
        
                <div className="col-md-3 px-0">
                  <div className="img-stack-div">
                    <img className="img-stack" src="./img/stack/gitlab.png" alt="Image Description"/>
                  </div>
                </div>
        
                <div className="col-md-3 px-0">
                  <div className="img-stack-div">
                    <img className="img-stack" src="./img/stack/grpc.png" alt="Image Description"/>
                  </div>
                </div>
        
                <div className="col-md-3 px-0">
                  <div className="img-stack-div">
                    <img className="img-stack" src="./img/stack/postgresql.png" alt="Image Description"/>
                  </div>
                </div>

                <div className="col-md-3 px-0">
                  <div className="img-stack-div">
                      <img className="img-stack" src="./img/stack/hydra.png" alt="Image Description"/>
                    </div>
                  </div>
          
                  <div className="col-md-3 px-0">
                    <div className="img-stack-div">
                      <img className="img-stack" src="./img/stack/istio.png" alt="Image Description"/>
                    </div>
                  </div>
          
                  <div className="col-md-3 px-0">
                    <div className="img-stack-div">
                      <img className="img-stack" src="./img/stack/jaeger.png" alt="Image Description"/>
                    </div>
                  </div>
          
                  <div className="col-md-3 px-0">
                    <div className="img-stack-div">
                      <img className="img-stack" src="./img/stack/prometheus.png" alt="Image Description"/>
                    </div>
                  </div>

                  <div className="col-md-3 px-0">
                    <div className="img-stack-div">
                      <img className="img-stack" src="./img/stack/terraform.png" alt="Image Description"/>
                    </div>
                  </div>
          
                  <div className="col-md-3 px-0">
                    <div className="img-stack-div">
                      <img className="img-stack" src="./img/stack/nats.png" alt="Image Description"/>
                    </div>
                  </div>
          
                  <div className="col-md-3 px-0">
                    <div className="img-stack-div">
                      <img className="img-stack" src="./img/stack/pact.jpg" alt="Image Description"/>
                    </div>
                  </div>
          
                  <div className="col-md-3 px-0">
                    <div className="img-stack-div">
                      <img className="img-stack" src="./img/stack/vault.png" alt="Image Description"/>
                    </div>
                  </div>
              </div>
            </div>
          </div>
        </section>        
      );
    };
    

    const joinSection = {
      backgroundColor: "#f2f2f2",
      padding: "40px",
    }

    const joinStyle = {
      textAlign: "center",
      fontFamily: "Lato",
      fontWeight: "400",
      fontSize: "20",
      margin: "0 auto 60px",
      lineHeight: "1.5em",
      maxWidth: "640px",
    }

    const JoinUs = () => {
      return (
        <section id="joinus" style={joinSection}>
          <div className="g-pos-rel" style={joinStyle}>
            <div className="container text-center g-pt-100 g-pb-50">
              <div className="g-max-width-645 mx-auto g-mb-40">
                <h2 className="h1 mb-3">Want to know more?</h2>
                <h3 className="h3 mb-3">Read our articles on Medium</h3>
                <p><a className="button u-shadow-v33 g-color-white g-bg-primary g-bg-main--hover g-rounded-30 g-px-35 g-py-13" href="https://medium.com/@yannick.b/empower-stack-the-demonstration-cd34af05da48?source=friends_link&sk=644e66f18ba479d6b59948cfde7b3066" target="_blank">Empower Stack: The demonstration</a></p>
                <p><a className="button u-shadow-v33 g-color-white g-bg-primary g-bg-main--hover g-rounded-30 g-px-35 g-py-13" href="https://medium.com/@yannick.b/how-should-be-built-the-ideal-cloud-native-framework-part-1-7e922086f34b?source=friends_link&sk=24750def5b939713c89e3219ee582209" target="_blank">How should be built the ideal cloud-native framework (part 1)</a></p>
                <p><a className="button u-shadow-v33 g-color-white g-bg-primary g-bg-main--hover g-rounded-30 g-px-35 g-py-13" href="https://medium.com/@yannick.b/how-should-be-built-the-ideal-cloud-native-framework-part-2-2fcfa38c9de7?source=friends_link&sk=748f4555cde159609196c62cc261bee8" target="_blank">How should be built the ideal cloud-native framework (part 2)</a></p>
                <p><a className="button u-shadow-v33 g-color-white g-bg-primary g-bg-main--hover g-rounded-30 g-px-35 g-py-13" href="https://medium.com/@yannick.b/how-should-be-designed-the-ideal-golang-crud-backend-36c8f874c6a7?source=friends_link&sk=68ff2ff4eca01005b212723a38fe45c7" target="_blank">How should be designed the ideal Golang CRUD backend ?</a></p>
                <p>
                  The Empower Stack is only backed by a community of passionate developers, seeking to share their knowledge with others and building the tools of their dreams. There is no editor.
                  <br/>
                  <br/>
                  <a className="button subscribeButton u-shadow-v33 g-color-white g-bg-primary g-bg-main--hover g-rounded-30 g-px-35 g-py-13" href="docs/roadmap" target="_blank">See the Roadmap</a>
                  <br/>
                  <br/>
                  The Empower Stack is still in the early-alpha stage, not yet usable in production. If you want to stay tuned about our progress, join the community by subscribing to the mailing list, or contact us if you're willing to help.
                  </p>

              </div>
          
              <a className="button subscribeButton u-shadow-v33 g-color-white g-bg-primary g-bg-main--hover g-rounded-30 g-px-35 g-py-13" href="https://www.freelists.org/list/empower-stack" target="_blank">Join the mailing list</a>
              <br/>
              &nbsp;&nbsp;&nbsp;or&nbsp;&nbsp;&nbsp;
              <br/>
          
              <a className="button u-shadow-v32 g-color-primary g-color-white--hover g-bg-white g-bg-main--hover g-rounded-30 g-px-35 g-py-13" href="mailto:contact@empower.sh">Contact Us</a>
            </div>
          </div>
        </section>
      );
    };


    return (
      <div>
        <HomeSplash siteConfig={siteConfig} language={language} />
        <div className="mainContainer mainContainerHome">
          <Description />
          <KeyPoints />
        </div>
          <Installation />
          <Stack />
          <JoinUs />
      </div>
    );
  }
}

module.exports = Index;
