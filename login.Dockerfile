FROM golang:1.13-alpine AS build

# WORKDIR /go/src/app
# COPY . .

# RUN apt update && apt install unzip
# RUN wget https://github.com/protocolbuffers/protobuf/releases/download/v3.9.0/protoc-3.9.0-linux-x86_64.zip -O /tmp/protoc.zip
# RUN unzip /tmp/protoc.zip -d /tmp/protoc
# RUN mv /tmp/protoc/bin/protoc /usr/bin/protoc
# RUN wget https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/v0.3.0/grpc_health_probe-linux-amd64 -O /usr/bin/grpc_health_probe
# RUN chmod +x /usr/bin/grpc_health_probe
# RUN mkdir -p $GOPATH/src/gitlab.com/empowerlab/stack
# RUN cp -R ./* $GOPATH/src/gitlab.com/empowerlab/stack

RUN apk add git make protobuf

RUN wget https://github.com/grpc-ecosystem/grpc-health-probe/releases/download/v0.3.0/grpc_health_probe-linux-amd64 -O /usr/bin/grpc_health_probe
RUN chmod +x /usr/bin/grpc_health_probe

# RUN mkdir -p $GOPATH/src/gitlab.com/empowerlab/example
COPY . /go/src/gitlab.com/empowerlab/stack

#RUN ls -al ./
#RUN ls -al $GOPATH/src/gitlab.com/empowerlab/example
RUN cd /go/src/gitlab.com/empowerlab/stack/login
RUN pwd
RUN cd /go/src/gitlab.com/empowerlab/stack/login  && make dep || true
RUN go get github.com/gogo/protobuf/protoc-gen-gogofaster
RUN go install github.com/gogo/protobuf/protoc-gen-gogofaster
RUN pwd
RUN cd /go/src/gitlab.com/empowerlab/stack/login  && go generate
RUN cd /go/src/gitlab.com/empowerlab/stack/login  && make dep

RUN cd /go/src/gitlab.com/empowerlab/stack/login && make install

FROM alpine

WORKDIR /go/src/gitlab.com/empowerlab/stack/login

COPY --from=build /usr/bin/grpc_health_probe /usr/bin/grpc_health_probe

ENV GOPATH /go
ENV PATH /go/bin:$PATH

COPY --from=build /go/src/gitlab.com/empowerlab/stack/login /go/src/gitlab.com/empowerlab/stack/login
COPY --from=build /go/bin/login /go/bin/login

CMD ["login"]